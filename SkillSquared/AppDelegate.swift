//
//  AppDelegate.swift
//  SkillSquared
//
//  Created by Awais Aslam on 23/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import Braintree
import Paystack
import PushKit
import Firebase
import LocalAuthentication
import BackgroundTasks
import Stripe
import GooglePlaces
import Quickblox
import SVProgressHUD
import QuickbloxWebRTC
import CallKit
import NotificationBannerSwift

struct CredentialsConstant {
    static let applicationID:UInt = 84642
    static let authKey = "Xzp5m4abA7TGrNY"
    static let authSecret = "CtutrAxPcUuBSWU"
    static let accountKey = "dwXRe--TE8KCBtZcvUfC"
}

struct TimeIntervalConstant {
    static let answerTimeInterval: TimeInterval = 60.0
    static let dialingTimeInterval: TimeInterval = 5.0
}

struct AppDelegateConstant {
    static let enableStatsReports: UInt = 1
}

class AppConstants: NSObject {
    let userDefaults = UserDefaults.standard
    static let sharedInstance = AppConstants()
    let notificationClick = "notificationS"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    lazy private var backgroundTask: UIBackgroundTaskIdentifier = {
        let backgroundTask = UIBackgroundTaskIdentifier.invalid
        return backgroundTask
    }()
    
    var isCalling = false {
        didSet {
            if UIApplication.shared.applicationState == .background,
                isCalling == false, CallKitManager.instance.isHasSession() {
                disconnect()
            }
        }
    }
    
    var token:Data!
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var context = LAContext()
    var isGroup:String!
    
    var value:String!
    var dialogValue: String!
    
    let userDefaults = UserDefaults.standard
    let kisFromNotificationSecond = "notificationS"
    let kisFromNotificationThird = "notificationT"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        IQKeyboardManager.shared.enable = true
        
        GMSPlacesClient.provideAPIKey("AIzaSyDv7B05izVAv4muJbK0-iq6N2Q3HTQEmyM")
        BTAppSwitch.setReturnURLScheme("com.techreacher.SkillSquared.payments")
        Paystack.setDefaultPublicKey("pk_test_c09e1d31fde729bb340948942628f8a1dc40fc93")
        
        application.applicationIconBadgeNumber = 0
        
        QBSettings.applicationID = CredentialsConstant.applicationID
        QBSettings.authKey = CredentialsConstant.authKey
        QBSettings.authSecret = CredentialsConstant.authSecret
        QBSettings.accountKey = CredentialsConstant.accountKey
        QBSettings.carbonsEnabled = false
        QBSettings.autoReconnectEnabled = true
        QBSettings.logLevel = QBLogLevel.nothing
        QBSettings.disableXMPPLogging()
        QBSettings.disableFileLogging()
        QBRTCConfig.setLogLevel(QBRTCLogLevel.nothing)
        QBRTCConfig.setAnswerTimeInterval(TimeIntervalConstant.answerTimeInterval)
        QBRTCConfig.setDialingTimeInterval(TimeIntervalConstant.dialingTimeInterval)
        
        
        if AppDelegateConstant.enableStatsReports == 1 {
            QBRTCConfig.setStatsReportTimeInterval(1.0)
        }
        
        
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
        QBRTCClient.initializeRTC()
        
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            // Do your task here
            
            let dic = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? NSDictionary
            let dic2 = dic?.value(forKey: "aps") as? NSDictionary
            let badgeValue = dic2?.value(forKey: "badge") as? Int
            Constants.badgeValue = badgeValue ?? 0
            let type = dic2?.value(forKey: "type") as? String
            Constants.notificationConstant = type ?? ""
            // We can add one more key name 'click_action' in payload while sending push notification and check category for indentifying the push notification type. 'category' is one of the seven built in key of payload for identifying type of notification and take actions accordingly
            if type == "skillsquared"
            {
                /// Set the flag true for is app open from Notification and on root view controller check the flag condition to take action accordingly
                AppConstants.sharedInstance.userDefaults.set(true, forKey: AppConstants.sharedInstance.notificationClick)
            }
                
            else {
                
                
                
            }
            
        }
        registerForRemoteNotifications()
        self.loadSignInOrDashBoard()
        
        return true
    }
    
    func loadSignInOrDashBoard(){
        
        self.splashWindow()
    }
    
    func loginWindow(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let viewController: LoginController = storyboard.instantiateViewController(withIdentifier: "LoginController") as! LoginController;
        
        let nav = UINavigationController(rootViewController: viewController)
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
    }
    
    func splashWindow(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController: SplashViewController = storyboard.instantiateViewController(withIdentifier: "splachController") as! SplashViewController
        
        let nav = UINavigationController(rootViewController: viewController)
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
    }
    
    func dashboardWindow(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let viewController: HomeViewController = storyboard.instantiateViewController(withIdentifier: "Dashboard") as! HomeViewController;
        
        
        
        let nav = UINavigationController(rootViewController: viewController)
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
    }
    
    func dialogWindow() {
        
        let storyboard = UIStoryboard(name: "Dialogs", bundle: nil)
        let viewController: DialogsViewController = storyboard.instantiateViewController(withIdentifier: "DialogsViewController") as! DialogsViewController
        
        let nav = UINavigationController(rootViewController: viewController)
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
        
    }
    
    
    func homeFromNotifications(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let viewController: HomeViewController = storyboard.instantiateViewController(withIdentifier: "Dashboard") as! HomeViewController;
        
        viewController.value = "FromDelegate"
        
        let nav = UINavigationController(rootViewController: viewController)
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
    }
    
    
    
    //    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    //
    //        print("App delegate string\(userInfo)")
    //
    //    }
    //
    //
    //    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Swift.Void) {
    //
    //        print(userInfo)
    //
    //    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        
    }
    
    //    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    //        print("Unable to register for remote notifications: \(error.localizedDescription)")
    //    }
    
    //    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //
    //        let token = deviceToken
    //        Messaging.messaging().apnsToken = token
    //
    //
    //        let deviceTokenString = deviceToken.hexString
    //        print("deviceTokenString is \(deviceTokenString)")
    //        Constants.DeviceToken = deviceTokenString
    //
    //
    //                let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
    //                voipRegistry.desiredPushTypes = Set([PKPushType.voIP])
    //                voipRegistry.delegate = self
    //
    //    }
    //
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        
        
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        if isCalling == false {
            disconnect()
        }
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        
        registerForRemoteNotifications()
        if QBChat.instance.isConnected == true {
            return
        }
        
        connect { (error) in
            if let error = error {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                return
            }
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        disconnect()
        self.saveContext()
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        guard let identifierForVendor = UIDevice.current.identifierForVendor else {
            return
        }
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        
        print("Device Token: \(token)")
        deviceTokenGet = token
        
        let deviceIdentifier = identifierForVendor.uuidString
        let subscription = QBMSubscription()
        subscription.notificationChannel = .APNS
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = deviceToken
        print("deviceTokenString is \(deviceToken)")
        QBRequest.createSubscription(subscription, successBlock: { response, objects in
            
            print("Subscribe")
            
        }, errorBlock: { response in
            debugPrint("[AppDelegate] createSubscription error: \(String(describing: response.error))")
        })
    }
    
    
    //    func application(_ application: UIApplication,
    //                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //
    //        guard let identifierForVendor = UIDevice.current.identifierForVendor else {
    //            return
    //        }
    //
    //        let tokenParts = deviceToken.map { data -> String in
    //            return String(format: "%02.2hhx", data)
    //        }
    //
    //        let token = tokenParts.joined()
    //
    //        print("Device Token: \(token)")
    //        deviceTokenGet = token
    //
    //        let deviceIdentifier = identifierForVendor.uuidString
    //        let subscription = QBMSubscription()
    //        subscription.notificationChannel = .APNS
    //        subscription.deviceUDID = deviceIdentifier
    //        subscription.deviceToken = deviceToken
    //        print("deviceTokenString is \(deviceToken)")
    //        QBRequest.createSubscription(subscription, successBlock: { response, objects in
    //
    //            print("Subscribe")
    //
    //        }, errorBlock: { response in
    //            debugPrint("[AppDelegate] createSubscription error: \(String(describing: response.error))")
    //        })
    //    }
    //
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        debugPrint("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    private func registerForRemoteNotifications() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.sound, .alert, .badge], completionHandler: { granted, error in
            if let error = error {
                debugPrint("[AppDelegate] requestAuthorization error: \(error.localizedDescription)")
                return
            }
            center.getNotificationSettings(completionHandler: { settings in
                if settings.authorizationStatus != .authorized {
                    return
                }
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            })
        })
    }
    
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare("com.techreacher.SkillSquared.payments") == .orderedSame {
            return BTAppSwitch.handleOpen(url, options: options)
        }
        return false
    }
    
    
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "SkillSquared")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        
        if let info = userInfo["aps"] as? Dictionary<String, AnyObject> {
            
            if  let badgeValue = info["badge"] as? Int {
                Constants.badgeValue = badgeValue
                print("badgeValue is:  \(badgeValue)")
            }
            
            if  let myType = info["type"] as? String {
                print("Type is:  \( myType)")
                
                if myType == "skillsquared"{
                    
                    if let push = userInfo as? [String:Any]{
                        print(push)
                        
                        self.handleNotificationTypeClick(notification: push, state: UIApplication.shared.applicationState)
                        
                    }
                }
            }
        }
        
        center.removeAllDeliveredNotifications()
        center.removeAllPendingNotificationRequests()
        
        guard let dialogID = userInfo["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String,
            dialogID.isEmpty == false else {
                return
        }
        // calling dispatch async for push notification handling to have priority in main queue
        DispatchQueue.main.async {
            
            if let chatDialog = ChatManager.instance.storage.dialog(withID: dialogID) {
                self.openChat(chatDialog)
            } else {
                ChatManager.instance.loadDialog(withID: dialogID, completion: { (loadedDialog: QBChatDialog?) -> Void in
                    guard let dialog = loadedDialog else {
                        return
                    }
                    self.openChat(dialog)
                })
            }
        }
        completionHandler()
    }
    
    
    
    func openChat(_ chatDialog: QBChatDialog) {
        
        
        guard let window = window,
            let navigationController = window.rootViewController as? UINavigationController else {
                return
        }
        var controllers = [UIViewController]()
        
        for controller in navigationController.viewControllers {
            controllers.append(controller)
            
            if controller is HomeViewController {
                
                let storyboard = UIStoryboard(name: "Chat", bundle: nil)
                let chatController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatController.dialogID = chatDialog.id
                chatController.userName = chatDialog.name
                chatController.valueOfDelegate = "FromDelegate"
                controllers.append(chatController)
                navigationController.setViewControllers(controllers, animated: true)
                return
            }
            
        }
    }
    
    
    //MARK: - Connect/Disconnect
    func connect(completion: QBChatCompletionBlock? = nil) {
        let currentUser = Profile()
        
        guard currentUser.isFull == true else {
            completion?(NSError(domain: LoginConstant.chatServiceDomain,
                                code: LoginConstant.errorDomaimCode,
                                userInfo: [
                                    NSLocalizedDescriptionKey: "Please enter your login and username."
            ]))
            return
        }
        if QBChat.instance.isConnected == true {
            completion?(nil)
        } else {
            QBSettings.autoReconnectEnabled = true
            QBChat.instance.connect(withUserID: currentUser.ID, password: currentUser.password, completion: completion)
        }
    }
    
    func disconnect(completion: QBChatCompletionBlock? = nil) {
        QBChat.instance.disconnect(completionBlock: completion)
    }
}


extension AppDelegate {
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var rootViewController: RootParentVC {
        return window!.rootViewController as! RootParentVC
    }
}

extension AppDelegate{

    func handleNotificationTypeClick(notification:[String:Any], state:UIApplication.State){
        
        
        print("Notification is : \(notification)")
        
        switch state {
            
        case .active:
            print(state)
            
            break
            
            
        case .background, .inactive:
            print(state)
            print("Background")
            
            let val = "testing123"
            let info = [ "text" : val]
            NotificationCenter.default.post(name: Notification.Name("notification"), object: nil, userInfo: info)
            UserDefaults.standard.set(true, forKey: "openedByShortcutAction")
            
            break
        @unknown default:
            break
        }
    }
}
