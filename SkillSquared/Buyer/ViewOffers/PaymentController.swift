//
//  PaymentController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 04/12/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Braintree
import Alamofire
import SwiftyJSON
import SVProgressHUD
import Stripe

class PaymentController: UIViewController, postrequest {
    
    @IBOutlet weak var paypalBtn: UIButton!
    
    var orderPrice = ""
    var Paypalnonce = ""
    var req_id = ""
    var braintreeClient: BTAPIClient?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paypalBtn.visibility == .invisible
        paypalBtn.visibility = .gone
        
        print(req_id)
        braintreeClient = BTAPIClient(authorization: "sandbox_kttn24dq_63zmsxbxhgp8p9g9")!
        print(orderPrice)
    }
    
    @IBAction func paywithPaypal(_ sender: Any) {
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        let payPalDriver = BTPayPalDriver(apiClient: braintreeClient!)
        payPalDriver.viewControllerPresentingDelegate = self
        payPalDriver.appSwitchDelegate = self
        
        // Specify the transaction amount here. "2.32" is used in this example.
        let request = BTPayPalRequest(amount: "2.32")
        request.currencyCode = "USD" // Optional; see BTPayPalRequest.h for more options
        
        payPalDriver.requestOneTimePayment(request) { (tokenizedPayPalAccount, error) in
            if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                print("Got a nonce: \(tokenizedPayPalAccount.nonce)")
                self.Paypalnonce = tokenizedPayPalAccount.nonce
                
                
                SVProgressHUD.dismiss()
                
                self.ProceedPayment()
                //
                // Access additional information
                //                let email = tokenizedPayPalAccount.email
                //                let firstName = tokenizedPayPalAccount.firstName
                //                let lastName = tokenizedPayPalAccount.lastName
                //                let phone = tokenizedPayPalAccount.phone
                //
                //                // See BTPostalAddress.h for details
                //                let billingAddress = tokenizedPayPalAccount.billingAddress
                //                let shippingAddress = tokenizedPayPalAccount.shippingAddress
                
                
            } else if error != nil {
                print(error ?? "")
            }
                
            else {
                SVProgressHUD.dismiss()
                let message = "Payment Not Successful"
                
                let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel,handler: nil));
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
    }
    
    @IBAction func paywithPaystack(_ sender: Any) {
        
        
        let payStackController = self.storyboard?.instantiateViewController(withIdentifier: "PaystackVc") as! PaymentViewController
        payStackController.providesPresentationContextTransitionStyle = true
        payStackController.definesPresentationContext = true
        self.navigationController?.present(payStackController, animated: true, completion: nil)
    }
    
    
    @IBAction func payWithStripe(_ sender: Any) {
        
        
        let stripeController = self.storyboard?.instantiateViewController(withIdentifier: "StripeController") as! StripeViewController
        stripeController.navigation = self.navigationController
        stripeController.request_id = req_id
        stripeController.price = orderPrice
        stripeController.providesPresentationContextTransitionStyle = true
        stripeController.definesPresentationContext = true
        self.navigationController?.present(stripeController, animated: true, completion: nil)
        stripeController.dismissController = self
    }
    
    
    @IBAction func closeBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onViewTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    
    }
    
    func didJobPosted() {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: ViewOffersVC.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
}

extension PaymentController: BTViewControllerPresentingDelegate {
    
    
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
    }
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
    }
}

extension PaymentController: BTAppSwitchDelegate{
    
    
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
    }
}


extension PaymentController{
    
    func ProceedPayment() {
        
        SVProgressHUD.show()
        
        let val = self.orderPrice.toDouble()! + 3.00
        print(val)
        
        let price = val
        let payNonce = Paypalnonce
        let id = req_id
        
        let params = ["nonce":payNonce,
                      "amount":price,
                      "payment_method": "paypal",
                      "request_id":id]
            as [String : AnyObject]
        
        print(params)
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let url = "placeOrder"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON {response in
            
            var err:Error?
            
            switch response.result {
                
            case .success(let json):
                print(json)
                
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let status = dictObject["status"] as! Int
                    let message = dictObject["message"] as! String
                    
                    
                    if (status == 200){
                        
                        SVProgressHUD.dismiss()
                        
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            
                            self.dismiss(animated: true) {
                                
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: ViewOffersVC.self) {
                                        _ =  self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                            }
                        }
                        
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        SVProgressHUD.dismiss()
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
            case .failure(let error):
                err = error
                print(err!)
                SVProgressHUD.dismiss()
                
            }
        }
    }
}
