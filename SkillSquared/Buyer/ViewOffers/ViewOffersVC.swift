//
//  ViewOffersVC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 28/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import Quickblox
import SVProgressHUD

class ViewOffersVC: UIViewController, postrequest  {
    
    @IBOutlet weak var ViewOffersCollectionView: UICollectionView!
    
    var catId = ""
    var reqId = ""
    var sellerCatId = ""
    var sellerRequestArray: Array<Asellerrequest>?
    var orderCheck: Int = 0
    
    
    var selectedUsers = QBUUser()
    private let chatManager = ChatManager.instance
    
    var ViewOffersFlowLayout: UICollectionViewFlowLayout!
    let OffersCollectionViewIdentifier = "OffersCell"
    var deleteindexOffers: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(catId)
        SetUpCollectionView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        getOffersData()
    }
    
    private func SetUpCollectionView(){
        ViewOffersCollectionView.delegate = self
        ViewOffersCollectionView.dataSource = self
        
        if let layout = ViewOffersCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 5
            layout.sectionInset = UIEdgeInsets(top: 10, left: 2, bottom: 10, right: 2)
            let width =  UIScreen.main.bounds.width
            let size = CGSize(width:width - 30, height: 170)
            layout.itemSize = size
        }
        
        let Offersnib = UINib(nibName: "ViewOffersCell", bundle: nil)
        ViewOffersCollectionView.register(Offersnib, forCellWithReuseIdentifier: OffersCollectionViewIdentifier)
    }
}


extension ViewOffersVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return sellerRequestArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OffersCollectionViewIdentifier, for: indexPath) as! ViewOffersCell
        
        if let sellerData = sellerRequestArray{
            cell.userImage.kf.setImage(with: URL(string: sellerData[indexPath.item].freelancerimage!), placeholder: UIImage(named: "Placeholder"))
            cell.OffersUserName.text = sellerData[indexPath.row].username ?? ""
            cell.offerDescription.text = sellerData[indexPath.row].description ?? ""
            cell.orderBtn.setTitle("\(sellerData[indexPath.item].price ?? "")\("$")", for: .normal)
            let ratings = sellerData[indexPath.item].avgRating ?? 0
            let value = Double(ratings)
            cell.userReviewsStars.rating = value
            
            if orderCheck == 0{
                
                cell.orderBtn.backgroundColor = #colorLiteral(red: 0, green: 0.6153565645, blue: 0.06842776388, alpha: 1)
                cell.orderBtn.isUserInteractionEnabled = true
                cell.orderBtn.addTarget(self, action: #selector(orderBtnTapped(_ :)), for: .touchUpInside)
                cell.orderBtn.tag = indexPath.row
            }
            else if orderCheck == 1{
                cell.orderBtn.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                cell.orderBtn.isUserInteractionEnabled = false
            }
            
            cell.askQuestion.addTarget(self, action: #selector(QuestionbtnTapped(_ :)), for: .touchUpInside)
            cell.askQuestion.tag = indexPath.row
            
            
            cell.deleteBtn.addTarget(self, action: #selector(deleteBtnTapped(_ :)), for: .touchUpInside)
            cell.deleteBtn.tag = indexPath.row
            
            
            let id = sellerData[indexPath.item].request_id ?? ""
            self.reqId = id
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        
        self.sellerCatId = sellerRequestArray![indexPath.row].freelancer_id ?? ""
        let PopupController = self.storyboard?.instantiateViewController(withIdentifier: "OffersPopUp") as! OffersPopUpViewController
        PopupController.providesPresentationContextTransitionStyle = true
        PopupController.definesPresentationContext = true
        PopupController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        PopupController.catid = self.sellerCatId
        self.navigationController?.present(PopupController, animated: true, completion: nil)
    }
    
    @objc func orderBtnTapped(_ sender: UIButton){
        
        let price = sellerRequestArray?[sender.tag].price ?? ""
        let req_id = sellerRequestArray?[sender.tag].request_id ?? ""
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let paymentController = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentController
        paymentController.orderPrice = price
        paymentController.req_id = req_id
        self.navigationController?.pushViewController(paymentController, animated: true)
        
    }
    
    @objc func QuestionbtnTapped(_ sender: UIButton){
        
        let fullName = sellerRequestArray?[sender.tag].seller_name ?? ""
        let login = sellerRequestArray?[sender.tag].seller_email ?? ""
        
        SVProgressHUD.show()
        
        signUp(fullName: fullName, login: login)
    }
    
    @objc func deleteBtnTapped(_ sender: UIButton){
        
        
        let alert = UIAlertController(title: NSLocalizedString("Report", comment: ""), message: NSLocalizedString("Are you sure you want to delete this offer?", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
            switch action.style{
            case .default:
                
                let id = self.sellerRequestArray?[sender.tag].request_id ?? ""
                self.deleteRow(index: sender.tag)
                self.deleteItemOffers(req_id: id)
                
                
            case .cancel: break
                
            case .destructive: break
            @unknown default:
                fatalError()
            }}))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel",comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func deleteRow(index: Int){
        print("indx is : \(index)")
        print("one")
        
        sellerRequestArray?.remove(at: index)
    }
    
    
    func didJobPosted() {
        self.navigationController!.popViewController(animated: true)
    }
    
}

extension ViewOffersVC{
    
    func getOffersData() {
        
        SVProgressHUD.show()
        
        let url = "offerReceived"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let params = [
            "request_id": catId
            ] as [String : Any]
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                
                do{
                    let responseObject = try JSONDecoder().decode(ViewOffersObject.self, from: jsonData)
                    let sellerArray = responseObject.asellerrequest
                    
                    self.sellerRequestArray = sellerArray
                    self.orderCheck = responseObject.ordercheck ?? 0
                    self.ViewOffersCollectionView.reloadData()
                }
                catch{
                    print(error.localizedDescription)
                }
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}

extension ViewOffersVC {
    
    func deleteItemOffers(req_id: String){
        
        SVProgressHUD.show()
        
        let url = "removesellerrequest"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let params = [
            "offer_id": req_id
            ] as [String : Any]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            var err:Error?
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            switch response.result {
                
            case .success(let json):
                print(json)
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let message = dictObject["message"] as! String
                    let status = dictObject["status"] as! Int
                    
                    if status == 200 {
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                        
                    else{
                        let alertController = UIAlertController(title: "Alert", message: "The ERROR CODE: \(String(describing: status)) & \(String(describing: message))", preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                    self.ViewOffersCollectionView.reloadData()
                }
                
            case .failure(let error):
                SVProgressHUD.dismiss()
                err = error.localizedDescription as? Error
                print(err!)
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
                self.ViewOffersCollectionView.reloadData()
                
            }
        }
    }
}

extension ViewOffersVC{
    
    
    private func signUp(fullName: String, login: String) {
        
        let user = QBUUser()
        user.login = login
        user.fullName = fullName
        user.password = LoginConstant.defaultPassword
        
        QBRequest.signUp(user, successBlock: { response, user in
            
            self.getUser(withLogin: login)
            
        }, errorBlock: { (response) in
            
            print("User Already Created")
            
            self.getUser(withLogin: login)
            
        })
    }
    
    private func getUser(withLogin: String){
        
        QBRequest.user(withLogin: withLogin, successBlock: { (response, user) in
            
            print("Get user is : \(user)")
            self.selectedUsers = user
            self.CreateDialogue()
            
        }, errorBlock: { (response) in
        })
    }
    
    func CreateDialogue(){
        
        if Reachability.instance.networkConnectionStatus() == .notConnection {
            showAlertView(LoginConstant.checkInternet, message: LoginConstant.checkInternetMessage)
            SVProgressHUD.dismiss()
            return
        }
        
        SVProgressHUD.show()
        
        chatManager.createPrivateDialog(withOpponent: selectedUsers, completion: { (response, dialog) in
            guard let dialog = dialog else {
                if let error = response?.error {
                    SVProgressHUD.showError(withStatus: error.error?.localizedDescription)
                }
                return
            }
            SVProgressHUD.showSuccess(withStatus: "Success".localized)
            self.openNewDialog(dialog)
        })
    }
    
    private func openNewDialog(_ newDialog: QBChatDialog) {
        
        let storyboard = UIStoryboard(name: "Chat", bundle: nil)
        let dialogsVC = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        dialogsVC.dialogID = newDialog.id
        dialogsVC.id = newDialog.userID
        dialogsVC.newDialogue = selectedUsers
        dialogsVC.selectedValue = "Gigs"
        self.navigationController?.pushViewController(dialogsVC, animated: true)
        
    }
}
