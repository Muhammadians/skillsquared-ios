//
//  StripeViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 27/03/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import CreditCardForm
import Stripe
import Alamofire
import ObjectMapper
import SVProgressHUD
import SwiftyJSON

class StripeViewController: UIViewController, STPPaymentCardTextFieldDelegate{
    
    @IBOutlet weak var chargeBtn: UIButton!
    @IBOutlet weak var creditCardFormView: CreditCardFormView!
    @IBOutlet weak var paymentTextField: STPPaymentCardTextField!
    
    
    var navigation : UINavigationController!
    var dismissController : postrequest?
    var request_id:String!
    var price:String!
    var OrderType:String!
    var order_id:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(request_id!)
        print(price!)
      
        
        paymentTextField.delegate = self
        creditCardFormView.cardHolderString = Constants.userName
        
    }
    
    @IBAction func chargeTapped(_ sender: Any) {
        ValidateCard()
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        creditCardFormView.paymentCardTextFieldDidChange(cardNumber: textField.cardNumber, expirationYear: textField.expirationYear, expirationMonth: textField.expirationMonth, cvc: textField.cvc)
    }
    
    func paymentCardTextFieldDidEndEditingExpiration(_ textField: STPPaymentCardTextField) {
        creditCardFormView.paymentCardTextFieldDidEndEditingExpiration(expirationYear: textField.expirationYear)
    }
    
    func paymentCardTextFieldDidBeginEditingCVC(_ textField: STPPaymentCardTextField) {
        creditCardFormView.paymentCardTextFieldDidBeginEditingCVC()
    }
    
    func paymentCardTextFieldDidEndEditingCVC(_ textField: STPPaymentCardTextField) {
        creditCardFormView.paymentCardTextFieldDidEndEditingCVC()
    }
    
}


extension StripeViewController{
    
    func ValidateCard() {
        
        SVProgressHUD.show()
        
        let cardNo = paymentTextField.cardNumber
        let exp_month = paymentTextField.expirationMonth
        let exp_year = paymentTextField.expirationYear
        let cvc = paymentTextField.cvc
        
        
        let params = [
            
            "card": cardNo ?? "",
            "exp_month": exp_month,
            "exp_year": exp_year,
            "cvc": cvc ?? ""
            
            ] as [String : Any]
        
        print(params)
        
        let url = "validateCard"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        Alamofire.request(baseUrl, method: .post, parameters: params).responseJSON { (response) in
            print(response)
            if response.result.isSuccess {
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<StripeModal>().map(JSONString: jsonStr!)
                
                if responseModel?.status == 200 {
                    
                    let stripeToken = responseModel?.id
                    
                    if self.OrderType == "customOrder" {
                        
                        self.ProceedCustomPayment(token: stripeToken ?? "")
                    }
                    else {
                        
                    self.ProceedPayment(token: stripeToken ?? "")
                        
                    }
                }
                    
                    
                else if responseModel?.status == 204 {
                    
                    SVProgressHUD.dismiss()
                    
                    let message = responseModel?.message
                    
                    let alertController = UIAlertController(title: "Alert", message: message!, preferredStyle: .alert);
                    alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                    self.present(alertController, animated: true, completion: nil)
                    
                }
            }
                
            else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
            }
        }
    }
}

extension StripeViewController{
    
    func ProceedPayment(token: String) {
        
        SVProgressHUD.show()
        
        let val = self.price.toDouble()! + 3.00
        let str = self.price + "3"
        print(str)
        
        let params = ["nonce":token,
                      "amount":val,
                      "payment_method": "stripe",
                      "request_id":request_id!]
            
            as [String : AnyObject]
        
        print(params)
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let url = "placeOrder"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON {response in
            
            var err:Error?
            
            switch response.result {
                
            case .success(let json):
                print(json)
                
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let status = dictObject["status"] as! Int
                    let message = dictObject["message"] as! String
                    
                    
                    if (status == 200){
                        
                        SVProgressHUD.dismiss()
                        
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            
                            self.dismiss(animated: true) {
                                self.dismissController?.didJobPosted()
                            }
                        }
                        
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        SVProgressHUD.dismiss()
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
            case .failure(let error):
                err = error
                print(err!)
                SVProgressHUD.dismiss()
                
            }
        }
    }
}


extension StripeViewController{
    
    func ProceedCustomPayment(token: String) {
        
        SVProgressHUD.show()
        
        let val = self.price.toDouble()! + 3.00
        let str = self.price + "3"
        print(str)
        
        let params = ["nonce":token,
                      "amount":val,
                      "payment_method": "stripe",
                      "customorderid":request_id ?? ""]
            
            as [String : AnyObject]
        
        print(params)
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let url = "placeOrderCustom"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON {response in
            
            var err:Error?
            
            switch response.result {
                
            case .success(let json):
                print(json)
                
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let status = dictObject["status"] as! Int
                    let message = dictObject["message"] as! String
                    
                    
                    if (status == 200){
                        
                        SVProgressHUD.dismiss()
                        
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            
                            self.dismiss(animated: true) {
                                self.dismissController?.didJobPosted()
                            }
                        }
                        
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        SVProgressHUD.dismiss()
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
            case .failure(let error):
                err = error
                print(err!)
                SVProgressHUD.dismiss()
                
            }
        }
    }
}
