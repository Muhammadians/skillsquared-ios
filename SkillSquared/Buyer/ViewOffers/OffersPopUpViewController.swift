//
//  OffersPopUpViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 21/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RappleProgressHUD
import Kingfisher

class OffersPopUpViewController: UIViewController {
    @IBOutlet weak var userNme: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var freeLancerDescription: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var freeLancerTitle: UILabel!
    @IBOutlet weak var totalReviews: UILabel!
    
    var catid = ""
    var freeLancerInfo : Freelancer_info?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getSellerProfile()
//        fillUserDetail()
    }
    
    func fillUserDetail()  {
        
        if let details = freeLancerInfo{
            
            self.userNme.text = details.username
            print(self.userNme!)
            self.freeLancerTitle.text = details.freelancer_title
            self.userImage.kf.setImage(with: URL(string: details.userImage!), placeholder: UIImage(named: "Placeholder"))
            self.freeLancerDescription.text = details.description?.html2String
            self.location.text = details.location
            self.language.text = details.language
            
            let x: Int = details.avgRating ?? 0
            self.totalReviews.text = String(x)
            
            
        }
    }
    
    
    @IBAction func dismisPopUp(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    @IBAction func closePopup(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
}

extension OffersPopUpViewController {
    
    func getSellerProfile(){
        
        let url = "sellerInfoByFreelancerID"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let params = [
            "freelancer_id": catid
            ] as [String : Any]
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                RappleActivityIndicatorView.stopAnimation()
                
                guard let jsonData = response.data else{return}
                do{
                    let responseObject = try JSONDecoder().decode(FreeLancerObject.self, from: jsonData)
                    
                    if responseObject.freelancer_info != nil {
                    
                    self.freeLancerInfo = responseObject.freelancer_info
                    self.fillUserDetail()
                    }
                    else{
                        Alert.showAlert(on: self, title: "Alert", message: "No Data Found")
                    }
                }
                catch{
                    Alert.showAlert(on: self, title: "Alert", message: "No Data Found")
                    print(error.localizedDescription)
                }
            }
        }
        
        
    }
}
