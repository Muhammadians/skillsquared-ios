//
//  ViewOffersCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 28/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import FloatRatingView

protocol DeleteOffers {
    func removeSellerRequest(indx: Int)
}

class ViewOffersCell: UICollectionViewCell {
    @IBOutlet weak var OffersUserName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var offerDescription: PaddingLabel!
    @IBOutlet weak var orderBtn: UIButton!
    @IBOutlet weak var askQuestion: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var userReviewsStars: FloatRatingView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

   
}
