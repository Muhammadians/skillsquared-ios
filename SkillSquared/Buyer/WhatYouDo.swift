//
//  WhatYouDo.swift
//  SkillSquared
//
//  Created by Awais Aslam on 23/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class WhatYouDo: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func onPostJobTap(_ sender: Any) {
        performSegue(withIdentifier: "PostRequest", sender: self)
        
    }
    
    @IBAction func viewJobTapped(_ sender: Any) {
        
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "postedRequest") as! PostedRequestVC
         self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func manageOrdersTapped(_ sender: Any) {
        
//        alertPopupss(title: "Buyer Orders")

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let normalOrderController = storyboard.instantiateViewController(withIdentifier: "BuyerNormalOrder") as! ManageOrdersViewController
        self.navigationController?.pushViewController(normalOrderController, animated: true)
    }

}


//extension WhatYouDo {
//    
//    
//    func alertPopupss(title : String)
//    {
//        let alerts = UIAlertController.init(title: title , message: "", preferredStyle: .alert)
//        
//        alerts.addAction(UIAlertAction(title: "Normal Orders", style: .default, handler: { (uiaction:UIAlertAction) in
//            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let normalOrderController = storyboard.instantiateViewController(withIdentifier: "BuyerNormalOrder") as! ManageOrdersViewController
//            self.navigationController?.pushViewController(normalOrderController, animated: true)
//            
//        }))
//        
//        alerts.addAction(UIAlertAction(title: "Custom Orders", style: .default, handler: { (uiaction:UIAlertAction) in
//
//            
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let customOrderController = storyboard.instantiateViewController(withIdentifier: "BuyerCustomOrder") as! ManageOrderCustomController
//            self.navigationController?.pushViewController(customOrderController, animated: true)
//        
//        
//        }))
//      
//        
//        alerts.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (uiaction:UIAlertAction) in
//            
//            print("alert actions active")
//            
//        }))
//        
//        self.present(alerts, animated: true, completion: nil)
//    }
//    
//}
