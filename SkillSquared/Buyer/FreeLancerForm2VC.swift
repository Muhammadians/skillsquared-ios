//
//  FreeLancerForm2VC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 13/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import Photos
import Kingfisher
import SVProgressHUD
import MobileCoreServices
import SwiftyJSON
import ObjectMapper

class FreeLancerForm2VC: UIViewController {
    
    @IBOutlet weak var imageName: UIButton!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var languageDropDown: DropDown!
    @IBOutlet weak var availabilityDropDown: DropDown!
    @IBOutlet weak var faceBookField: UITextField!
    @IBOutlet weak var linkedInField: UITextField!
    @IBOutlet weak var webSiteField: UITextField!
    @IBOutlet weak var twiterField: UITextField!
    @IBOutlet weak var instagramField: UITextField!
    
    var langArr = [String]()
    var name = ""
    var titleString = ""
    var country = ""
    var state = ""
    var city = ""
    var catid = ""
    var des = ""
    var selectedLanguage = ""
    var availabilityTxt = ""
    
    var imagePicker = UIImagePickerController()
    var selectedPhoto:UIImage?
    var pickedImageName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(titleString)
        print(country)
        print(state)
        print(city)
        print(catid)
        print(des)
        getLanguages()
        selectAvailability()
    }
    
    @IBAction func ChooseFileTapped(_ sender: Any) {
        
        imageActionSheet()
    }
    
    
    func getLanguages()
    {
        languageDropDown.optionArray = langArr
        languageDropDown.didSelect{(selectedText , index ,id) in
            
            self.selectedLanguage = selectedText
            print("\(self.selectedLanguage)")
        }
    }
    
    func selectAvailability()
    {
        availabilityDropDown.optionArray = ["Full-Time","Part-Time"]
        availabilityDropDown.didSelect{(selectedText , index ,id) in
            
            self.availabilityTxt = selectedText
            print("\(self.availabilityTxt)")
        }
    }
    
    @IBAction func BecomeaFreelancer(_ sender: Any) {
        uploadform()
    }
}

extension FreeLancerForm2VC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? NSURL
        
        self.pickedImageName = (imageURL?.lastPathComponent) ?? ""
        imageName.setTitle(pickedImageName, for: .normal)
        
        
        var selectImage : UIImage!
        if let img = info[.editedImage] as? UIImage
        {
            selectImage = img
        }
        else if let img = info[.originalImage] as? UIImage
        {
            selectImage = img
        }
        self.selectedPhoto = selectImage
        self.selectedImage.image = selectImage
        dismiss(animated: true, completion: nil)
        
    }
    
    func imageActionSheet() {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let pickFromCamera = UIAlertAction(title: "Take From Camera", style: .default)
        { _ in
            
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera;
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetController.addAction(pickFromCamera)
        
        let pickFromGallery = UIAlertAction(title: "Pick From Gallery", style: .default)
        { _ in
            
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        actionSheetController.addAction(pickFromGallery)
        self.present(actionSheetController, animated: true, completion: nil)
    }
}

extension FreeLancerForm2VC {
    
    func uploadform(){
        
        if selectedLanguage.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please select Language")
        }
            
        else if availabilityTxt.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please select availability")
        }
            
        else if selectedPhoto == nil{
            Alert.showAlert(on: self, title: "Alert", message: "Please select Driver's license / id")
        }
            
            
        else {
            
            SVProgressHUD.show()
            
            let title = titleString
            let selectedCountry = country
            let selectedCity = city
            let selectedState = state
            let description = des
            let cat_id = catid
            let language = selectedLanguage
            let availability = availabilityTxt
            
            
            let params = [ "freelancer_title": title,
                           "category_id": cat_id,
                           "availablity": availability,
                           "description": description,
                           "country": selectedCountry,
                           "state": selectedState,
                           "city": selectedCity,
                           "username": name,
                           "language": language,
                           "facebbok": faceBookField.text ?? "",
                           "linked_in": linkedInField.text ?? "",
                           "google_plus": webSiteField.text ?? "",
                           "twitter": twiterField.text ?? "",
                           "instagram": instagramField.text ?? ""
                ] as [String : Any]
            
            
        
            
            let url = "saveBecomeFreelancer"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            let headers: HTTPHeaders = [
                "Content-type": "multipart/form-data",
                "accesstoken": Constants.accessToken
            ]
            
            let imgData = selectedPhoto?.jpegData(compressionQuality: 0.4)
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                if let data = imgData{
                    multipartFormData.append(data, withName: "identityfile", fileName: "\(Date().timeIntervalSinceNow)image.jpg", mimeType: "image/jpg")
                }
                
                for (key, value) in params {
                    print("\(key):\(value)")
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
                print(params)
                
                
            },  to: baseUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        print(response.result.isSuccess)
                        
                        SVProgressHUD.dismiss()
                        
                        guard let data = response.data else{return}
                        let jsonStr = String(data: data, encoding: .utf8)
                        let responseModel = Mapper<Login>().map(JSONString: jsonStr!)
                        
                        let jsonData = JSON(response.result.value!)
                        print(jsonData)
                        
                        if let dictObject = jsonData.dictionaryObject {
                            print(dictObject)
                            
                            let message = dictObject["message"] as! String
                            let status = dictObject["status"] as! Int
                            
                            if status == 200 {
                                
                                Constants.userImage = responseModel?.skillSquaredUser?.image ?? ""
                                Constants.freeLancer = responseModel?.skillSquaredUser?.freelancerProfileSet ?? false
                                
                                
                                let alertController = UIAlertController(title: "Title", message: (String(describing: message)), preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                    
                                    self.dismiss(animated: true) {
                                        self.navigationController?.popToRootViewController(animated: true)
                                    }
                                    
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                                
                            else{
                                let alertController = UIAlertController(title: "Alert", message: "\(String(describing: message))", preferredStyle: .alert);
                                alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                    
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    print(error)
                    SVProgressHUD.dismiss()
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
                }
            }
        }
    }
}

