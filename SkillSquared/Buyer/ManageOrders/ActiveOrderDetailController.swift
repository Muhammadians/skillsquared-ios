//
//  ActiveOrderDetailController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 19/03/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper
import SVProgressHUD

class ActiveOrderDetailController: UIViewController, postrequest {
    
    @IBOutlet weak var order: UILabel!
    @IBOutlet weak var orderDescription: UILabel!
    @IBOutlet weak var buyer: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var orderDuration: UILabel!
    @IBOutlet weak var completedBtn: UILabel!
    @IBOutlet weak var revisionLabel: UILabel!
    @IBOutlet weak var cancelOrder: UILabel!
    @IBOutlet weak var deliverOrderStatus: UILabel!
    @IBOutlet weak var downloadSourceBtn: UILabel!
    @IBOutlet weak var sellerDeliveryNote: UILabel!
    @IBOutlet weak var deliveryNoteLabel: UILabel!
    @IBOutlet weak var disputeBtn: UILabel!
    
    
    var orderDetailArr: Array<Sellerorderdetail>?
    var disputeArr: Array<Disputereason>?
    var selectedIndexValue: Int = 0
    var type:String!
    var value:String!
    var statusOrderid:String!
    var orderId:String!
    var imageUrl:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("id is  \(orderId ?? "")")
        print("Type is  \(type ?? "")")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if value == "Active"{
            
            revisionLabel.visibility = .invisible
            revisionLabel.visibility = .gone
            
            completedBtn.visibility = .invisible
            completedBtn.visibility = .gone
            
            deliverOrderStatus.visibility = .invisible
            deliverOrderStatus.visibility = .gone
            
            downloadSourceBtn.visibility = .invisible
            downloadSourceBtn.visibility = .gone
            
            disputeBtn.visibility = .invisible
            disputeBtn.visibility = .gone
            
            sellerDeliveryNote.visibility = .invisible
            deliveryNoteLabel.visibility = .gone
            
        }
            
        else if value == "Delivered"{
            
            revisionLabel.visibility = .visible
            completedBtn.visibility = .visible
            
            disputeBtn.visibility = .invisible
            disputeBtn.visibility = .gone
            
        }
            
        else if value == "Completed" {
            
            revisionLabel.visibility = .invisible
            revisionLabel.visibility = .gone
            
            completedBtn.visibility = .invisible
            completedBtn.visibility = .gone
            
            cancelOrder.visibility = .invisible
            cancelOrder.visibility = .gone
            
            
            deliverOrderStatus.visibility = .invisible
            deliverOrderStatus.visibility = .gone
            
            downloadSourceBtn.visibility = .invisible
            downloadSourceBtn.visibility = .gone
            
            sellerDeliveryNote.visibility = .invisible
            deliveryNoteLabel.visibility = .gone
            
            disputeBtn.visibility = .invisible
            disputeBtn.visibility = .gone
            
        }
        else if value == "Revision" {
            
            revisionLabel.visibility = .invisible
            revisionLabel.visibility = .gone
            
            completedBtn.visibility = .invisible
            completedBtn.visibility = .gone
            
            cancelOrder.visibility = .invisible
            cancelOrder.visibility = .gone
            
            deliverOrderStatus.visibility = .invisible
            deliverOrderStatus.visibility = .gone
            
            downloadSourceBtn.visibility = .invisible
            downloadSourceBtn.visibility = .gone
            
            sellerDeliveryNote.visibility = .invisible
            deliveryNoteLabel.visibility = .gone
            
            disputeBtn.visibility = .invisible
            disputeBtn.visibility = .gone
            
        }
        else if value == "Disputed" {
            
            revisionLabel.visibility = .invisible
            revisionLabel.visibility = .gone
            
            completedBtn.visibility = .invisible
            completedBtn.visibility = .gone
            
            cancelOrder.visibility = .invisible
            cancelOrder.visibility = .gone
            
            deliverOrderStatus.visibility = .invisible
            deliverOrderStatus.visibility = .gone
            
            downloadSourceBtn.visibility = .invisible
            downloadSourceBtn.visibility = .gone
            
            sellerDeliveryNote.visibility = .invisible
            deliveryNoteLabel.visibility = .gone
        }
        
        
        self.getBuyerOrdersDetail()
        self.setUpDisputeBtn()
        self.setUpRevisionBtn()
        self.setUpCompletedBtn()
        self.downloadImagePressed()
        self.setupWebDisputeBtn()
        
    }
    
    
    func fillData(){
        
        if let data = orderDetailArr{
            
            self.order.text = data.first?.orderNo
            self.orderDescription.text = data.first?.offer_description
            self.buyer.text = data.first?.payer_name
            
            let str = "$"
            self.price.text = (data.first?.price ?? "") + str
            
            let days = " Days"
            self.orderDuration.text = (data.first?.duration ?? "") + days
            
            self.statusOrderid = data.first?.order_id
        }
    }
    
    func setUpDisputeBtn(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.disputePressed(_ :)))
        self.cancelOrder.isUserInteractionEnabled = true
        self.cancelOrder.addGestureRecognizer(labelTap)
    }
    
    @objc func disputePressed(_ sender: UITapGestureRecognizer) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "manageOrderDispute") as! ManageOrderDisputeController
        vc.navigation = self.navigationController
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.orderId = self.orderDetailArr?.first?.payment_id
        vc.orderType = self.type
        vc.disputeArray = self.disputeArr
        self.navigationController?.present(vc, animated: true, completion: nil)
        vc.dismissController = self
    }
    
    func setUpRevisionBtn(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.revisionPressed(_ :)))
        self.revisionLabel.isUserInteractionEnabled = true
        self.revisionLabel.addGestureRecognizer(labelTap)
    }
    
    @objc func revisionPressed(_ sender: UITapGestureRecognizer) {
        
        self.RevisionPressed()
        
    }
    
    func setUpCompletedBtn(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.completedPressed(_ :)))
        self.completedBtn.isUserInteractionEnabled = true
        self.completedBtn.addGestureRecognizer(labelTap)
    }
    
    @objc func completedPressed(_ sender: UITapGestureRecognizer) {
        
        self.completedOrderPressed()
        
    }
    
    func setupWebDisputeBtn(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.disputeWebPressed(_ :)))
        self.disputeBtn.isUserInteractionEnabled = true
        self.disputeBtn.addGestureRecognizer(labelTap)
    }
    
    @objc func disputeWebPressed(_ sender: UITapGestureRecognizer) {
        
        let alertController = UIAlertController(title: "Skillsquared", message: "Please visit Skillquared.com to proceed dispute.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            let url = "https://www.skillsquared.com"
            let nsurl = NSURL(string: url)
            UIApplication.shared.canOpenURL(nsurl! as URL)
            UIApplication.shared.open(nsurl! as URL)
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func downloadImagePressed(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.downloadPressed(_ :)))
        self.downloadSourceBtn.isUserInteractionEnabled = true
        self.downloadSourceBtn.addGestureRecognizer(labelTap)
    }
    
    @objc func downloadPressed(_ sender: UITapGestureRecognizer) {
        
        imageActionSheet()
    }
    
    
    func imageActionSheet() {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let pickFromCamera = UIAlertAction(title: "Save image to photos", style: .default)
        { _ in
            
            SVProgressHUD.show(withStatus: "Downloading...")
            
            let ImageURLString = self.imageUrl
            print(ImageURLString ?? "")
            guard let yourImageURL = URL(string: ImageURLString!) else { return }
            
            self.getDataFromUrl(url: yourImageURL) { (data, response, error) in
                
                guard let data = data, let imageFromData = UIImage(data: data) else { return }
                
                UIImageWriteToSavedPhotosAlbum(imageFromData, nil, nil, nil)
                SVProgressHUD.showSuccess(withStatus: "Completed")
            }
            
        }
        
        actionSheetController.addAction(pickFromCamera)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
        }.resume()
    }
    
    func didJobPosted() {
        self.navigationController!.popViewController(animated: true)
    }
    
}

extension ActiveOrderDetailController{
    
    func getBuyerOrdersDetail(){
        
        SVProgressHUD.show()
        
        let url = "orderDetailBuyer"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let id =  orderId
        
        let params = ["order_id":id!,
                      "type":type
            
            ] as [String : AnyObject]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<BuyerOrderDetailObj>().map(JSONString: jsonStr!)
                print(responseModel as Any)
                
                self.orderDetailArr = responseModel?.sellerorderdetail
                self.disputeArr = responseModel?.disputereason
                self.deliverOrderStatus.text = responseModel?.orderStatus ?? ""
                self.imageUrl = responseModel?.deliveryNote?.file
                self.fillData()
                self.sellerDeliveryNote.text = responseModel?.deliveryNote?.description ?? ""
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}


extension ActiveOrderDetailController{
    
    func RevisionPressed(){
        
        SVProgressHUD.show()
        
        let url = "buyerChangeOrderStatus"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let params = ["order_id":orderId,
                      "status":"5"
            
            ] as [String : AnyObject]
        
        print(params)
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let message = dictObject["message"] as! String
                    let status = dictObject["status"] as! Int
                    
                    
                    if status == 200 {
                        let alertController = UIAlertController(title: "Title", message: (String(describing: message)), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else{
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                }
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
            
        }
    }
}


extension ActiveOrderDetailController{
    
    func completedOrderPressed(){
        
        SVProgressHUD.show()
        
        let url = "buyerChangeOrderStatus"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let params = ["order_id":orderId,
                      "status":"3"
            
            ] as [String : AnyObject]
        
        print(params)
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let message = dictObject["message"] as! String
                    let status = dictObject["status"] as! Int
                    
                    
                    if status == 200 {
                        
                        let reviewController = self.storyboard?.instantiateViewController(withIdentifier: "submitReviewVC") as! SubmitReviewController
                        reviewController.navigation = self.navigationController
                        reviewController.providesPresentationContextTransitionStyle = true
                        reviewController.definesPresentationContext = true
                        reviewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                        reviewController.order_id =  self.orderDetailArr?.first?.order_id
                        reviewController.service_id = self.orderDetailArr?.first?.service_id
                        self.navigationController?.present(reviewController, animated: true, completion: nil)
                        reviewController.postdismiss = self
                        
                        let alertController = UIAlertController(title: "Title", message: (String(describing: message)), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                        
                    else{
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}
