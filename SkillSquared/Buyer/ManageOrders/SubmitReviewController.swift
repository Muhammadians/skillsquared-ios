//
//  SubmitReviewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 05/06/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import FloatRatingView
import Alamofire
import SVProgressHUD
import SwiftyJSON

class SubmitReviewController: UIViewController, UITextViewDelegate {
    
    
    @IBOutlet weak var reviewRatting: FloatRatingView!
    @IBOutlet weak var reviewDetail: UITextView!
    @IBOutlet weak var characterCount: UILabel!
    
    
    var order_id: String!
    var service_id: String!
    
    var navigation : UINavigationController!
    var postdismiss : postrequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reviewDetail.delegate = self
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        characterCount.text = ("\(numberOfChars)\(" /")\(" 100")")
        return numberOfChars < 100
    }
    
    @IBAction func okTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("Review", comment: ""), message: NSLocalizedString("Are you sure you did not want to review seller", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
            switch action.style{
            case .default:
                
                self.dismiss(animated: true) {
                    self.postdismiss?.didJobPosted()
                }
                
            case .cancel: break
                
            case .destructive: break
            @unknown default:
                fatalError()
            }}))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel",comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        
        SubmitReviewPressed()
        
    }
}


extension SubmitReviewController{
    
    
    func SubmitReviewPressed(){
        
        SVProgressHUD.show()
        
        if reviewRatting.rating == 0.0 {
            
            showAlertView("Alert", message: "Please select rating")
        }
            
        else {
            
            
            let url = "publishReview"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            let headers: HTTPHeaders = [
                "accesstoken": Constants.accessToken
            ]
            
            let params = ["order_id": order_id!,
                          "service_id": service_id ?? "",
                          "rating": reviewRatting.rating,
                          "review": reviewDetail.text ?? ""
                
                ] as [String : AnyObject]
            
            print(params)
            
            Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
                
                print(response.request as Any)  // original url request
                print(response)  // http url reponse
                
                if response.result.isSuccess {
                    SVProgressHUD.dismiss()
                    
                    let jsonData = JSON(response.result.value!)
                    
                    if let dictObject = jsonData.dictionaryObject {
                        print(dictObject)
                        
                        let message = dictObject["message"] as! String
                        let status = dictObject["status"] as! Int
                        
                        
                        if status == 200 {
                            
                            let alertController = UIAlertController(title: "Title", message: (String(describing: message)), preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                                self.dismiss(animated: true, completion: {
                                    
                                    self.postdismiss?.didJobPosted()
                                    
                                })
                            }
                            
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else{
                            
                            let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                            alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                    }
                    
                }else if response.result.isFailure{
                    print(response)
                    SVProgressHUD.dismiss()
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
                }
                
            }
        }
    }
}
