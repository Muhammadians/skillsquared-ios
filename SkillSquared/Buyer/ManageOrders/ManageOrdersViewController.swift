//
//  ManageOrdersViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 28/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher
import SwiftyJSON
import SVProgressHUD
import ObjectMapper
import ScrollableSegmentedControl

class ManageOrdersViewController: UIViewController {
    
    @IBOutlet weak var segmentController: ScrollableSegmentedControl!
    @IBOutlet weak var ordersTableView: UITableView!
    
    var selectedString = ""
    var activeArray: Array<BuyerAactive>?
    var manageArray: Array<Amanage>?
    var completedArray: Array<BuyerAcompleted>?
    var revisionArray: Array<BuyerArevsion>?
    var disputedArray: Array<BuyerAdisputed>?
    
    var selectedIndex: Int = 0
    var id:String!
    var ord_id:String!
    var orderType:String!
    var orderValue:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ordersTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        
        segmentController.segmentStyle = .textOnly
        segmentController.segmentContentColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        segmentController.selectedSegmentContentColor = #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1)
        segmentController.backgroundColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        segmentController.borderColor = #colorLiteral(red: 0, green: 0.5628422499, blue: 0.3188166618, alpha: 1)
        
        segmentController.insertSegment(withTitle: "ACTIVE", at: 0)
        segmentController.insertSegment(withTitle: "MANAGE", at: 1)
        segmentController.insertSegment(withTitle: "COMPLETED",  at: 2)
        segmentController.insertSegment(withTitle: "REVISION",  at: 3)
        segmentController.insertSegment(withTitle: "DISPUTED",  at: 4)
        
        segmentController.fixedSegmentWidth = false
        segmentController.underlineSelected = true
        segmentController.selectedSegmentIndex = 0
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        manageArray = []
        activeArray = []
        completedArray = []
        revisionArray = []
        getOrdersDetails()
        
    }
    
    
    @IBAction func segmentIndexChanged(_ sender:ScrollableSegmentedControl) {
        
        switch sender.selectedSegmentIndex {
            
        case 0:
            let case0 = "Active"
            self.selectedString = case0
            print(selectedString)
            ordersTableView.reloadData()
            
        case 1:
            let case1 = "Delivered"
            self.selectedString = case1
            print(selectedString)
            ordersTableView.reloadData()
            
        case 2:
            let case2 = "Completed"
            self.selectedString = case2
            print(selectedString)
            ordersTableView.reloadData()
            
        case 3:
            let case3 = "Revision"
            self.selectedString = case3
            print(selectedString)
            ordersTableView.reloadData()
            
        case 4:
            let case4 = "Disputed"
            self.selectedString = case4
            ordersTableView.reloadData()
            
        default:
            break
            
        }
    }
}

extension ManageOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var value: Int = 0
        
        if selectedString == "Active" || segmentController.selectedSegmentIndex == 0 {
            if activeArray?.count ?? 0 > 0{
                self.ordersTableView.restore()
                value = activeArray?.count ?? 0
            }
            else{
                self.ordersTableView.setEmptyMessage("No Data Found.")
                return 0
            }
        }
            
        else if selectedString == "Delivered"{
            
            if manageArray?.count ?? 0 > 0{
                self.ordersTableView.restore()
                value = manageArray?.count ?? 0
            }
            else{
                self.ordersTableView.setEmptyMessage("No Data Found.")
                return 0
            }
        }
            
        else if selectedString == "Completed"{
            
            if completedArray?.count ?? 0 > 0{
                self.ordersTableView.restore()
                value = completedArray?.count ?? 0
            }
            else{
                self.ordersTableView.setEmptyMessage("No Data Found.")
                return 0
            }
        }
            
        else if selectedString == "Revision"{
            
            if revisionArray?.count ?? 0 > 0{
                self.ordersTableView.restore()
                value = revisionArray?.count ?? 0
            }
            else{
                self.ordersTableView.setEmptyMessage("No Data Found.")
                return 0
            }
        }
            
        else if selectedString == "Disputed"{
            
            if disputedArray?.count ?? 0 > 0{
                self.ordersTableView.restore()
                value = disputedArray?.count ?? 0
            }
            else{
                self.ordersTableView.setEmptyMessage("No Data Found.")
                return 0
            }
        }
        
        return value
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "saleCell", for: indexPath) as! OrderCell
        
        if selectedString == "Active" || segmentController.selectedSegmentIndex == 0{
            
            if let activeDetails = activeArray{
                
                cell.userImage.kf.setImage(with: URL(string: activeDetails[indexPath.row].sellerImage ?? ""), placeholder: UIImage(named: "Placeholder"))
                cell.saleUsername.text = activeDetails[indexPath.row].sellerName
                cell.saleDate.text = activeDetails[indexPath.row].created_on
                
                cell.saleDescription.text = activeDetails[indexPath.row].seller_service_info?.title ?? ""
                
                let price = activeDetails[indexPath.row].offerPrice ?? ""
                let str = "Price: "
                cell.salePrice.text   =  str + (price) + ("$")
                cell.statusText.text = "Active"
            }
        }
            
        else if selectedString == "Delivered"{
            
            if let manageArray = manageArray{
                
                cell.userImage.kf.setImage(with: URL(string: manageArray[indexPath.item].sellerImage ?? ""), placeholder: UIImage(named: "Placeholder"))
                cell.saleDate.text = manageArray[indexPath.row].created_on
                cell.saleUsername.text = manageArray[indexPath.row].sellerName
                cell.saleDescription.text = manageArray[indexPath.row].seller_service_info?.title ?? ""
                
                let price = manageArray[indexPath.item].offerPrice ?? ""
                let str = "Price: "
                cell.salePrice.text   =  str + (price) + ("$")
                cell.statusText.text = "Delivered"
            }
        }
            
        else if selectedString == "Completed"{
            
            if let completedDetails = completedArray{
                
                cell.userImage.kf.setImage(with: URL(string: completedDetails[indexPath.item].sellerImage!), placeholder: UIImage(named: "Placeholder"))
                cell.saleUsername.text = completedDetails[indexPath.item].sellerName
                cell.saleDate.text = completedDetails[indexPath.row].created_on
                cell.saleDescription.text = completedDetails[indexPath.row].seller_service_info?.title ?? ""
                
                let price = completedDetails[indexPath.item].offerPrice ?? ""
                let str = "Price: "
                cell.salePrice.text   =  str + (price) + ("$")
                cell.statusText.text = "Completed"
            }
        }
            
        else if selectedString == "Revision"{
            
            if let revisionDetails = revisionArray{
                
                cell.userImage.kf.setImage(with: URL(string: revisionDetails[indexPath.item].sellerImage ?? ""), placeholder: UIImage(named: "Placeholder"))
                cell.saleUsername.text = revisionDetails[indexPath.item].sellerName
                cell.saleDate.text = revisionDetails[indexPath.row].created_on
                cell.saleDescription.text = revisionDetails[indexPath.row].seller_service_info?.title ?? ""
                
                let price = revisionDetails[indexPath.item].offerPrice ?? ""
                let str = "Price: "
                cell.salePrice.text   =  str + (price) + ("$")
                cell.statusText.text = "Revision"
            }
        }
            
        else if selectedString == "Disputed"{
            
            if let disputedDetails = disputedArray{
                
                cell.userImage.kf.setImage(with: URL(string: disputedDetails[indexPath.item].sellerImage ?? ""), placeholder: UIImage(named: "Placeholder"))
                cell.saleUsername.text = disputedDetails[indexPath.item].sellerName
                cell.saleDate.text = disputedDetails[indexPath.row].created_on
                cell.saleDescription.text = disputedDetails[indexPath.row].seller_service_info?.title ?? ""
                
                let price = disputedDetails[indexPath.item].offerPrice ?? ""
                let str = "Price: "
                cell.salePrice.text   =  str + (price) + ("$")
                cell.statusText.text = "Disputed"
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        
        if selectedString == "Active" || segmentController.selectedSegmentIndex == 0{
            
            self.ord_id = activeArray?[selectedIndex].payment_order_id
            self.orderValue = "Active"
            
            if activeArray?[selectedIndex].seller_offer_request_id == nil {
                self.orderType = "custom"
            }
                
            else {
                
                self.orderType = ""
            }
            
            performSegue(withIdentifier: "OrderDetailSegue", sender: self)
        }
            
            
        else if selectedString == "Delivered" {
            
            self.ord_id = manageArray?[selectedIndex].payment_order_id ?? ""
            self.orderValue = "Delivered"
            
            if manageArray?[selectedIndex].seller_offer_request_id == nil {
                self.orderType = "custom"
            }
                
            else {
                
                self.orderType = ""
            }
            
            performSegue(withIdentifier: "OrderDetailSegue", sender: self)
        }
            
        else if selectedString == "Completed"{
            
            self.ord_id = completedArray?[selectedIndex].payment_order_id ?? ""
            self.orderValue = "Completed"
            
            if completedArray?[selectedIndex].seller_offer_request_id == nil {
                self.orderType = "custom"
            }
            else{
                
                self.orderType = ""
            }
            
            performSegue(withIdentifier: "OrderDetailSegue", sender: self)
        }
            
        else if selectedString == "Revision"{
            
            self.ord_id = revisionArray?[selectedIndex].payment_order_id ?? ""
            self.orderValue = "Revision"
            
            if revisionArray?[selectedIndex].seller_offer_request_id == nil {
                self.orderType = "custom"
            }
            else{
                
                self.orderType = ""
            }
            
            performSegue(withIdentifier: "OrderDetailSegue", sender: self)
        }
            
        else if selectedString == "Disputed" {
            
            self.ord_id = disputedArray?[selectedIndex].payment_order_id ?? ""
            self.orderValue = "Disputed"
            
            if disputedArray?[selectedIndex].seller_offer_request_id == nil {
                self.orderType = "custom"
            }
            else{
                
                self.orderType = ""
            }
            
            performSegue(withIdentifier: "OrderDetailSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if selectedString == "Active" || segmentController.selectedSegmentIndex == 0 {
            
            if segue.identifier == "OrderDetailSegue" {
                let vc = segue.destination as! ActiveOrderDetailController
                vc.selectedIndexValue = self.selectedIndex
                vc.orderId = self.ord_id
                vc.type = self.orderType
                vc.value = self.orderValue
            }
        }
            
        else if selectedString == "Delivered" || selectedString == "Completed" || selectedString == "Revision" || selectedString == "Disputed" {
            
            if segue.identifier == "OrderDetailSegue" {
                let vc = segue.destination as! ActiveOrderDetailController
                vc.selectedIndexValue = self.selectedIndex
                vc.orderId = self.ord_id
                vc.type = self.orderType
                vc.value = self.orderValue
            }
        }
    }
}

extension ManageOrdersViewController{
    
    func getOrdersDetails()  {
        
        SVProgressHUD.show()
        
        let url = "buyerPlacedOrdersMerged"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<ManageOrderObject>().map(JSONString: jsonStr!)
                print(responseModel as Any)
                
                self.activeArray = responseModel?.buyerOrders?.aactive
                self.manageArray = responseModel?.buyerOrders?.amanage
                self.completedArray = responseModel?.buyerOrders?.acompleted
                self.revisionArray = responseModel?.buyerOrders?.arevsion
                self.disputedArray = responseModel?.buyerOrders?.adisputed
                self.ordersTableView.reloadData()
                
            }
                
            else if response.result.isFailure {
                
                print(response)
                
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}
