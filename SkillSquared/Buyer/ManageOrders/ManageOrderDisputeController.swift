//
//  ManageOrderDisputeController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 19/03/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import iOSDropDown
import SVProgressHUD
import Alamofire
import SwiftyJSON
import ObjectMapper


class ManageOrderDisputeController: UIViewController {
    
    @IBOutlet weak var disputeDescription: DropDown!
    @IBOutlet weak var requestAdminRefund: UITextView!
    
    var navigation : UINavigationController!
    var dismissController : postrequest?
    var orderId:String!
    var disputeReason:String!
    var reasonId:String!
    var orderType:String!
    
    var disputeArray: Array<Disputereason>?
    var sellerOrder: Array<Sellerorderdetail>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getOrderData()
    }
    
    
    func descriptionDrop(mainlist:[String])
    {
        disputeDescription.optionArray = mainlist
        disputeDescription.didSelect{(selectedText , index ,id) in
            
            self.disputeReason = selectedText
            let checkValue = selectedText
            
            if let mainArray = self.disputeArray{
                for i in 0..<mainArray.count{
                    if (mainArray[i].title == checkValue){
                        self.reasonId = mainArray[i].id
                        print(self.reasonId!)
                    }
                }
            }
        }
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func submitTapped(_ sender: Any) {
        submitOrder()
    }
}

extension ManageOrderDisputeController{
    
    func getOrderData(){
        
        SVProgressHUD.show()
        
        let url = "orderDetailBuyer"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let id =  orderId
        
        let params = ["order_id":id!] as [String : AnyObject]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<OrderDetailObj>().map(JSONString: jsonStr!)
                print(responseModel as Any)
                
                self.disputeArray = responseModel?.disputereason
                
                
                var arrayList = [String]()
                let data = self.disputeArray
                for i in 0..<data!.count{
                    arrayList.append(data?[i].title ?? "")
                }
                self.descriptionDrop(mainlist: arrayList)
                
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}

extension ManageOrderDisputeController{
    
    func submitOrder() {
        
        SVProgressHUD.show()
        
        let url = "generetedisputeFromBuyer"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let id =  orderId
        let reason_id = reasonId
        let requestMessage = requestAdminRefund.text
        let type = orderType
        
        let params = ["order_id":id, "disputereason_id":reason_id, "disputerequestmessage": requestMessage, "ordertype": type] as [String : AnyObject]
        
        print(params)
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
        
            switch response.result {
                
            case .success(let json):
                print(json)
                
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let message = dictObject["message"] as! String
                    let status = dictObject["status"] as! Int
                    
                    if status == 200 {
                        let alertController = UIAlertController(title: "Title", message: (String(describing: message)), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            self.dismiss(animated: true, completion: {
                                
                                self.dismissController?.didJobPosted()
                            })
                        }
                        
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                        
                    else{
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
            case .failure(let error):
                SVProgressHUD.dismiss()
                print(error)
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
                
            }
        }
    }
}



