//
//  freeLancerForm1VC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 13/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import SwiftyJSON
import ObjectMapper
import SVProgressHUD

class FreeLancerForm1VC: UIViewController {
    

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var freeLancertitle: UITextField!
    @IBOutlet weak var countryDropDown: DropDown!
    @IBOutlet weak var mainCategoryDropDown: DropDown!
    @IBOutlet weak var freeLancerDescription: UITextView!
    @IBOutlet weak var subCategoryDropDown: DropDown!
    @IBOutlet weak var subCatChild: DropDown!
    @IBOutlet weak var stateDropDown: DropDown!
    @IBOutlet weak var cityDropDown: DropDown!
    @IBOutlet weak var mainCategoryView: UIView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var subChildView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var cityView: UIView!
    
    var countryName : Array<Countries>?
    var catMainArray : Array<Catoptions>?
    var subCat : Array<Subcat>?
    var subCatChildArray : Array<Subcat_child>?
    var languageArray = [String]()
    var statesArray : Array<stateArr>?
    var cityArray: Array<CityArrObj>?
    
    var mainCatTxt = ""
    var subCattxt = ""
    var subCatChildtxt = ""
    var catid = ""
    
    var selectedCountry = ""
    var selectedCountryId = ""
    
    var selectedState = ""
    var Stateid = ""
    
    var selectedCity = ""
    var selectedCityid = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        subView.visibility = .invisible
        subView.visibility = .gone
        
        subChildView.visibility = .invisible
        subChildView.visibility = .gone
        
        stateView.visibility = .invisible
        stateView.visibility = .gone
        
        cityView.visibility = .invisible
        cityView.visibility = .gone
        
        getFreeLancerData()
    }
    
    @IBAction func navigateToSecondForm(_ sender: Any) {
        
        if freeLancertitle.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter title")
        }
        else if selectedCountryId.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please select country")
        }
        else if Stateid.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please select state")
        }
        else if selectedCityid.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please select city")
        }
        else if catid.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please select category")
        }
        else if freeLancerDescription.text.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter description")
        }
            
        else{
            
            let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "form2") as! FreeLancerForm2VC
            navigationController.langArr = languageArray
            navigationController.titleString = freeLancertitle.text!
            navigationController.country = selectedCountryId
            navigationController.state = Stateid
            navigationController.city = selectedCityid
            navigationController.catid = catid
            navigationController.des = freeLancerDescription.text!
            navigationController.name = userName.text ?? ""
            self.navigationController?.pushViewController(navigationController, animated: true)
        }
    }
    
    
    func MainCatDropDown(List:[String])
    {
        mainCategoryDropDown.optionArray = List
        mainCategoryDropDown.didSelect{(selectedText , index ,id) in
            
            self.mainCatTxt = selectedText
            print("\(self.mainCatTxt)")
            let checkVal2 = selectedText
            
            if let mainArray = self.catMainArray{
                for i in 0..<mainArray.count{
                    if (mainArray[i].title == checkVal2){
                        if (mainArray[i].subcat != nil) {
                            self.subCat = mainArray[i].subcat
                            self.subView.visibility = .visible
                        } else {
                            self.catid = mainArray[i].cat_id ?? ""
                            print(self.catid)
                        }
                    }
                }
            }
            var subCategory = [String]()
            if let subCategoryArray = self.subCat{
                for i in 0..<subCategoryArray.count{
                    subCategory.append(subCategoryArray[i].title ?? "")
                    self.catid = subCategoryArray[i].id ?? ""
                    //                    print("\(self.catId)")
                }
                self.subCatDropDown(subList: subCategory)
            }
        }
    }
    
    func subCatDropDown(subList:[String])
    {
        subCategoryDropDown.optionArray = subList
        subCategoryDropDown.didSelect{(selectedText , index ,id) in
            
            self.subCattxt = selectedText
            print("\(self.subCattxt)")
            let checkVal2 = selectedText
            
            if let subCategoryArray = self.subCat{
                for i in 0..<subCategoryArray.count{
                    if (subCategoryArray[i].title == checkVal2){
                        if (subCategoryArray[i].subcat_child != nil) {
                            self.subCatChildArray = subCategoryArray[i].subcat_child
                            self.subChildView.visibility = .visible
                        } else {
                            self.catid = subCategoryArray[i].id ?? ""
                            print(self.catid)
                        }
                    }
                }
            }
            var subCategoryChild = [String]()
            if let subChildArray = self.subCatChildArray{
                for i in 0..<subChildArray.count{
                    subCategoryChild.append(subChildArray[i].title ?? "")
                    self.catid = subChildArray[i].id ?? ""
                    //                    print("\(self.catId)")
                }
                self.subCatChildDropDown(subChildList: subCategoryChild)
            }
        }
    }
    
    func subCatChildDropDown(subChildList:[String])
    {
        subCatChild.optionArray = subChildList
        subCatChild.didSelect{(selectedText , index ,id) in
            
            //
            let checkVal2 = selectedText
            
            for i in 0..<self.subCatChildArray!.count{
                if (self.subCatChildArray![i].title == checkVal2){
                    self.catid = self.subCatChildArray![i].id ?? ""
                    print(self.catid)
                }
            }
        }
    }
    
    
    func getCountry(List:[String])
    {
        countryDropDown.optionArray = List
        countryDropDown.didSelect{(selectedText , index ,id) in
            self.selectedCountry = selectedText
            print("\(self.selectedCountry)")
            
            let CheckValue = selectedText
            
            if let countryArray = self.countryName{
                for i in 0..<countryArray.count{
                    if (countryArray[i].name == CheckValue){
                        self.selectedCountryId = countryArray[i].id ?? ""
                        print(self.selectedCountryId)
                        self.getStates()
                        self.stateView.visibility = .visible
                    }
                }
            }
        }
    }
    
    func getStatesName(List:[String])
    {
        stateDropDown.optionArray = List
        stateDropDown.didSelect{(selectedText , index ,id) in
            
            self.selectedState = selectedText
            print("\(self.selectedState)")
            
            let CheckValue = selectedText
            
            if let stateArr = self.statesArray{
                for i in 0..<stateArr.count{
                    if (stateArr[i].name == CheckValue){
                        self.Stateid = stateArr[i].id ?? ""
                        print(self.Stateid)
                        self.getCities()
                        self.cityView.visibility = .visible
                    }
                }
            }
        }
    }
    
    func getCitiesName(List:[String])
    {
        cityDropDown.optionArray = List
        cityDropDown.didSelect{(selectedText , index ,id) in
            
            self.selectedCity = selectedText
            print("\(self.selectedCity)")
            
            let CheckValue = selectedText
            
            if let cityArr = self.cityArray{
                for i in 0..<cityArr.count{
                    if (cityArr[i].name == CheckValue){
                        self.selectedCityid = cityArr[i].id ?? ""
                        print(self.selectedCityid)
                    }
                }
            }
        }
    }
}

extension FreeLancerForm1VC{
    
    func getFreeLancerData(){
        
        SVProgressHUD.show()
        
        let url = "becomeafreelancer"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            print(response) // http url reponse
            if response.result.isSuccess {
                
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<MainObject>().map(JSONString: jsonStr!)
                print("\(String(describing: responseModel))")
                
                self.catMainArray = responseModel?.catoptions
                self.countryName = responseModel?.countries
                let langArr = responseModel?.languages
                self.languageArray = langArr ?? [""]
                self.userName.text = responseModel?.userMainInfo?.name
                print(self.userName.text as Any)
                
                // Get Main Category name
                var arrayList = [String]()
                let data = responseModel?.catoptions
                for i in 0..<data!.count{
                    arrayList.append(data?[i].title ?? "")
                }
                self.MainCatDropDown(List: arrayList)
                
                // Get Country names
                var CountryList = [String]()
                let name = responseModel?.countries
                for i in 0..<name!.count{
                    CountryList.append(name?[i].name ?? "")
                }
                
                let reversedNames : [String] = Array(CountryList.reversed())
                self.getCountry(List: reversedNames)
                
            }else if response.result.isFailure{
                print(response)
                
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
    
    func getStates(){
        
        
        SVProgressHUD.show()
        
        let url = "get_cityStates"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let params = [
            "hint": "1",
            "id": selectedCountryId,] as [String : Any]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            print(response) // http url reponse
            if response.result.isSuccess {
                
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                do{
                    let responseObject = try JSONDecoder().decode([stateArr].self, from: jsonData)
                    print("\(responseObject)")
                    
                    self.statesArray = responseObject
                    var arrayList = [String]()
                    let data = responseObject
                    for i in 0..<data.count{
                        arrayList.append(data[i].name ?? "")
                    }
                    
                    let reversedNames : [String] = Array(arrayList.reversed())
                    self.getStatesName(List: reversedNames)
                    
                }
                catch{
                    print(error.localizedDescription)
                }
                
            }else if response.result.isFailure{
                print(response)
                
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
    
    func getCities(){
        
        
        SVProgressHUD.show()
        
        let url = "get_cityStates"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let params = [
            "hint": "2",
            "id": Stateid,] as [String : Any]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            print(response) // http url reponse
            if response.result.isSuccess {
                
                
                SVProgressHUD.dismiss()

                
                guard let jsonData = response.data else{return}
                do{
                    let responseObject = try JSONDecoder().decode([CityArrObj].self, from: jsonData)
                    print("\(responseObject)")
                    
                    self.cityArray = responseObject
                    
                    var arrayList = [String]()
                    let data = responseObject
                    for i in 0..<data.count{
                        arrayList.append(data[i].name ?? "")
                    }
                    
                    let reversedNames : [String] = Array(arrayList.reversed())
                    self.getCitiesName(List: reversedNames)
                }
                catch{
                    print(error.localizedDescription)
                }
                
            }else if response.result.isFailure{
                print(response)
                
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}

