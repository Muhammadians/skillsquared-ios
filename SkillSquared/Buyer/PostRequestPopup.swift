//
//  PostRequestPopup.swift
//  SkillSquared
//
//  Created by Awais Aslam on 30/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit


protocol postrequest {
    func didJobPosted()
}

class PostRequestPopup: UIViewController {
    
    var navigation : UINavigationController!
    var postdismiss : postrequest?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func RequestPopUpClose(_ sender: Any) {
        
        self.dismiss(animated: true, completion: {
        
            self.postdismiss?.didJobPosted()
    
        })
    }
}
