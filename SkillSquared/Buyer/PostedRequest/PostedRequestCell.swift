//
//  PostedRequestCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 29/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

protocol DeletePostedRequest {
    func deleteDataBuyer(indx: Int)
    func passDataBuyer(indx: Int)
}


class PostedRequestCell: UICollectionViewCell {
    @IBOutlet weak var requestDate: UILabel!
    @IBOutlet weak var requestDurationDays: UILabel!
    @IBOutlet weak var postDescription: UILabel!
    @IBOutlet weak var status: PaddingLabel!
    @IBOutlet weak var totalOffers: UILabel!
    @IBOutlet weak var totalBudget: UILabel!
    @IBOutlet weak var totalOffersButton: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
    var delegate: DeletePostedRequest?
    var index: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func deleteCell(_ sender: Any) {
        
        delegate?.deleteDataBuyer(indx: (index?.row)!)
    }
}
