//
//  PostedRequestVC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 29/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import ObjectMapper

class PostedRequestVC: UIViewController {
    
    @IBOutlet weak var postedRequestCollectionView: UICollectionView!
    
    var approved: Array<Approve>?
    var unapproved: Array<Unapproved>?
    var pending: Array<Pending>?
    
    var filterValue = String()
    var catId = ""
    var deleteindex: Int = 0
    var catDeleteId = ""
    
    var postRequestFlowLayout: UICollectionViewFlowLayout!
    let requestsCollectionViewIdentifier = "RequestCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        approved = []
        unapproved = []
        pending = []
        SetUpCollectionView()
        getAllpost()
    }
    
    @IBAction func FilterButtonPopUp(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    private func SetUpCollectionView(){
        postedRequestCollectionView.delegate = self
        postedRequestCollectionView.dataSource = self
        let Requestnib = UINib(nibName: "PostedRequestCell", bundle: nil)
        postedRequestCollectionView.register(Requestnib, forCellWithReuseIdentifier: requestsCollectionViewIdentifier)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        PostRequestCollectionViewSize()
    }
    
    private func PostRequestCollectionViewSize() {
        
        if postRequestFlowLayout == nil {
            
            
            let lineSpacing: CGFloat = 10
            let interItemSpacing: CGFloat = 0
            
            
            let width =  UIScreen.main.bounds.width
            
            postRequestFlowLayout = UICollectionViewFlowLayout()
            postRequestFlowLayout.itemSize = CGSize(width: width - 30 , height: 140)
            postRequestFlowLayout.sectionInset = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
            postRequestFlowLayout.scrollDirection = .vertical
            postRequestFlowLayout.minimumLineSpacing = lineSpacing
            postRequestFlowLayout.minimumInteritemSpacing = interItemSpacing
            postedRequestCollectionView.setCollectionViewLayout(postRequestFlowLayout, animated: true)
        }
    }
    
    @IBAction func submitRequestTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "postARequest") as! PostRequestVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}

//extension PostedRequestVC: DeletePostedRequest{
//
////    func deleteDataBuyer(indx: Int) {
////
////        self.deleteindex = indx
////        if filterValue == "Unapproved" {
////            unapproved?.remove(at: indx)
////
////        }
////        if filterValue == "Pending" {
////            pending?.remove(at: indx)
////
////        }
////        else{approved?.remove(at: indx)}
////
////        deleteItem()
////
////        self.postedRequestCollectionView.reloadData()
////    }
//
//    func passDataBuyer(indx: Int) {
//    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "ViewOffersSegue" {
//            let vc = segue.destination as! ViewOffersVC
//            vc.catId = self.catId
//        }
//    }
//}

extension PostedRequestVC: UICollectionViewDelegate, UICollectionViewDataSource{
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ViewOffersSegue" {
            let vc = segue.destination as! ViewOffersVC
            vc.catId = self.catId
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var returnValue: Int = 0
        
        if filterValue == "Unapproved" {
            if unapproved?.count ?? 0 > 0{
                self.postedRequestCollectionView.restore()
                returnValue = unapproved?.count ?? 0
            }
            else{
                self.postedRequestCollectionView.setEmptyMessage("No Data Found.")
                return 0
            }
            
        } else if filterValue == "Pending" {
            
            if pending?.count ?? 0 > 0{
                self.postedRequestCollectionView.restore()
                returnValue = pending?.count ?? 0
            }
            else{
                self.postedRequestCollectionView.setEmptyMessage("No Data Found.")
                return 0
            }
        }
            
        else{
            if approved?.count ?? 0 > 0{
                self.postedRequestCollectionView.restore()
                returnValue = approved?.count ?? 0
            }
            else{
                self.postedRequestCollectionView.setEmptyMessage("No Data Found.")
                return 0
            }
        }
        
        return returnValue
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: requestsCollectionViewIdentifier, for: indexPath) as! PostedRequestCell
        
        
        if filterValue == "Unapproved" {
            cell.postDescription.text = unapproved![indexPath.row].description
            cell.requestDurationDays.text = unapproved![indexPath.row].delievry
            cell.requestDate.text = unapproved![indexPath.row].created_date
            cell.totalBudget.text = unapproved![indexPath.row].budget
            cell.totalOffers.text = "No offers yet"
            cell.totalOffersButton.isUserInteractionEnabled = false
            cell.status.text = "Unapproved"
            cell.status.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
            
            
            cell.index = indexPath
            //          cell.delegate = self
            
            let id = unapproved![indexPath.item].id ?? ""
            self.catDeleteId = id
            
        } else if filterValue == "Pending" {
            cell.postDescription.text = pending![indexPath.row].description
            cell.requestDurationDays.text = pending![indexPath.row].delievry
            cell.requestDate.text = pending![indexPath.row].created_date
            cell.totalBudget.text = pending![indexPath.row].budget
            cell.totalOffers.text = "No offers yet"
            cell.totalOffersButton.isUserInteractionEnabled = false
            cell.status.text = "Pending"
            cell.status.backgroundColor = #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            
            cell.index = indexPath
            //            cell.delegate = self
            
            let id = pending![indexPath.item].id ?? ""
            self.catDeleteId = id
            
        }
            
        else {
            
            cell.postDescription.text = approved![indexPath.row].description
            cell.requestDurationDays.text = approved![indexPath.row].delievry
            cell.requestDate.text = approved![indexPath.row].created_date
            
            let offers = approved![indexPath.item].totaloffer ?? ""
            cell.totalOffers.text = "Total Offers (" + offers + ")"
            
            
            if offers == "0"{
                cell.totalOffersButton.isUserInteractionEnabled = false
            }else{
                cell.totalOffersButton.isUserInteractionEnabled = true
            }
            
            cell.totalBudget.text = approved![indexPath.row].budget
            cell.status.text = "Approved"
            cell.status.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            
            cell.totalOffersButton.addTarget(self, action: #selector(btnTapped(_ :)), for: .touchUpInside)
            cell.totalOffersButton.tag = indexPath.row
            
            cell.index = indexPath
            //            cell.delegate = self
            
            cell.deleteBtn.addTarget(self, action: #selector(deleteBtnTapped(_ :)), for: .touchUpInside)
            cell.deleteBtn.tag = indexPath.row
            
            let id = approved![indexPath.row].id ?? ""
            self.catDeleteId = id
            
        }
        return cell
    }
    
    @objc func btnTapped(_ sender: UIButton){
        
        let id = approved![sender.tag].id ?? ""
        self.catId = id
        self.performSegue(withIdentifier: "ViewOffersSegue", sender: self)
    }
    
    
    @objc func deleteBtnTapped(_ sender: UIButton){
        
        
        let alert = UIAlertController(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("Are you sure you want to delete this offer?", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
            switch action.style{
            case .default:
                
                
                if self.filterValue == "Unapproved"
                
                {
                    
                    let id = self.unapproved?[sender.tag].id ?? ""
                    self.deleteRow(index: sender.tag)
                    self.deleteItem(ser_id: id)
                    
                }
                
                if self.filterValue == "Pending"
                
                {
                    
                    let id = self.pending?[sender.tag].id ?? ""
                    self.deleteRow(index: sender.tag)
                    self.deleteItem(ser_id: id)
                    
                }
                    
                else
                    
                {
                    let id = self.approved?[sender.tag].id ?? ""
                    self.deleteRow(index: sender.tag)
                    self.deleteItem(ser_id: id)
                    
                }
                
                
            case .cancel: break
                
            case .destructive: break
            @unknown default:
                fatalError()
            }}))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel",comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func deleteRow(index: Int){
        print("indx is : \(index)")
        print("one")
        
        if filterValue == "Unapproved" {
            unapproved?.remove(at: index)
            
        }
        if filterValue == "Pending" {
            pending?.remove(at: index)
            
        }
        else{approved?.remove(at: index)}
    }
}


extension PostedRequestVC{
    
    func getAllpost() {
        
        SVProgressHUD.show()
        
        let url = "buyerManageRequest"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .get, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<PostedRequestObj>().map(JSONString: jsonStr!)
                let data = responseModel?.buyerrequests
                
                self.approved = data?.approve
                self.unapproved = data?.unapproved
                self.pending = data?.pending
                self.postedRequestCollectionView.reloadData()
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}

extension PostedRequestVC: SendDataback {
    func backData(value: String) {
        filterValue = value
        print(value)
        postedRequestCollectionView.reloadData()
    }
}

extension PostedRequestVC {
    
    func deleteItem(ser_id: String) {
        
        SVProgressHUD.show()
        
        let url = "removesBuyerPostedRequest"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let params = [
            "id": ser_id
            ] as [String : Any]
        
        print(params)
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            var err:Error?
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            switch response.result {
                
            case .success(let json):
                
                SVProgressHUD.dismiss()
                
                print(json)
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let message = dictObject["message"] as! String
                    let status = dictObject["status"] as! Int
                    
                    if status == 200 {
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                        
                    else{
                        let alertController = UIAlertController(title: "Alert", message:(String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                    self.postedRequestCollectionView.reloadData()
                }
                
            case .failure(let error):
                SVProgressHUD.dismiss()
                err = error.localizedDescription as? Error
                print(err!)
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
                self.postedRequestCollectionView.reloadData()
                
            }
        }
    }
}
