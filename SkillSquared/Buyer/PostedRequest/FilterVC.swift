//
//  FilterVC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 29/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import DLRadioButton

protocol SendDataback {
    
    func backData(value: String)
}
class FilterVC: UIViewController {
    
    @IBOutlet weak var ActiveButton: DLRadioButton!
    @IBOutlet weak var PendingButton: DLRadioButton!
    @IBOutlet weak var UnUprovedButton: DLRadioButton!
    
    @IBOutlet weak var CloseButton: UIButton!
    
    var radioButtonText = ""
    
    var delegate: SendDataback?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func CloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func OnClickbtn2(_ sender: Any) {
        
        if  PendingButton.isSelected || UnUprovedButton.isSelected{
            
            PendingButton.isSelected = false
            UnUprovedButton.isSelected = false
            ActiveButton.isSelected = true
        }
        else{
            ActiveButton.isSelected = true
            radioButtonText.append(ActiveButton.titleLabel!.text!)
            print("\(radioButtonText)")
            delegate?.backData(value: radioButtonText)
            
        }
    }
    
    
    @IBAction func OnClickbtn3(_ sender: Any) {
        
        if  ActiveButton.isSelected || UnUprovedButton.isSelected{
            
            ActiveButton.isSelected = false
            UnUprovedButton.isSelected = false
            PendingButton.isSelected = true
        
        } else {
        
            PendingButton.isSelected = true
            radioButtonText.append(PendingButton.titleLabel!.text!)
            print("\(radioButtonText)")
            delegate?.backData(value: radioButtonText)
        }
    }
    
    @IBAction func OnClickbtn4(_ sender: Any) {
        
        if ActiveButton.isSelected || PendingButton.isSelected{
            
            ActiveButton.isSelected = false
            PendingButton.isSelected = false
            UnUprovedButton.isSelected = true
        }
        else{
            UnUprovedButton.isSelected = true
            radioButtonText.append(UnUprovedButton.titleLabel!.text!)
            print("\(radioButtonText)")
            delegate?.backData(value: radioButtonText)
        }
        
    }
    
}
