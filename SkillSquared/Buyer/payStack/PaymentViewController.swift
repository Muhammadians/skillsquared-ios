//
//  PaymentViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 29/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Paystack
import Alamofire
import SwiftyJSON

class PaymentViewController: UIViewController, PSTCKPaymentCardTextFieldDelegate {
    
    @IBOutlet weak var tokenLabel: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cardDetailForm: PSTCKPaymentCardTextField!
    
    let paymentTextField = PSTCKPaymentCardTextField()
    let paystackPublicKey = "pk_test_c09e1d31fde729bb340948942628f8a1dc40fc93"
    
    let backendURLString = "https://paystactesting.herokuapp.com"
    let card : PSTCKCard = PSTCKCard()
    
    override func viewDidLoad() {
        
        // hide token label and email box
        tokenLabel.text=nil
        saveBtn.isEnabled = false
        // clear text from card details
        // comment these to use the sample data set
        
        super.viewDidLoad()
    }
    
    @IBAction func closePopup(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    
    // MARK: Helpers
    func showOkayableMessage(_ title: String, message: String){
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: UIAlertController.Style.alert
        )
        let action = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func dismissKeyboardIfAny(){
        // Dismiss Keyboard if any
        cardDetailForm.resignFirstResponder()
        
    }
    
    // MARK: Actions
    @IBAction func cardDetailsChanged(_ sender: PSTCKPaymentCardTextField) {
        saveBtn.isEnabled = sender.isValid
    }
    
    
    func paymentCardTextFieldDidChange(_ textField: PSTCKPaymentCardTextField) {
        // Toggle navigation, for example
        saveBtn.isEnabled = textField.isValid
    }
    
    
    @IBAction func chargeBtn(_ sender: Any) {
        
        dismissKeyboardIfAny()
        
        // Make sure public key has been set
        if (paystackPublicKey == "" || !paystackPublicKey.hasPrefix("pk_")) {
            showOkayableMessage("You need to set your Paystack public key.", message:"You can find your public key at https://dashboard.paystack.co/#/settings/developer .")
            // You need to set your Paystack public key.
            return
        }
        
        Paystack.setDefaultPublicKey(paystackPublicKey)
        
        if cardDetailForm.isValid {
            
            if backendURLString != "" {
                fetchAccessCodeAndChargeCard()
                return
            }
            showOkayableMessage("Backend not configured", message:"To run this sample, please configure your backend.")
            //            chargeWithSDK(newCode:"");
            
        }
    }
    
    func fetchAccessCodeAndChargeCard(){
        
        Alamofire.request(backendURLString  + "/new-access-code").response{ response in
            print("Request: \(response.request!)")
            print("Response: \(response.response!)")
            print("Error: \(response.error!)")
            print("Timeline: \(response.timeline)")
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
                
                self.outputOnLabel(str: "Fetched access code: "+utf8Text)
                self.chargeWithSDK(newCode: utf8Text as NSString)
                
            }
        }
        
        //        if let url = URL(string: backendURLString  + "/new-access-code") {
        //            self.makeBackendRequest(url: url, message: "fetching access code", completion: { str in
        //                self.outputOnLabel(str: "Fetched access code: "+str)
        //                self.chargeWithSDK(newCode: str as NSString)
        //            })
        //        }
    }
    
    func outputOnLabel(str: String){
        DispatchQueue.main.async {
            if let former = self.tokenLabel.text {
                self.tokenLabel.text = former + "\n" + str
            } else {
                self.tokenLabel.text = str
            }
        }
    }
    
    func makeBackendRequest(url: URL, message: String, completion: @escaping (_ result: String) -> Void){
        let session = URLSession(configuration: URLSessionConfiguration.default)
        self.outputOnLabel(str: "Backend: " + message)
        session.dataTask(with: url, completionHandler: { data, response, error in
            let successfulResponse = (response as? HTTPURLResponse)?.statusCode == 200
            if successfulResponse && error == nil && data != nil {
                if let str = NSString(data: data!, encoding: String.Encoding.utf8.rawValue){
                    completion(str as String)
                } else {
                    self.outputOnLabel(str: "<Unable to read response> while "+message)
                    print("<Unable to read response>")
                }
            } else {
                if let e=error {
                    print(e.localizedDescription)
                    self.outputOnLabel(str: e.localizedDescription)
                } else {
                    // There was no error returned though status code was not 200
                    print("There was an error communicating with your payment backend.")
                    self.outputOnLabel(str: "There was an error communicating with your payment backend while "+message)
                }
            }
        }).resume()
    }
    
    func verifyTransaction(reference: String){
        if let url = URL(string: backendURLString  + "/verify/" + reference) {
            makeBackendRequest(url: url, message: "verifying " + reference, completion:{(str) -> Void in
                self.outputOnLabel(str: "Message from paystack on verifying " + reference + ": " + str)
            })
        }
    }
    
    func chargeWithSDK(newCode: NSString){
        print("Hello")
        let transactionParams = PSTCKTransactionParams.init();
        transactionParams.access_code = newCode as String;
        //transactionParams.additionalAPIParameters = ["enforce_otp": "true"];
        transactionParams.email = "ibrahim@paystack.co";
        transactionParams.amount = 100;
        
        // use library to create charge and get its reference
        PSTCKAPIClient.shared().chargeCard(self.cardDetailForm.cardParams, forTransaction: transactionParams, on: self, didEndWithError: { (error, reference) in
            self.outputOnLabel(str: "Charge errored")
            // what should I do if an error occured?
            print(error)
            if error._code == PSTCKErrorCode.PSTCKExpiredAccessCodeError.rawValue{
                // access code could not be used
                // we may as well try afresh
            }
            if error._code == PSTCKErrorCode.PSTCKConflictError.rawValue{
                // another transaction is currently being
                // processed by the SDK... please wait
            }
            if let errorDict = (error._userInfo as! NSDictionary?){
                if let errorString = errorDict.value(forKeyPath: "com.paystack.lib:ErrorMessageKey") as! String? {
                    if let reference=reference {
                        self.showOkayableMessage("An error occured while completing "+reference, message: errorString)
                        self.outputOnLabel(str: reference + ": " + errorString)
                        self.verifyTransaction(reference: reference)
                    } else {
                        self.showOkayableMessage("An error occured", message: errorString)
                        self.outputOnLabel(str: errorString)
                    }
                }
            }
            self.saveBtn.isEnabled = true;
        }, didRequestValidation: { (reference) in
            self.outputOnLabel(str: "requested validation: " + reference)
        }, willPresentDialog: {
            // make sure dialog can show
            // if using a "processing" dialog, please hide it
            self.outputOnLabel(str: "will show a dialog")
        }, dismissedDialog: {
            // if using a processing dialog, please make it visible again
            self.outputOnLabel(str: "dismissed dialog")
        }) { (reference) in
            self.outputOnLabel(str: "succeeded: " + reference)
            self.saveBtn.isEnabled = true;
            self.verifyTransaction(reference: reference)
        }
        return
    }
}
