//
//  BuyerRequestCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 30/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class BuyerRequestCell: UICollectionViewCell {
    
    @IBOutlet weak var buyerRequestName: UILabel!
    @IBOutlet weak var buyerRequestDate: UILabel!
    @IBOutlet weak var buyerRequestOffer: UILabel!
    @IBOutlet weak var buyerRequestDuration: UILabel!
    @IBOutlet weak var buyerRequestBudget: UILabel!
    @IBOutlet weak var buyerRequestCriteria: UILabel!
    @IBOutlet weak var buyerDescription: UITextView!
    @IBOutlet weak var SendOfferBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
