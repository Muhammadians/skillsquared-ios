//
//  SendServicePopup.swift
//  SkillSquared
//
//  Created by Awais Aslam on 26/12/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import SwiftyJSON
import SVProgressHUD

class SendServicePopup: UIViewController {
    
    @IBOutlet weak var serviceDescription: UITextView!
    @IBOutlet weak var totalAmount: UITextField!
    @IBOutlet weak var deliveryTime: DropDown!
    
    
    var navigation : UINavigationController!
    var dismissController : postrequest?
    var buyer_id_request = ""
    var ser_id = ""
    var deliverDays = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(buyer_id_request)
        print(ser_id)
        deliveryDropDown()
    }
    
    func deliveryDropDown()
    {
        deliveryTime.optionArray = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"]
        deliveryTime.didSelect{(selectedText , index ,id) in
            
            self.deliverDays = selectedText
            print("\(self.deliverDays)")
            
        }
    }
    
    
    @IBAction func dismissPopUp(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendOffer(_ sender: Any) {
        
        SellerSendOffer()
    }
}

extension SendServicePopup {
    
    func SellerSendOffer(){
        
        
        if serviceDescription.text.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter description")
        }
        else if totalAmount.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter amount")
        }
        else if deliveryTime.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter delivery time")
        }
            
            
        else {
            
            SVProgressHUD.show()
            
            let service_id = ser_id
            let buyer_request_id = buyer_id_request
            let description = serviceDescription.text
            let price = totalAmount.text
            let duration = deliverDays
            
            print(("\(service_id )\(buyer_request_id )\(description ?? "")\(price ?? "")\(duration)"))
            
            let headers: HTTPHeaders = [
                "accesstoken": Constants.accessToken
            ]
            
            let params = [
                "service_id": service_id,
                "buyer_request_id": buyer_request_id,
                "description": description!,
                "price": price!,
                "duration": ("\(duration)\(" days")")
                ] as [String : Any]
            
            
            let url = "sendOffer"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON {response in
                
                var err:Error?
                
                switch response.result {
                    
                case .success(let json):
                    print(json)
                    SVProgressHUD.dismiss()
                    
                    let jsonData = JSON(response.result.value!)
                    print(jsonData)
                    
                    
                    if let dictObject = jsonData.dictionaryObject {
                        print(dictObject)
                        
                        let message = dictObject["message"] as! String
                        let status = dictObject["status"] as! Int
                        
                        if status == 200 {
                            let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                                self.dismiss(animated: true) {
                                    
                                    self.dismissController?.didJobPosted()
                                }
                            }
                            
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                            
                        else{
                            let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                            alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                    
                case .failure(let error):
                    err = error
                    print(err ?? "")
                    SVProgressHUD.dismiss()
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
                }
            }
        }
    }
}

