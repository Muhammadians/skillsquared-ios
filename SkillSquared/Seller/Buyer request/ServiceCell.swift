//
//  ServiceCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 26/12/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class ServiceCell: UITableViewCell {
    
   
    @IBOutlet weak var serImage: UIImageView!
    @IBOutlet weak var txt: UILabel!
    
    @IBOutlet weak var CellTapped: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
