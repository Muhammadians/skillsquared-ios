//
//  selectServices.swift
//  SkillSquared
//
//  Created by Awais Aslam on 26/12/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit


class selectServices: UIViewController, postrequest {
    
    @IBOutlet weak var serTableView: UITableView!
    
    var sellerServiceArray: Array<Sellerservices>?
    var buyer_id = ""
    
    override func viewDidLoad() {
        print(buyer_id)
        super.viewDidLoad()
        
        serTableView.delegate = self
        serTableView.dataSource = self
    }
    
}

extension selectServices: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var value: Int = 0
        
        if sellerServiceArray?.count ?? 0 > 0{
            self.serTableView.restore()
            value = sellerServiceArray?.count ?? 0
        }
        else{
            self.serTableView.setEmptyMessage("No Data Found.")
            return 0
        }
        return value
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell", for: indexPath) as! ServiceCell
        
        if let details = sellerServiceArray{
            cell.serImage.kf.setImage(with: URL(string: details[indexPath.item].service_image!), placeholder: UIImage(named: "Placeholder"))
            cell.txt.text = details[indexPath.item].title ?? ""
            
            cell.CellTapped.addTarget(self, action: #selector(buttonPressed(_ :)), for: .touchUpInside)
            cell.CellTapped.tag = indexPath.row
            
        }
        return cell
    }
    
    
    @objc func buttonPressed(_ sender: UIButton){
        
        let service_Id = sellerServiceArray?[sender.tag].id ?? ""
        let PopupController = self.storyboard?.instantiateViewController(withIdentifier: "SendServicePopupVC") as! SendServicePopup
        
        PopupController.navigation = self.navigationController
        PopupController.providesPresentationContextTransitionStyle = true
        PopupController.definesPresentationContext = true
        PopupController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        PopupController.ser_id = service_Id
        PopupController.buyer_id_request = buyer_id
        self.navigationController?.present(PopupController, animated: true, completion: nil)
        PopupController.dismissController = self
        
    }
    
    func didJobPosted() {
        self.navigationController!.popViewController(animated: true)
    }
}


