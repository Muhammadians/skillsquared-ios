//
//  BuyerRequest.swift
//  SkillSquared
//
//  Created by Awais Aslam on 30/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import ObjectMapper

class BuyerRequest: UIViewController {
    
    @IBOutlet weak var buyerCollectionView: UICollectionView!
    @IBOutlet weak var totalOffers: UILabel!
    
    var buyerRequestArray: Array<Buyerequest>?
    var servicesArray: Array<Sellerservices>?
    var buyerRequestFlowLayout: UICollectionViewFlowLayout!
    var buyerCellIdentifier =  "BuyerCell"
    
    var buyer_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        buyerRequestArray = []
        setUpCollectionView()
        getBuyerServices()
    }
    
    private func setUpCollectionView(){
        buyerCollectionView.delegate = self
        buyerCollectionView.dataSource = self
        let buyernib = UINib(nibName: "BuyerRequestCell", bundle: nil)
        buyerCollectionView.register(buyernib, forCellWithReuseIdentifier: buyerCellIdentifier)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        buyerCollectionViewSize()
    }
    
    private func buyerCollectionViewSize() {
        
        if buyerRequestFlowLayout == nil {
            
            let lineSpacing: CGFloat = 5
            let interItemSpacing: CGFloat = 0
            
            let width = UIScreen.main.bounds.width
            let height = buyerCollectionView.frame.height
            
            buyerRequestFlowLayout = UICollectionViewFlowLayout()
            
            buyerRequestFlowLayout.itemSize = CGSize(width: width - 5, height: height - 20)
            buyerRequestFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            buyerRequestFlowLayout.scrollDirection = .horizontal
            buyerRequestFlowLayout.minimumLineSpacing = lineSpacing
            buyerRequestFlowLayout.minimumInteritemSpacing = interItemSpacing
            
            buyerCollectionView.setCollectionViewLayout(buyerRequestFlowLayout, animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pagenumber = round(scrollView.contentOffset.x / buyerCollectionView.frame.width)
        let val = Int(pagenumber)
        self.totalOffers.text = "\(val + 1)/\(buyerRequestArray!.count)"
    }
}

extension BuyerRequest: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var value: Int = 0
        
        if buyerRequestArray?.count ?? 0 > 0{
            self.buyerCollectionView.restore()
            value = buyerRequestArray?.count ?? 0
        }
        else{
            self.buyerCollectionView.setEmptyMessage("No Data Found.")
            return 0
        }
         return value
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: buyerCellIdentifier, for: indexPath) as! BuyerRequestCell
        
        if let details = buyerRequestArray{
            
            cell.buyerRequestName.text = details[indexPath.item].user_name ?? ""
            cell.buyerDescription.text = details[indexPath.item].description ?? ""
            cell.buyerRequestDate.text = details[indexPath.item].created_date ?? ""
            cell.buyerRequestDuration.text = details[indexPath.item].delievry ?? ""
            cell.buyerRequestBudget.text = details[indexPath.item].budget ?? ""
            
            
            let cat = details[indexPath.item].category ?? ""
            let offerSent = details[indexPath.item].offer_sent ?? ""
            cell.buyerRequestOffer.text   = cat + " / " + offerSent
            
            let status = details[indexPath.item].totaloffer
            let val = (status! as NSString).integerValue
            
            if  val >= 1{
                cell.SendOfferBtn.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                cell.SendOfferBtn.isUserInteractionEnabled = false
            }
                
            else if val == 0{
                cell.SendOfferBtn.backgroundColor = #colorLiteral(red: 0, green: 0.6153565645, blue: 0.06842776388, alpha: 1)
                cell.SendOfferBtn.isUserInteractionEnabled = true
            }
            
            cell.SendOfferBtn.addTarget(self, action: #selector(offerTapped(_ :)), for: .touchUpInside)
            cell.SendOfferBtn.tag = indexPath.row
            
        }
        return cell
    }
    
    @objc func offerTapped(_ sender: UIButton){
        self.buyer_id = buyerRequestArray?[sender.tag].id ?? ""
        performSegue(withIdentifier: "btnTapped", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "btnTapped" {
            let vc = segue.destination as! selectServices
            vc.buyer_id = buyer_id
            vc.sellerServiceArray = self.servicesArray
        }
    }
}

extension BuyerRequest{
    
    func getBuyerServices() {
        
        SVProgressHUD.show()
        
        let url = "getBuyerRequests"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .get, headers: headers).responseJSON { (response) in
            
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<BuyerObject>().map(JSONString: jsonStr!)
                print(responseModel as Any)
                
                self.buyerRequestArray = responseModel?.buyer_requests?.buyerequest
                self.servicesArray = responseModel?.sellerservices
                
                self.buyerCollectionView.reloadData()
                
                if self.buyerRequestArray?.count ?? 0 > 0{
                self.totalOffers.text = "\(1)/\(self.buyerRequestArray?.count ?? 0)"
                }
                else{
                    self.totalOffers.text = "\(0)/\(0)"
                }
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}
