//
//  SellerProfileCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 23/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class SellerProfileCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var review: UILabel!
    @IBOutlet weak var rating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
