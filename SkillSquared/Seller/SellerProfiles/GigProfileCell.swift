//
//  GigProfileCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 31/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class GigProfileCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
