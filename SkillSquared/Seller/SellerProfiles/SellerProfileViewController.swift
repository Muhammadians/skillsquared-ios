//
//  SellerProfileViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 31/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import WMSegmentControl
import Alamofire
import SVProgressHUD
import SwiftyJSON
import Kingfisher
import ExpandableLabel
import ObjectMapper


class SellerProfileViewController: UIViewController {
    
    @IBOutlet weak var segment: WMSegment!
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var gigsView: UIView!
    @IBOutlet weak var reviewView: UIView!
    @IBOutlet weak var gigsCollectionView:
    UICollectionView!
    
    @IBOutlet weak var skillsCollectionView: UICollectionView!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var memberShipSince: UILabel!
    @IBOutlet weak var recentDelivery: UILabel!
    @IBOutlet weak var lastActive: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var SellerDescription: ExpandableLabel!
    
    
    @IBOutlet weak var reviewTableView: UITableView!
    @IBOutlet weak var editProfileButton: UIBarButtonItem!
    @IBOutlet weak var overAllRating: UILabel!
    
    var userDetails: SellerProfileinfo?
    var reviewsList: Array<SellerReviews>?
    var allRating: GetSellerObject?
    var gigArray : Array<Active_gigs_services>?
    var skillsArray: [String]? = []
    
    
    var checkValue:String!
    var userID:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segment.selectorType = .bottomBar
        gigsView.isHidden = true
        reviewView.isHidden = true
        reviewTableView.delegate = self
        reviewTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reviewsList = []
        gigArray = []
        SetUpCollectionView()
        
        if checkValue == "Gigs" {
            getSellerProfileData()
            self.navigationItem.rightBarButtonItem = nil
        }
        else {
            getSellerDetails()
        }
    }
    
    
    func fillUserDetail()  {
        
        if let details = userDetails{
            
            self.navigationItem.title = details.username
            self.userImage.kf.setImage(with: URL(string: details.freelancer_image!), placeholder: UIImage(named: "Placeholder"))
            self.userName.text = details.username ?? ""
            self.location.text = details.location ?? ""
            self.language.text = details.language ?? ""
            self.memberShipSince.text = details.memberSince ?? ""
            self.recentDelivery.text = details.recent_delivery ?? ""
            self.SellerDescription.text = details.description?.html2String ?? ""
        }
    }
    
    func getAllRating()  {
        if let rating = allRating{
            let totalRating: Int =  rating.avgRating ?? 0
            self.overAllRating.text = String(totalRating)
        }
    }
    
    @IBAction func viewChange(_ sender: WMSegment) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            print("first item")
            view.addSubview(aboutView)
            gigsView.isHidden = true
            reviewView.isHidden = true
            aboutView.isHidden = false
            
        case 1:
            print("second item")
            view.addSubview(gigsView)
            
            if gigArray == nil{
                
                Alert.showAlert(on: self, title: "Alert", message: "No Data Found")
            }
            aboutView.isHidden = true
            reviewView.isHidden = true
            gigsView.isHidden = false
            
        case 2:
            print("Third item")
            view.addSubview(reviewView)
            if reviewsList?.count == nil{
                Alert.showAlert(on: self, title: "Alert", message: "No Data Found")
            }
            aboutView.isHidden = true
            gigsView.isHidden = true
            reviewView.isHidden = false
            
        default:
            break
        }
    }
    
    @IBAction func editProfileTapped(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Edit profile", message: "Please visit Skillquared.com to edit profile.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            //          UIApplication.shared.open(URL(string: "www.google.com")! as URL, options: [:], completionHandler: nil)
            let url = "https://www.skillsquared.com"
            let nsurl = NSURL(string: url)
            UIApplication.shared.canOpenURL(nsurl! as URL)
            UIApplication.shared.open(nsurl! as URL)
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    var gigFlowLayout: UICollectionViewFlowLayout!
    var skillFlowLayout: UICollectionViewFlowLayout!
    let gigCellIdentifier = "profileCell"
    
    private func SetUpCollectionView(){
        gigsCollectionView.delegate = self
        gigsCollectionView.dataSource = self
        let gigNib = UINib(nibName: "GigProfileCell", bundle: nil)
        gigsCollectionView.register(gigNib, forCellWithReuseIdentifier: gigCellIdentifier)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        GigCollectionViewSize()
        SkillCollectionViewSize()
    }
    
    private func GigCollectionViewSize() {
        
        if gigFlowLayout == nil {
            
            let lineSpacing: CGFloat = 5
            let interItemSpacing: CGFloat = 10
            
            let width = gigsCollectionView.frame.width
            let height = gigsCollectionView.frame.height / 4
            
            gigFlowLayout = UICollectionViewFlowLayout()
            gigFlowLayout.itemSize = CGSize(width: width - 15, height: height)
            gigFlowLayout.sectionInset = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
            gigFlowLayout.scrollDirection = .vertical
            gigFlowLayout.minimumLineSpacing = lineSpacing
            gigFlowLayout.minimumInteritemSpacing = interItemSpacing
            gigsCollectionView.setCollectionViewLayout(gigFlowLayout, animated: true)
        }
    }
    
    private func SkillCollectionViewSize() {
        
        skillsCollectionView.delegate = self
        skillsCollectionView.dataSource = self
        
        if let layout = skillsCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            
            layout.scrollDirection = .horizontal
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 15
            layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
            
            let size = CGSize(width: 100, height: 30)
            layout.itemSize = size
        }
        
    }
}

extension SellerProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == gigsCollectionView{
            return gigArray?.count ?? 0
        }
        else {
            return skillsArray?.count ?? 0
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == gigsCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: gigCellIdentifier, for: indexPath) as! GigProfileCell
            
            if let gigDetais = gigArray{
                
                cell.textLabel.text = gigDetais[indexPath.item].description?.html2String ?? ""
                cell.image.kf.setImage(with: URL(string: gigDetais[indexPath.item].service_image!), placeholder: UIImage(named: "Placeholder"))
                cell.rating.text = gigDetais[indexPath.item].rating ?? ""
                let price = gigDetais[indexPath.item].price
                let str = "Price: "
                cell.price.text  =  str + (price ?? "")
            }
            
            return cell
        }
            
        else {
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "skillsCellIdentifier", for: indexPath) as! skillsCell
            
            cellB.skillTxt.text = skillsArray?[indexPath.item]
            
            return cellB
        }
    }
}


extension SellerProfileViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reviewsList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = reviewTableView.dequeueReusableCell(withIdentifier: "SellerProfileIdentifier", for: indexPath) as! SellerProfileCell
        
        if let reviewDetails = reviewsList {
            
            cell.userName.text = reviewDetails[indexPath.item].buyerName
            cell.userImage.kf.setImage(with: URL(string: reviewDetails[indexPath.item].buyer_image!), placeholder: UIImage(named: "User"))
            cell.review.text = reviewDetails[indexPath.item].review
            cell.rating.text = reviewDetails[indexPath.item].rating
        }
        
        return cell
    }
}


extension SellerProfileViewController{
    
    func getSellerDetails(){
        
        SVProgressHUD.show()
        
        let url = "getSellerProfile"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<GetSellerObject>().map(JSONString: jsonStr!)
                print(responseModel as Any)
                
                self.allRating = responseModel
                self.userDetails = responseModel?.profileinfo?.sellerProfileinfo
                self.reviewsList = responseModel?.sellerReviews
                self.gigArray = responseModel?.profileinfo?.gigs?.active_gigs_services
                
                
                var skillList = [String]()
                
                let data = responseModel?.skills ?? []
                for i in 0..<data.count{
                    skillList.append(data[i])
                }
                
                self.skillsArray = skillList
                print(self.skillsArray!)
                
                
                self.reviewTableView.reloadData()
                self.gigsCollectionView.reloadData()
                self.skillsCollectionView.reloadData()
                
                self.fillUserDetail()
                self.getAllRating()
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}


extension SellerProfileViewController{
    
    func getSellerProfileData(){
        
        SVProgressHUD.show()
        
        let url = "getSellerProfileById"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let params = ["freelancer_user_id": userID ?? 0] as [String : Any]
        print("Params is \(params)")
        
        Alamofire.request(baseUrl, method: .post, parameters: params).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<GetSellerObject>().map(JSONString: jsonStr!)
                print(responseModel as Any)
                
                self.allRating = responseModel
                self.userDetails = responseModel?.profileinfo?.sellerProfileinfo
                self.reviewsList = responseModel?.sellerReviews
                self.gigArray = responseModel?.profileinfo?.gigs?.active_gigs_services
                
                
                var skillList = [String]()
                
                let data = responseModel?.skills ?? []
                for i in 0..<data.count{
                    skillList.append(data[i])
                }
                
                self.skillsArray = skillList
                print(self.skillsArray!)
                
                
                self.reviewTableView.reloadData()
                self.gigsCollectionView.reloadData()
                self.skillsCollectionView.reloadData()
                
                self.fillUserDetail()
                self.getAllRating()
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}

