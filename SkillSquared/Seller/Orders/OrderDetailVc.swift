//
//  OrderDetailVc.swift
//  SkillSquared
//
//  Created by Awais Aslam on 14/02/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON
import SVProgressHUD

class OrderDetailVc: UIViewController, postrequest {
    
    @IBOutlet weak var order: UILabel!
    @IBOutlet weak var orderDescription: UILabel!
    @IBOutlet weak var buyer: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var workDuration: UILabel!
    @IBOutlet weak var deliverWork: UILabel!
    @IBOutlet weak var cancelOrder: UILabel!
    @IBOutlet weak var disputeBtn: UILabel!
    @IBOutlet weak var resubmitOrderBtn: UILabel!
    
    var activeOrderArr: Array<Aactive>?
    var revisionArr: Array<Arevsion>?
    var orderDetailArr: Array<Sellerorderdetail>?
    var selectedIndexValue: Int = 0
    var order_id:String!
    var value:String!
    var type:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("order id is : \(order_id!)")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if value == "Active"{
            
            deliverWork.visibility = .visible
            cancelOrder.visibility = .visible
            
            disputeBtn.visibility = .invisible
            disputeBtn.visibility = .gone
            
            resubmitOrderBtn.visibility = .invisible
            resubmitOrderBtn.visibility = .gone
            
        }
            
        else if value == "Completed" {
            
            deliverWork.visibility = .invisible
            deliverWork.visibility = .gone
            
            cancelOrder.visibility = .invisible
            cancelOrder.visibility = .gone
            
            disputeBtn.visibility = .invisible
            disputeBtn.visibility = .gone
            
            resubmitOrderBtn.visibility = .invisible
            resubmitOrderBtn.visibility = .gone
            
        }
            
        else if value == "Revision" {
            
            deliverWork.visibility = .invisible
            deliverWork.visibility = .gone
            
            cancelOrder.visibility = .invisible
            cancelOrder.visibility = .gone
            
            disputeBtn.visibility = .invisible
            disputeBtn.visibility = .gone
            
        }
            
        else if value == "Disputed" {
            
            deliverWork.visibility = .invisible
            deliverWork.visibility = .gone
            
            cancelOrder.visibility = .invisible
            cancelOrder.visibility = .gone
            
            resubmitOrderBtn.visibility = .invisible
            resubmitOrderBtn.visibility = .gone
        }
        
        getSellerOrderDetail()
        self.setUpDisputeBtn()
        self.setUpDeliveredWork()
        self.setupWebDisputeBtn()
        self.setUpResubmitOrder()
    }
    
    
    func fillData(){
        
        if let data = orderDetailArr{
            
            self.order.text = data.first?.orderNo
            self.orderDescription.text = data.first?.offer_description ?? ""
            self.buyer.text = data.first?.payer_name
            self.price.text = (data.first?.price ?? "") + ("$")
            self.workDuration.text = (data.first?.duration ?? "") + (" Days")
        }
    }
    
    func setUpDeliveredWork(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.deliveredPressed(_ :)))
        self.deliverWork.isUserInteractionEnabled = true
        self.deliverWork.addGestureRecognizer(labelTap)
    }
    
    @objc func deliveredPressed(_ sender: UITapGestureRecognizer) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentDeliverOrder") as! DeliveredWorkViewController
        vc.navigation = self.navigationController
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.orderId = self.activeOrderArr?[selectedIndexValue].id
        self.navigationController?.present(vc, animated: true, completion: nil)
        vc.dismissController = self
    }
    
    
    func setUpResubmitOrder(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.resubmitPressed(_ :)))
        self.resubmitOrderBtn.isUserInteractionEnabled = true
        self.resubmitOrderBtn.addGestureRecognizer(labelTap)
    }
    
    @objc func resubmitPressed(_ sender: UITapGestureRecognizer) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentDeliverOrder") as! DeliveredWorkViewController
        vc.navigation = self.navigationController
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.orderId = self.revisionArr?[selectedIndexValue].id
        self.navigationController?.present(vc, animated: true, completion: nil)
        vc.dismissController = self
    }
    
    func setUpDisputeBtn(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.disputePressed(_ :)))
        self.cancelOrder.isUserInteractionEnabled = true
        self.cancelOrder.addGestureRecognizer(labelTap)
    }
    
    @objc func disputePressed(_ sender: UITapGestureRecognizer) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PresentDisputeOrder") as! DisputeOrderVC
        vc.navigation = self.navigationController
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.orderId = self.activeOrderArr?[selectedIndexValue].id
        vc.orderType = self.type
        self.navigationController?.present(vc, animated: true, completion: nil)
        vc.dismissController = self
    }
    
    func setupWebDisputeBtn(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.disputeWebPressed(_ :)))
        self.disputeBtn.isUserInteractionEnabled = true
        self.disputeBtn.addGestureRecognizer(labelTap)
    }
    
    
    @objc func disputeWebPressed(_ sender: UITapGestureRecognizer) {
        
        let alertController = UIAlertController(title: "Skillsquared", message: "Please visit Skillquared.com to proceed dispute.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            let url = "https://www.skillsquared.com"
            let nsurl = NSURL(string: url)
            UIApplication.shared.canOpenURL(nsurl! as URL)
            UIApplication.shared.open(nsurl! as URL)
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func didJobPosted() {
        self.navigationController!.popViewController(animated: true)
    }
    
}


extension OrderDetailVc{
    
    func getSellerOrderDetail(){
        
        SVProgressHUD.show()
        
        let url = "orderDetail"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let id =  order_id
        
        let params = ["order_id":id,
                      "type":type
            
            ] as [String : AnyObject]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<BuyerOrderDetailObj>().map(JSONString: jsonStr!)
                print(responseModel as Any)
                
                self.orderDetailArr = responseModel?.sellerorderdetail
                self.fillData()
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}
