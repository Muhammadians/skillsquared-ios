//
//  SalesCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 01/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {
    
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var saleUsername: UILabel!
    @IBOutlet weak var saleDescription: UILabel!
    @IBOutlet weak var salePrice: UILabel!
    @IBOutlet weak var saleDate: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var statusText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
}
