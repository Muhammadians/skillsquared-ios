//
//  DeliveredViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 17/02/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper
import SVProgressHUD

class DeliveredViewController: UIViewController {
    
    @IBOutlet weak var downloadSource: UILabel!
    @IBOutlet weak var movedOrderInDeliverState: UILabel!
    @IBOutlet weak var aboutOrder: UILabel!
    @IBOutlet weak var orderDescription: UILabel!
    @IBOutlet weak var buyerName: UILabel!
    @IBOutlet weak var orderPrice: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var workDays: UILabel!
    
    
    var orderId:String!
    var selectedIndex:Int!
    var type:String!
    var downloadPhoto:UIImage?
    
    var dataArray:Array<Sellerorderdetail>?
    var imageUrl:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(orderId ?? "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getOrderData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DeliverWork()
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
        }.resume()
    }
    
    
    func DeliverWork(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.deliverWorkTapped(_ :)))
        self.downloadSource.isUserInteractionEnabled = true
        self.downloadSource.addGestureRecognizer(labelTap)
    }
    
    @objc func deliverWorkTapped(_ sender: UITapGestureRecognizer) {
        imageActionSheet()
    }
    
    func imageActionSheet() {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let pickFromCamera = UIAlertAction(title: "Save image to photos", style: .default)
        { _ in
            
            SVProgressHUD.show(withStatus: "Downloading...")
            
            let ImageURLString = self.imageUrl
            print(ImageURLString)
            guard let yourImageURL = URL(string: ImageURLString!) else { return }
            
            self.getDataFromUrl(url: yourImageURL) { (data, response, error) in
                
                guard let data = data, let imageFromData = UIImage(data: data) else { return }
                
               
                    UIImageWriteToSavedPhotosAlbum(imageFromData, nil, nil, nil)
                    SVProgressHUD.showSuccess(withStatus: "Completed")
//                    self.imageView.image = imageFromData
            }
        }
        
        actionSheetController.addAction(pickFromCamera)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func fillData() {
        
        if let data = dataArray{
            self.orderPrice.text = data.first?.price
            self.aboutOrder.text = data.first?.orderNo
            self.buyerName.text = data.first?.payer_name
            self.orderDescription.text = data.first?.offer_description
            
            let days = " Days"
            self.workDays.text = (data.first?.duration ?? "") + days
            
        }
    }
}

extension DeliveredViewController{
    
    func getOrderData(){
        
        SVProgressHUD.show()
        
        let url = "orderDetail"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let id =  orderId
        
        let params = ["order_id":id!,
            "type":type] as [String : AnyObject]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<OrderDetailObj>().map(JSONString: jsonStr!)
                print(responseModel as Any)
                
                self.dataArray = responseModel?.sellerorderdetail
                self.imageUrl = responseModel?.deliverNote?.file
                self.movedOrderInDeliverState.text = responseModel?.orderStatus ?? ""
                self.fillData()
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}
