//
//  DeliveredWorkViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 21/02/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import Alamofire
import SwiftyJSON
import SVProgressHUD

class DeliveredWorkViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var workDescription: UITextView!
    @IBOutlet weak var fileName: UIButton!
    @IBOutlet weak var fileImage: UIImageView!
    
    
    var navigation : UINavigationController!
    var dismissController : postrequest?
    var imagePicker = UIImagePickerController()
    var selectedPhoto:UIImage?
    var pickedImageName:String!
    var videoURL : URL!
    var orderId:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("order id is \(orderId)")
        
    }
    
    @IBAction func chooseFileTapped(_ sender: Any) {
        imageActionSheet()
    }
    
    @IBAction func closeBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitBtnTapped(_ sender: Any) {
        submitOrder()
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? NSURL
        self.videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
        
        print(videoURL)
        
        if imageURL != nil {
            self.pickedImageName = (imageURL?.lastPathComponent)
        }
        else {
            self.pickedImageName = (videoURL?.lastPathComponent)
        }
        
        fileName.setTitle(pickedImageName, for: .normal)
        
        
        var selectImage : UIImage!
        if let img = info[.editedImage] as? UIImage
        {
            selectImage = img
        }
        else if let img = info[.originalImage] as? UIImage
        {
            selectImage = img
        }
        
        self.selectedPhoto = selectImage
        self.fileImage.image = selectImage
        dismiss(animated: true, completion: nil)
    }
    
    func imageActionSheet() {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let pickFromCamera = UIAlertAction(title: "Take From Camera", style: .default)
        { _ in
            
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera;
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetController.addAction(pickFromCamera)
        
        let pickFromGallery = UIAlertAction(title: "Select image & video", style: .default)
        { _ in
            
            self.imagePicker.mediaTypes = ["public.image", "public.movie"]
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        actionSheetController.addAction(pickFromGallery)
        self.present(actionSheetController, animated: true, completion: nil)
    }
}

extension DeliveredWorkViewController{
    
    func submitOrder(){
        
        if workDescription.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter description")
        }
        else if selectedPhoto == nil && self.pickedImageName.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please choose file")
        }
        else{
            
            SVProgressHUD.show()
            
            let des = workDescription.text
            let id = orderId
            
            let params = [ "order_id": id!,
                           "description": des!,
                ] as [String : Any]
            
            
            let url = "deliverWork"
            let baseUrl = "\(K_BaseUrl)\(url)"
            let imgData = selectedPhoto?.jpegData(compressionQuality: 1)!
            
            let headers: HTTPHeaders = [
                "Content-type": "multipart/form-data",
                "accesstoken": Constants.accessToken
            ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                if let data = imgData{
                    multipartFormData.append(data, withName: "file", fileName: "\(Date().timeIntervalSinceNow)image.jpg", mimeType: "image/jpg")
                }
                
                if self.videoURL != nil{
                    if let url = self.videoURL {
                        do {
                            let videoData = try Data(contentsOf: url)
                            print(videoData)
                            multipartFormData.append(videoData, withName: "file", fileName: "\(Date().timeIntervalSinceNow)video.mp4" , mimeType: "video/mp4")
                        } catch let error {
                            print(error)
                        }
                    }
                }
                
                for (key, value) in params {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
            },  to: baseUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        print(response.result.isSuccess)
                        
                        SVProgressHUD.dismiss()
                        
                        let jsonData = JSON(response.result.value!)
                        print(jsonData)
                        
                        if let dictObject = jsonData.dictionaryObject {
                            print(dictObject)
                            
                            let message = dictObject["message"] as! String
                            let status = dictObject["status"] as! Int
                            
                            if status == 200 {
                                let alertController = UIAlertController(title: "Title", message: (String(describing: message)), preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                    self.dismiss(animated: true) {
                                        
                                        self.dismissController?.didJobPosted()
                                    }
                                }
                                
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                                
                            else{
                                let alertController = UIAlertController(title: "Alert", message: "\(String(describing: message))", preferredStyle: .alert);
                                alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                    
                case .failure(let error):
                    SVProgressHUD.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                    print(error)
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
                }
            }
        }
    }
}
