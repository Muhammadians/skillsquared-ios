//
//  AnalyticsVC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 30/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import ObjectMapper
import Charts

class AnalyticsVC: UIViewController {
    
    @IBOutlet weak var totalEarnings: UILabel!
    @IBOutlet weak var completedOrders: UILabel!
    @IBOutlet weak var avgSellingPrice: UILabel!
    @IBOutlet weak var earningThisMonth: UILabel!
    
    @IBOutlet weak var chartView: LineChartView!
    
    var analyticsData: AnalyticsObject?
    var dataEntries: [ChartDataEntry] = []
    var months: [String]!
    weak var axisFormatDelegate: IAxisValueFormatter?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        getAnalyticsData()
    }
    
    
    func setChart(values: [Double], dataPoints:[String]) {
        chartView.noDataText = "No data available!"
        
        //        for i in 0..<values.count {
        //            print("chart point : \(values[i])")
        //            let dataEntry = ChartDataEntry(x: Double(i), y: values[i])
        //
        //            dataEntries.append(dataEntry)
        //        }
        
        for i in 0..<dataPoints.count {
            print("chart point : \(dataPoints[i])")
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
        }
        
        let chartData = LineChartDataSet(entries: dataEntries, label: "Units Consumed")
        
        chartData.colors = [NSUIColor.blue]
        chartData.mode = .cubicBezier
        chartData.cubicIntensity = 0.2
        
        let gradient = getGradientFilling()
        chartData.fill = Fill.fillWithLinearGradient(gradient, angle: 90.0)
        chartData.drawFilledEnabled = true
        
        let data = LineChartData()
        data.addDataSet(chartData)
        chartView.data = data
        chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: months)
        chartView.xAxis.setLabelCount(months.count, force: true)
        
        chartView.xAxis.labelPosition = .bottom
        chartView.setScaleEnabled(false)
        chartView.animate(xAxisDuration: 1.5)
        chartView.drawGridBackgroundEnabled = false
        chartView.xAxis.drawAxisLineEnabled = false
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.leftAxis.drawAxisLineEnabled = false
        chartView.leftAxis.drawGridLinesEnabled = false
        chartView.rightAxis.drawAxisLineEnabled = false
        chartView.rightAxis.drawGridLinesEnabled = false
        chartView.legend.enabled = false
        chartView.xAxis.enabled = true
        chartView.leftAxis.enabled = true
        chartView.rightAxis.enabled = false
        chartView.xAxis.drawLabelsEnabled = true
        
    }
    
    
    private func getGradientFilling() -> CGGradient {
        // Setting fill gradient color
        let coloTop = UIColor(red: 0/255, green: 193/255, blue: 59/255, alpha: 1).cgColor
        let colorBottom = UIColor(red: 0/255, green: 210/255, blue: 49/255, alpha: 1).cgColor
        // Colors of the gradient
        let gradientColors = [coloTop, colorBottom] as CFArray
        // Positioning of the gradient
        let colorLocations: [CGFloat] = [0.7, 0.0]
        // Gradient Object
        return CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)!
    }
    
    func fillAnalyticsDetail() {
        
        if let details = analyticsData{
            
            
            let totalearningMonth: Int = details.seller_earnings_current_month ?? 0
            self.earningThisMonth.text = ("\(String(totalearningMonth))\("$")")
            
            let comp_orders: Int = details.seller_complted_orders_count ?? 0
            self.completedOrders.text = String(comp_orders)
            
            let totalearning: Int = details.seller_earnings ?? 0
            self.totalEarnings.text = ("\(String(totalearning))\("$")")
            
            let avgPrice: Int = details.avgSellingPrice ?? 0
            self.avgSellingPrice.text = ("\(String(avgPrice))\("$")")
            
            
            let str = details.seller_analytics
            let data = str?.components(separatedBy: ",")
            guard let val_1 = data?[0].toDouble() else { return }
            guard let val_2 = data?[1].toDouble() else { return }
            guard let val_3 = data?[2].toDouble() else { return }
            guard let val_4 = data?[3].toDouble() else { return }
            guard let val_5 = data?[4].toDouble() else { return }
            guard let val_6 = data?[5].toDouble() else { return }
            guard let val_7 = data?[6].toDouble() else { return }
            guard let val_8 = data?[7].toDouble() else { return }
            guard let val_9 = data?[8].toDouble() else { return }
            guard let val_10 = data?[9].toDouble() else { return }
            guard let val_11 = data?[10].toDouble() else { return }
            guard let val_12 = data?[11].toDouble() else { return }
            
            setChart(values: [val_1,val_2,val_3,val_4,val_5,val_6,val_7,val_8,val_9,val_10,val_11,val_12], dataPoints: months)
        }
    }
}

extension AnalyticsVC {
    
    func getAnalyticsData()  {
        
        SVProgressHUD.show()
        
        let url = "analytics"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<AnalyticsObject>().map(JSONString: jsonStr!)
                
                let data = responseModel
                self.analyticsData = data
                self.fillAnalyticsDetail()
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}
