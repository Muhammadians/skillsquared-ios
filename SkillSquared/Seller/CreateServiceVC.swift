//
//  CreateServiceVC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 31/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import iOSDropDown
import ObjectMapper

class CreateServiceVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var serviceTitle: UITextField!
    @IBOutlet weak var descriptionCount: UILabel!
    @IBOutlet weak var mainCat: DropDown!
    @IBOutlet weak var subCat: DropDown!
    @IBOutlet weak var subCatChild: DropDown!
    @IBOutlet weak var textDescription: UITextView!
    @IBOutlet weak var subCatView: UIView!
    @IBOutlet weak var subCatChildView: UIView!
    
    var categoriesArray: Array<Catoptions>?
    var subCatArray : Array<Subcat>?
    var subCatChildArray : Array<Subcat_child>?
    var catId = ""
    var selectMainCat = ""
    var selectSubCat = ""
    var selectChildCat = ""
    var cat_level = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        subCatView.visibility = .invisible
        subCatView.visibility = .gone
        
        subCatChildView.visibility = .invisible
        subCatChildView.visibility = .gone
        
        serviceTitle.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCategories()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        descriptionCount.text = ("\(count)\(" /")\(" 40")")
        return count < 40
    }
    
    
    func dropDownCat(mainlist:[String])
    {
        mainCat.optionArray = mainlist
        mainCat.didSelect{(selectedText , index ,id) in
            
            self.selectMainCat = selectedText
            print("\(self.selectMainCat)")
            let checkVal2 = selectedText
            
            if let mainArray = self.categoriesArray{
                for i in 0..<mainArray.count{
                    if (mainArray[i].title == checkVal2){
                        if (mainArray[i].subcat != nil) {
                            self.subCatArray = mainArray[i].subcat
                            self.subCatView.visibility = .visible
                        } else {
                            self.catId = mainArray[i].cat_id ?? ""
                            self.cat_level = "1"
                            print(self.catId)
                        }
                    }
                }
            }
            var subCategory = [String]()
            if let subCategoryArray = self.subCatArray{
                for i in 0..<subCategoryArray.count{
                    subCategory.append(subCategoryArray[i].title ?? "")
                    self.catId = subCategoryArray[i].id ?? ""
                    //                    print("\(self.catId)")
                }
                self.subCatDropDown(subList: subCategory)
            }
        }
    }
    
    func subCatDropDown(subList:[String])
    {
        subCat.optionArray = subList
        subCat.didSelect{(selectedText , index ,id) in
            
            self.selectSubCat = selectedText
            print("\(self.selectSubCat)")
            let checkVal2 = selectedText
            
            if let subCategoryArray = self.subCatArray{
                for i in 0..<subCategoryArray.count{
                    if (subCategoryArray[i].title == checkVal2){
                        if (subCategoryArray[i].subcat_child != nil) {
                            self.subCatChildArray = subCategoryArray[i].subcat_child
                            self.subCatChildView.visibility = .visible
                        } else {
                            self.catId = subCategoryArray[i].id ?? ""
                            self.cat_level = "2"
                            print(self.catId)
                        }
                    }
                }
            }
            var subCategoryChild = [String]()
            if let subChildArray = self.subCatChildArray{
                for i in 0..<subChildArray.count{
                    subCategoryChild.append(subChildArray[i].title ?? "")
                    self.catId = subChildArray[i].id ?? ""
                    //                    print("\(self.catId)")
                }
                self.subCatChildDropDown(subChildList: subCategoryChild)
            }
        }
    }
    
    func subCatChildDropDown(subChildList:[String])
    {
        subCatChild.optionArray = subChildList
        subCatChild.didSelect{(selectedText , index ,id) in
            print(self.selectSubCat)
            //
            let checkVal2 = selectedText
            
            for i in 0..<self.subCatChildArray!.count{
                if (self.subCatChildArray![i].title == checkVal2){
                    self.catId = self.subCatChildArray![i].id ?? ""
                    self.cat_level = "3"
                    print(self.catId)
                }
            }
            
            print("\(self.selectChildCat)")
        }
    }
    
    @IBAction func onNextTap(_ sender: Any) {
        
        if serviceTitle.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter title")
        }
        else if catId.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please select category")
        }
        else if textDescription.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter description")
        }
        else{
            self.performSegue(withIdentifier: "createServicesPublishSegue", sender: (Any).self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createServicesPublishSegue" {
            let vc = segue.destination as! CreateServicePublish
            vc.serviceTitle = self.serviceTitle.text!
            vc.catId = self.catId
            vc.serviceDescription = self.textDescription.text
            vc.cat_level = self.cat_level
        }
    }
}

extension CreateServiceVC {
    
    func getCategories(){
        
        SVProgressHUD.show()
        
        let url = "becomeafreelancer"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        Alamofire.request(baseUrl, method: .get).responseJSON { (response) in
            print(response) // http url reponse
            if response.result.isSuccess {
                
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<MainObject>().map(JSONString: jsonStr!)
                
                self.categoriesArray = responseModel?.catoptions
                
                var arrayList = [String]()
                
                let data = responseModel?.catoptions
                for i in 0..<data!.count{
                    arrayList.append(data?[i].title ?? "")
                    //                        self.catId = responseObject[i].cat_id ?? ""
                    //                        print("\(self.catId)")
                    
                }
                self.dropDownCat(mainlist: arrayList)
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}
