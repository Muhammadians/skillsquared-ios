//
//  CreateServicePublish.swift
//  SkillSquared
//
//  Created by Awais Aslam on 31/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Photos
import MobileCoreServices
import iOSDropDown
import SVProgressHUD

class CreateServicePublish: UIViewController {
    
    @IBOutlet weak var enterPrice: UITextField!
    @IBOutlet weak var chooseFile: UIButton!
    @IBOutlet weak var additionalDescription: UITextView!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var deliveryTime: UITextField!
    
    var navigation : UINavigationController!
    
    var deliveryText = ""
    var imagePicker = UIImagePickerController()
    var selectedPhoto:UIImage?
    var pickedImageName:String!
    var price = ""
    var serviceTitle = ""
    var catId = ""
    var serviceDescription = ""
    var cat_level = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        print(serviceTitle)
        print(catId)
        print(serviceDescription)
        print(cat_level)
    }
    
    @IBAction func chooseFileTapped(_ sender: Any) {
        imageActionSheet()
    }
    
    
    @IBAction func publishService(_ sender: Any) {
        postRequest()
    }
}


extension CreateServicePublish: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? NSURL
        
        self.pickedImageName = (imageURL?.lastPathComponent)
        chooseFile.setTitle(pickedImageName, for: .normal)
        
        
        var selectImage : UIImage!
        if let img = info[.editedImage] as? UIImage
        {
            selectImage = img
        }
        else if let img = info[.originalImage] as? UIImage
        {
            selectImage = img
        }
        
        self.selectedPhoto = selectImage
        self.selectedImage.image = selectImage
        dismiss(animated: true, completion: nil)
        
    }
    
    func imageActionSheet() {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let pickFromCamera = UIAlertAction(title: "Take From Camera", style: .default)
        { _ in
            
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera;
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetController.addAction(pickFromCamera)
        
        let pickFromGallery = UIAlertAction(title: "Pick From Gallery", style: .default)
        { _ in
            
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        
        actionSheetController.addAction(pickFromGallery)
        self.present(actionSheetController, animated: true, completion: nil)
    }
}


extension CreateServicePublish {
    
    func postRequest(){
        
        if enterPrice.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter price")
        }
        else if deliveryTime.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter delivery time")
        }
        else if selectedImage == nil{
            Alert.showAlert(on: self, title: "Alert", message: "Please select image")
        }
        else if additionalDescription.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter description")
        }
        else{
            
            SVProgressHUD.show()
            
            let price = enterPrice.text
            let time = deliveryTime.text
            let des = serviceDescription
            let title = serviceTitle
            let id = catId
            let ser_des = additionalDescription.text
            let category_level = cat_level
            
            let params = [ "service_title": title,
                           "category_id": id,
                           "description": des,
                           "price": price!,
                           "delivery_time": time!,
                           "additional_info": ser_des!,
                           "cat_level": category_level] as [String : Any]
            
            
            print(params)
            
            let url = "createService"
            let baseUrl = "\(K_BaseUrl)\(url)"
            let imgData = selectedPhoto?.jpegData(compressionQuality: 1)!
            
            let headers: HTTPHeaders = [
                "Content-type": "multipart/form-data",
                "accesstoken": Constants.accessToken
            ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                if let data = imgData{
                    multipartFormData.append(data, withName: "service_banner", fileName: "\(Date().timeIntervalSinceNow)image.jpg", mimeType: "image/jpg")
                }
                
                for (key, value) in params {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
            },  to: baseUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        print(response.result.isSuccess)
                        
                        SVProgressHUD.dismiss()
                        
                        let jsonData = JSON(response.result.value!)
                        print(jsonData)
                        
                        if let dictObject = jsonData.dictionaryObject {
                            print(dictObject)
                            
                            let message = dictObject["message"] as! String
                            let status = dictObject["status"] as! Int
                            
                            if status == 200 {
                                let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                    
                                    self.dismiss(animated: true) {
                                        
                                        for controller in self.navigationController!.viewControllers as Array {
                                            if controller.isKind(of: AllSallerScreensViewController.self) {
                                                _ =  self.navigationController!.popToViewController(controller, animated: true)
                                                break
                                            }
                                        }
                                    }
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                                
                            else{
                                let alertController = UIAlertController(title: "Alert", message: "\(String(describing: message))", preferredStyle: .alert);
                                alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                    
                case .failure(let error):
                    SVProgressHUD.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                    print(error)
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
                }
            }
        }
    }
}


