//
//  WithdrawWithPaystackVC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 08/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import SVProgressHUD
import SwiftyJSON
import ObjectMapper

class WithdrawWithPaystackVC: UIViewController {
    
    @IBOutlet weak var bankListDropdown: DropDown!
    @IBOutlet weak var bankCode: UITextField!
    @IBOutlet weak var accountName: UITextField!
    @IBOutlet weak var bankAccountNo: UITextField!
    
    var bankName = ""
    var arrayData = [BankDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        getBankList()
    }
    
    func Banklist(list:[String])
    {
        bankListDropdown.optionArray = list
        bankListDropdown.didSelect{(selectedText , index ,id) in
            self.bankName = selectedText
        }
    }
    
    @IBAction func submitBankInformation(_ sender: Any) {
        submitInformation()
    }
}

extension WithdrawWithPaystackVC{
    
    func getBankList(){
        
        let url = "psBankList"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
               
                let bank_list = json["paystackbanklistings"]
                let bankNames = bank_list["data"]
                for countryNames in bankNames.arrayValue{
                    self.arrayData.append(BankDataModel(json: countryNames))
                }
                
                var arrayList = [String]()
                
                for i in 0..<self.arrayData.count{
                    arrayList.append(self.arrayData[i].name ?? "")
                }
                self.Banklist(list: arrayList)
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension WithdrawWithPaystackVC{
    
    func submitInformation(){
        
        if bankName.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please select bank name")
        }
        else if bankCode.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter bank code")
        }
        else if accountName.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter account name")
        }
        else if bankAccountNo.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter bank account no")
        }
            
        else {
            
            SVProgressHUD.show()
            
            let code =  bankCode.text
            let acountName = accountName.text
            let acc_no = bankAccountNo.text
            let bank = bankName
            
            let headers: HTTPHeaders = [
                "accesstoken": Constants.accessToken
            ]
            
            let params = ["account_name":acountName!,"bank_name":bank,"bank_code":code!,"bank_account_no":acc_no!] as [String : AnyObject]
            
            let url = "psbanksetting"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers
            ).responseJSON {response in
                
                var err:Error?
                
                switch response.result {
                    
                case .success(let json):
                    print(json)
                    SVProgressHUD.dismiss()
                    
                    let jsonData = JSON(response.result.value!)
                    print(jsonData)
                    
                    
                    if let dictObject = jsonData.dictionaryObject {
                        print(dictObject)
                        
                        let message = dictObject["message"] as! String
                        let status = dictObject["status"] as! Int
                        
                        if status == 200 {
                            Alert.showAlert(on: self, title: "Alert", message: (String(describing: message)))
                            
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: EarningsVC.self) {
                                    _ =  self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                        }
                            
                        else{
                            Alert.showAlert(on: self, title: "Alert", message: (String(describing: message)))
                        }
                    }
                    
                case .failure(let error):
                    err = error
                    print(err!)
                    SVProgressHUD.dismiss()
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
                }
            }
        }
    }
}
