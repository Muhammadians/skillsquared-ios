//
//  WithdrawWithPaypalVC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 08/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class WithdrawWithPaypalVC: UIViewController {
    
    
    @IBOutlet weak var email: SATextField!
    @IBOutlet weak var withDrawAmount: SATextField!
    
    var withdrawValue = ""
    var navigation : UINavigationController!
    var dismissController : postrequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func dismissPopUp(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func requestWithdraw(_ sender: Any) {
        
        if withdrawValue == "payPal"{
            withDrawUsingPaypal()
        }
        else{
            withDrawUsingPaypal()
        }
        
    }
}

extension WithdrawWithPaypalVC{
    
    func withDrawUsingPaypal(){
        
        if email.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter email id")
        }
        else if withDrawAmount.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter ammount")
        }
        else {
            
            SVProgressHUD.show()
            
            let emailId =  email.text
            let ammount = withDrawAmount.text
            
            let headers: HTTPHeaders = [
                "accesstoken": Constants.accessToken
            ]
            
            let params = ["email":emailId!,"amount":ammount!] as [String : AnyObject]
            
            let url = "paystackWithdrawRequest"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON {response in
                
                var err:Error?
                
                switch response.result {
                    
                case .success(let json):
                    print(json)
                    SVProgressHUD.dismiss()
   
                    
                    let jsonData = JSON(response.result.value!)
                    print(jsonData)
                    
                    
                    if let dictObject = jsonData.dictionaryObject {
                        print(dictObject)
                        
                        let message = dictObject["message"] as! String
                        let status = dictObject["status"] as! Int
                        
                        if status == 200 {
                           
                            let alertController = UIAlertController(title: "Success", message: (String(describing: message)), preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                            
                                self.dismiss(animated: true, completion: {
                                    
                                        self.dismissController?.didJobPosted()
                                
                                    })
                                
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                            
                        else{
                            
                            let alertController = UIAlertController(title: "Error", message: (String(describing: message)), preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                            
                                self.dismiss(animated: true, completion: {
                                    
                                        self.dismissController?.didJobPosted()
                                
                                    })
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                    
                case .failure(let error):
                    err = error
                    print(err!)
                    SVProgressHUD.dismiss()
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
                }
            }
        }
    }
}
