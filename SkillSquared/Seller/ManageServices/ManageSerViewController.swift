//
//  ManageSerViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 01/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import Kingfisher
import ObjectMapper

class ManageSerViewController: UIViewController {
    
    
    @IBOutlet weak var servicesTableView: UITableView!
    
    var activeGigsArray: Array<Active_gigs_services>?
    var selectedIndex: Int = 0
    //    var deleteindex: Int = 0
    var serviceId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.servicesTableView.delegate = self
        self.servicesTableView.dataSource = self
        getManageServices()
        activeGigsArray = []
    }
    
}

//extension ManageSerViewController: DeleteManageService{
//
//    func deleteData(indx: Int) {
//        //        self.deleteindex = indx
//        print("indx is : \(indx)")
//        print("one")
//
//        activeGigsArray?.remove(at: indx)
//
//    }
//}

extension ManageSerViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var value: Int = 0
        
        if activeGigsArray?.count ?? 0 > 0{
            self.servicesTableView.restore()
            value = activeGigsArray?.count ?? 0
        }
        else{
            self.servicesTableView.setEmptyMessage("No Data Found.")
            return 0
        }
        return value
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ManageSerCell", for: indexPath) as! ManageServiceCell
        
        if let details = activeGigsArray{
            
            cell.serviceImage.kf.setImage(with: URL(string: details[indexPath.item].service_image!), placeholder: UIImage(named: "Placeholder"))
            cell.servicesLabel.text = details[indexPath.item].title ?? ""
            cell.totalOrders.text = details[indexPath.item].total_orders ?? ""
            
            
            let totalClicks: Int = details[indexPath.item].total_clicks ?? 0
            cell.totalClicks.text = String(totalClicks)
            
            cell.cancelBtn.addTarget(self, action: #selector(cancelBtnTapped(_ :)), for: .touchUpInside)
            cell.cancelBtn.tag = indexPath.row
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "GoToDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToDetails" {
            let vc = segue.destination as! ManageServiceDetailVC
            vc.selectedIndex = self.selectedIndex
            vc.servicesArray = self.activeGigsArray
        }
    }
    
    
    @objc func cancelBtnTapped(_ sender: UIButton){
        
        let alert = UIAlertController(title: NSLocalizedString("Report", comment: ""), message: NSLocalizedString("Are you sure you want to delete this offer?", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
            switch action.style{
            case .default:
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    
                    let id = self.activeGigsArray?[sender.tag].id ?? ""
                    self.deleteRow(index: sender.tag)
                    self.deleteItem(ser_id: id)
                }
                
                
            case .cancel: break
                
            case .destructive: break
            @unknown default:
                fatalError()
            }}))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel",comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func deleteRow(index: Int){
        
        print("indx is : \(index)")
        print("one")
        
        activeGigsArray?.remove(at: index)
        
        
    }
}

extension ManageSerViewController{
    
    func getManageServices() {
        
        SVProgressHUD.show()
        
        let url = "manageServices"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                
                let responseModel = Mapper<ManageServicesObject>().map(JSONString: jsonStr!)
                print(responseModel as Any)
                
                self.activeGigsArray = responseModel?.active_gigs_services
                self.servicesTableView.reloadData()
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}

extension ManageSerViewController{
    
    func deleteItem(ser_id: String) {
        
        //        SVProgressHUD.show()
        
        let url = "removeSellerService"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let params = [
            "service_id": ser_id
            ] as [String : Any]
        
        print(params)
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            var err:Error?
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            switch response.result {
                
            case .success(let json):
                print(json)
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let message = dictObject["message"] as! String
                    let status = dictObject["status"] as! Int
                    
                    if status == 200 {
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)) , preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                        
                    else{
                        let alertController = UIAlertController(title: "Alert", message: "\(String(describing: message))", preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                    self.servicesTableView.reloadData()
                }
                
            case .failure(let error):
                err = error.localizedDescription as? Error
                print(err!)
                SVProgressHUD.dismiss()
                self.servicesTableView.reloadData()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
                
            }
        }
    }
}
