//
//  ManageServiceCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 01/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

protocol DeleteManageService {
    func deleteData(indx: Int)
}

class ManageServiceCell: UITableViewCell {
    
    @IBOutlet var servicesLabel: UILabel!
    @IBOutlet weak var totalClicks: UILabel!
    @IBOutlet weak var totalOrders: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var delegate: DeleteManageService?
    var index: IndexPath?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func DeleteItemButton(_ sender: Any) {
        delegate?.deleteData(indx: (index?.row)!)
    }

}
