//
//  EditServiceSecondVc.swift
//  SkillSquared
//
//  Created by Awais Aslam on 29/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices
import Alamofire
import SVProgressHUD
import SwiftyJSON

class EditServiceSecondVc: UIViewController {
    
    @IBOutlet weak var deliveryTime: UITextField!
    @IBOutlet weak var servicePrice: UITextField!
    @IBOutlet weak var pickedImageView: UIImageView!
    @IBOutlet weak var imageName: UIButton!
    @IBOutlet weak var AditionalDescription: UITextView!
    
    var editServiceObject: Service_detail?
    
    var serviceid:String!
    var serviceTitle:String!
    var catId:String!
    var serviceDes:String!
    var cat_level:String!
    var pickedImageName:String!
    var imagePicker = UIImagePickerController()
    var selectedPhoto:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setDetail()
    }
    
    func setDetail() {
        if let data = editServiceObject{
            self.servicePrice.text = data.price
            self.deliveryTime.text = data.delivery_time
            self.AditionalDescription.text = data.additional_info
        }
    }
    
    @IBAction func chooseFileTapped(_ sender: Any) {
        imageActionSheet()
    }
    
    @IBAction func editServiceTapped(_ sender: Any) {
        editServiceRequest()
    }
}

extension EditServiceSecondVc: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? NSURL
        
        self.pickedImageName = (imageURL?.lastPathComponent)
        imageName.setTitle(pickedImageName, for: .normal)
        
        var selectImage : UIImage!
        if let img = info[.editedImage] as? UIImage
        {
            selectImage = img
        }
        else if let img = info[.originalImage] as? UIImage
        {
            selectImage = img
        }
        
        self.selectedPhoto = selectImage
        self.pickedImageView.image = selectImage
        dismiss(animated: true, completion: nil)
        
    }
    
    
    func imageActionSheet() {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let pickFromCamera = UIAlertAction(title: "Take From Camera", style: .default)
        { _ in
            
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera;
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetController.addAction(pickFromCamera)
        
        let pickFromGallery = UIAlertAction(title: "Pick From Gallery", style: .default)
        { _ in
            
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        actionSheetController.addAction(pickFromGallery)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
}

extension EditServiceSecondVc {
    
    func editServiceRequest(){
        
        if servicePrice.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter price")
        }
        else if deliveryTime.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter delivery time")
        }
        else if imageName == nil{
            Alert.showAlert(on: self, title: "Alert", message: "Please select image")
        }
        else if AditionalDescription.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter description")
        }
            
            
        else{
            
            SVProgressHUD.show()
            
            let price = servicePrice.text
            let time = deliveryTime.text
            let des = serviceDes
            let title = serviceTitle
            let id = catId
            let ser_des = AditionalDescription.text
            let category_level = cat_level
            
            let params = [ "service_id": serviceid!,
                           "service_title": title!,
                           "category_id": id!,
                           "description": des!,
                           "price": price!,
                           "delivery_time": time!,
                           "additional_info": ser_des!,
                           "cat_level": category_level!] as [String : Any]
            
            
            let url = "createService"
            let baseUrl = "\(K_BaseUrl)\(url)"
            let imgData = selectedPhoto?.jpegData(compressionQuality: 0.5)
            
            let headers: HTTPHeaders = [
                "Content-type": "multipart/form-data",
                "accesstoken": Constants.accessToken
            ]
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                if let data = imgData{
                    multipartFormData.append(data, withName: "service_banner", fileName: "\(Date().timeIntervalSinceNow)image.jpg", mimeType: "image/jpg")
                }
                
                for (key, value) in params {
                     multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
                print(params)
                
            },  to: baseUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        print(response.result.isSuccess)
                        
                        SVProgressHUD.dismiss()
                        
                        let jsonData = JSON(response.result.value!)
                        print(jsonData)
                        
                        if let dictObject = jsonData.dictionaryObject {
                            print(dictObject)
                            
                            let message = dictObject["message"] as! String
                            let status = dictObject["status"] as! Int
                            
                            if status == 200 {
                                let alertController = UIAlertController(title: "Title", message: (String(describing: message)), preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                
                                    self.dismiss(animated: true) {
                                        
                                        for controller in self.navigationController!.viewControllers as Array {
                                            if controller.isKind(of: AllSallerScreensViewController.self) {
                                                _ =  self.navigationController!.popToViewController(controller, animated: true)
                                                break
                                            }
                                        }
                                    }
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                                
                            else{
                                let alertController = UIAlertController(title: "Alert", message: "\(String(describing: message))", preferredStyle: .alert);
                                alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                    
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    print(error)
                }
            }
        }
    }
}


