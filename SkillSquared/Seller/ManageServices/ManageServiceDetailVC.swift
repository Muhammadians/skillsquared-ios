//
//  ManageServiceDetailVC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 28/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import FloatRatingView
import Kingfisher

class ManageServiceDetailVC: UIViewController {

    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var serviceDescription: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    @IBOutlet weak var deliverTime: UILabel!
    @IBOutlet weak var ServiceRating: FloatRatingView!
    
    
    var servicesArray: Array<Active_gigs_services>?
    var selectedIndex: Int = 0
    
    var service_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(servicesArray!)
        print("Selected Index is : \(selectedIndex)")
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
         setData()
    }
    
    func setData(){
     
        if let data = servicesArray?[selectedIndex]{
            
            self.headerImage.kf.setImage(with: URL(string: (data.service_image)!), placeholder: UIImage(named: "Placeholder"))
            self.serviceTitle.text = data.title
            self.serviceDescription.text = data.description?.html2String
            self.servicePrice.text = data.price
            self.deliverTime.text = data.delivery_time
            let rating = data.rating?.toDouble()
            self.ServiceRating.rating = rating ?? 0
            self.service_id = data.id!
        }
    }
    
    @IBAction func editServiceTapped(_ sender: Any) {
        performSegue(withIdentifier: "GoToEditService", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if segue.identifier == "GoToEditService" {
               let vc = segue.destination as! EditServiceViewController
               vc.ser_id = service_id
           }
       }
    
}
