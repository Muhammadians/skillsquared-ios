//
//  EditServiceViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 28/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import iOSDropDown
import SVProgressHUD
import ObjectMapper

class EditServiceViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var serviceTitle: UITextField!
    @IBOutlet weak var mainCatDropDown: DropDown!
    @IBOutlet weak var subCatDropDown: DropDown!
    @IBOutlet weak var subCatChildDropDown: DropDown!
    @IBOutlet weak var serviceDescription: UITextView!
    @IBOutlet weak var descriptionCount: UILabel!
    
    @IBOutlet weak var mainCatView: UIView!
    @IBOutlet weak var subCatView: UIView!
    @IBOutlet weak var subCatChildView: UIView!
    
    
    var categoryArray: Array<Catoptions>?
    var subCatArray : Array<Subcat>?
    var subCatChildArray : Array<Subcat_child>?
    var detailArray : Service_detail?
    
    var subCategory = [String]()
    var subCategoryChild = [String]()
    
    var sertitle:String!
    var serDescription:String!
    var categoryId:String!
    var subcat_id:String!
    var subcatChild_id:String!
    var selectMainCat = ""
    var selectSubCat = ""
    var selectChildCat = ""
    var catId = ""
    var cat_level = ""
    var ser_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(ser_id)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GetServiceData()
        
        subCatView.visibility = .invisible
        subCatView.visibility = .gone
        
        subCatChildView.visibility = .invisible
        subCatChildView.visibility = .gone
        
        serviceTitle.delegate = self
    }
    
    
    func fillData(){
        
        self.serviceTitle.text = sertitle
        self.serviceDescription.text = serDescription
        let count = sertitle.count
        self.descriptionCount.text = ("\(count)\(" /")\(" 40")")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        descriptionCount.text = ("\(count)\(" /")\(" 40")")
        return count < 40
    }
    
    func dropDownCat(mainlist:[String])
    {
        mainCatDropDown.optionArray = mainlist
        mainCatDropDown.didSelect{(selectedText , index ,id) in
            
            self.selectMainCat = selectedText
            print("\(self.selectMainCat)")
            let checkVal2 = selectedText
            
            if let mainArray = self.categoryArray{
                for i in 0..<mainArray.count{
                    if (mainArray[i].title == checkVal2){
                        if (mainArray[i].subcat != nil) {
                            self.subCatArray = mainArray[i].subcat
                            self.subCatView.visibility = .visible
                        } else {
                            self.catId = mainArray[i].cat_id ?? ""
                            self.cat_level = "1"
                            print(self.catId)
                        }
                    }
                }
            }
            
            var subCategory = [String]()
            if let subCategoryArray = self.subCatArray{
                for i in 0..<subCategoryArray.count{
                    subCategory.append(subCategoryArray[i].title ?? "")
                    self.catId = subCategoryArray[i].id ?? ""
                    //                    print("\(self.catId)")
                }
                self.subCatDropDown(subList: subCategory)
            }
        }
    }
    
    func subCatDropDown(subList:[String])
    {
        subCatDropDown.optionArray = subList
        subCatDropDown.didSelect{(selectedText , index ,id) in
            
            self.selectSubCat = selectedText
            print("\(self.selectSubCat)")
            let checkVal2 = selectedText
            
            if let subCategoryArray = self.subCatArray{
                for i in 0..<subCategoryArray.count{
                    if (subCategoryArray[i].title == checkVal2){
                        if (subCategoryArray[i].subcat_child != nil) {
                            self.subCatChildArray = subCategoryArray[i].subcat_child
                            self.subCatChildView.visibility = .visible
                        } else {
                            self.catId = subCategoryArray[i].id ?? ""
                            self.cat_level = "2"
                            print(self.catId)
                        }
                    }
                }
            }
            var subCategoryChild = [String]()
            if let subChildArray = self.subCatChildArray{
                for i in 0..<subChildArray.count{
                    subCategoryChild.append(subChildArray[i].title ?? "")
                    self.catId = subChildArray[i].id ?? ""
                    //print("\(self.catId)")
                }
                self.subCatChildDropDown(subChildList: subCategoryChild)
            }
        }
    }
    
    func subCatChildDropDown(subChildList:[String])
    {
        subCatChildDropDown.optionArray = subChildList
        subCatChildDropDown.didSelect{(selectedText , index ,id) in
            print(self.selectSubCat)
            //
            let checkVal2 = selectedText
            
            for i in 0..<self.subCatChildArray!.count{
                if (self.subCatChildArray![i].title == checkVal2){
                    self.catId = self.subCatChildArray![i].id ?? ""
                    self.cat_level = "3"
                    print(self.catId)
                }
            }
            
            print("\(self.selectChildCat)")
        }
    }
    
    @IBAction func nextBtn(_ sender: Any) {
        
        if serviceTitle.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter title")
        }
        else if catId.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please select category")
        }
        else if serviceDescription.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter description")
        }
        else{
            self.performSegue(withIdentifier: "NavigateToEditService", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NavigateToEditService" {
            let vc = segue.destination as! EditServiceSecondVc
            vc.serviceid = ser_id
            vc.editServiceObject = detailArray
            vc.serviceTitle = self.serviceTitle.text!
            vc.catId = self.catId
            vc.serviceDes = self.serviceDescription.text
            vc.cat_level = self.cat_level
        }
    }
    
}

extension EditServiceViewController{
    func GetServiceData(){
        
        SVProgressHUD.show()
        
        let url = "editservice"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let params = [
            
            "service_id": ser_id
            ] as [String : Any]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            print(response) // http url reponse
            if response.result.isSuccess {
                
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<EditServiceObj>().map(JSONString: jsonStr!)
                
                DispatchQueue.main.async {
                    
                    self.detailArray = responseModel?.service_detail
                    self.categoryId = responseModel?.category_id
                    self.subcat_id = responseModel?.subcategory_id
                    self.subcatChild_id = responseModel?.subcategorychild_id
                    self.categoryArray = responseModel?.catoptions
                    self.sertitle = (responseModel?.service_detail?.title)!
                    self.serDescription = responseModel?.service_detail?.description?.html2String
                    
                    
                    self.fillData()
                    
                    var arrayList = [String]()
                    let data = responseModel?.catoptions
                    for i in 0..<data!.count{
                        arrayList.append(data?[i].title ?? "")
                    }
                    self.dropDownCat(mainlist: arrayList)
                    
                    
                    if let mainArr = self.categoryArray{
                        for i in 0..<mainArr.count{
                            if mainArr[i].subcat != nil {
                                if(self.categoryId == self.categoryArray![i].cat_id){
                                    self.subCatArray = mainArr[i].subcat
                                    self.mainCatDropDown.text = self.categoryArray![i].title
                                }
                            }else{
                                self.catId = self.categoryId
                                self.cat_level = "1"
                            }
                        }
                    }
                    
                    print(self.subCatArray!)
                    
                    var SubCatList = [String]()
                    if let subCatArr = self.subCatArray{
                        for i in 0..<subCatArr.count{
                            if self.subCatArray != nil{
                                self.subCatView.visibility = .visible
                                SubCatList.append(subCatArr[i].title!)
                                if(self.subcat_id == subCatArr[i].id){
                                    self.subCatChildArray = subCatArr[i].subcat_child
                                    self.subCatDropDown.text = subCatArr[i].title
                                }else{
                                    self.catId = self.subcat_id
                                    self.cat_level = "2"
                                }
                            }
                        }
                    }
                    self.subCatDropDown(subList: SubCatList)
                    
                    
                    var subCatChildList = [String]()
                    if self.subCatChildArray != nil {
                        self.subCatChildView.visibility = .visible
                        if let subCatChildArr = self.subCatChildArray{
                            for i in 0..<subCatChildArr.count{
                                subCatChildList.append(subCatChildArr[i].title!)
                                if self.subcatChild_id == subCatChildArr[i].id{
                                    self.subCatChildDropDown.text = subCatChildArr[i].title
                                    self.catId = self.subcatChild_id
                                    self.cat_level = "3"
                                }
                            }
                        }
                    }
                    self.subCatChildDropDown(subChildList: subCatChildList)
                }
                
            }
            else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
            
        }
    }
}
