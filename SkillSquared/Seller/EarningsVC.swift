//
//  EarningsVC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 31/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import Braintree
import Paystack
import ObjectMapper

class EarningsVC: UIViewController, postrequest {
    
    @IBOutlet weak var totalEarnings: UILabel!
    @IBOutlet weak var totalCompletedOrders: UILabel!
    @IBOutlet weak var avgSellingPrice: UILabel!
    @IBOutlet weak var earnThisMonth: UILabel!
    
    var earningsData: EarningsObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getEarningsData()
    }
    
    func fillEearningsDetail() {
        
        if let details = earningsData{
            
            let sellerEarnings: Int = details.seller_earnings ?? 0
            self.totalEarnings.text = ("\(String(sellerEarnings))\("$")")
            
            let curruntMonthEarning: Int = details.seller_earnings_current_month ?? 0
            self.earnThisMonth.text = ("\(String(curruntMonthEarning))\("$")")
            
            let totalOrders: Int = details.total_completed_orders ?? 0
            self.totalCompletedOrders.text = String(totalOrders)
            
            
            let avgPrice: Int = details.avgSellingPrice ?? 0
            self.avgSellingPrice.text = ("\(String(avgPrice))\("$")")
        }
    }
    
    @IBAction func PayUsingPaypal(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "paypalPopUp") as! WithdrawWithPaypalVC
        vc.navigation = self.navigationController
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.withdrawValue = "payPal"
        self.navigationController?.present(vc, animated: true, completion: nil)
        vc.dismissController = self
        
    }
    
    @IBAction func withDrawWithPaystack(_ sender: Any){
        
        if earningsData?.paystack == nil{
            
            performSegue(withIdentifier: "withdrawsegue", sender: self)
            
        }
        else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "paypalPopUp") as! WithdrawWithPaypalVC
            vc.navigation = self.navigationController
            vc.providesPresentationContextTransitionStyle = true
            vc.definesPresentationContext = true
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.navigationController?.present(vc, animated: true, completion: nil)
            vc.dismissController = self
            
        }
    }
    
    func didJobPosted() {
        self.navigationController!.popViewController(animated: true)
    }
    
}


extension EarningsVC {
    
    func getEarningsData()  {
        
        SVProgressHUD.show()
        
        let url = "earnings"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<EarningsObject>().map(JSONString: jsonStr!)
                let data = responseModel
                self.earningsData = data
                self.fillEearningsDetail()
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}
