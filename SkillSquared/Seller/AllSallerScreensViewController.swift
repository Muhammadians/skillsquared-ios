//
//  AllSallerScreensViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 21/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class AllSallerScreensViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func createServicesTap(_ sender: Any) {
        
        performSegue(withIdentifier: "CreateServicesSegue", sender: self)
    }
    
    @IBAction func manageServicesTap(_ sender: Any) {
        
        performSegue(withIdentifier: "ManageServicesSegue", sender: self)
    }
    
    @IBAction func buyerRequest(_ sender: Any) {
        
        performSegue(withIdentifier: "buyerRequestSegue", sender: self)
    }
    
    @IBAction func orderTap(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let normalOrderController = storyboard.instantiateViewController(withIdentifier: "OrdersViewController") as! OrdersViewController
        self.navigationController?.pushViewController(normalOrderController, animated: true)
    }
    
    @IBAction func analyticsTap(_ sender: Any) {
        
        performSegue(withIdentifier: "AnalyticsController", sender: self)
    }
    
    @IBAction func earningTap(_ sender: Any) {
        
        performSegue(withIdentifier: "EarningSegue", sender: self)
    }
    
}

//extension AllSallerScreensViewController {
//
//
//    func alertPopupss(title : String)
//    {
//        let alerts = UIAlertController.init(title: title , message: "", preferredStyle: .alert)
//
//        alerts.addAction(UIAlertAction(title: "Normal Orders", style: .default, handler: { (uiaction:UIAlertAction) in
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let normalOrderController = storyboard.instantiateViewController(withIdentifier: "orderVc") as! OrdersViewController
//            self.navigationController?.pushViewController(normalOrderController, animated: true)
//
//        }))
//
//        alerts.addAction(UIAlertAction(title: "Custom Orders", style: .default, handler: { (uiaction:UIAlertAction) in
//
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let customOrderController = storyboard.instantiateViewController(withIdentifier: "CustomOrderVc") as! SellerCustomOrderController
//            self.navigationController?.pushViewController(customOrderController, animated: true)
//
//
//        }))
//
//
//        alerts.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (uiaction:UIAlertAction) in
//
//            print("alert actions active")
//
//        }))
//
//        self.present(alerts, animated: true, completion: nil)
//    }
//
//}
