//
//  Constants.swift
//  SkillSquared
//
//  Created by Awais Aslam on 04/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import UIKit

let K_BaseUrl = "https://www.skillsquared.com/mobileservices/"
let appName = "SkillSquared"
let uuid = UUID().uuidString
let app = UIApplication.shared.delegate as! AppDelegate
var deviceTokenGet = "121221dfdcdf244dfg454"
var quickBloxUnReadCount : Int = 0

struct Constants {
    
    
    static var QB_USERS_ENVIROMENT: String {
            
    #if DEBUG
            return "dev"
    #elseif QA
            return "qbqa"
    #else
        assert(false, "Not supported build configuration")
        return ""
    #endif
            
        }

    
    static var freeLancer: Bool{
        
        get {
            return UserDefaults.standard.bool(forKey: "freeLancer")
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "freeLancer")
        }
        
    }
    
    static var userEmail: String {
        get {
            return UserDefaults.standard.string(forKey: "userEmail") ?? ""
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "userEmail")
        }
    }
    
    static var userName: String {
        get {
            return UserDefaults.standard.string(forKey: "userName") ?? ""
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "userName")
        }
    }
    
    static var phoneNumber: String {
        get {
            return UserDefaults.standard.string(forKey: "phoneNumber") ?? ""
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "phoneNumber")
        }
    }
    
    static var address: String {
        get {
            return UserDefaults.standard.string(forKey: "address") ?? ""
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "address")
        }
    }
    
    static var userID: String {
        get {
            return UserDefaults.standard.string(forKey: "userID") ?? ""
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "userID")
        }
    }
    
    static var accessToken: String {
        get {
            return UserDefaults.standard.string(forKey: "AccessToken") ?? ""
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "AccessToken")
        }
    }
    
    static var DeviceToken: String {
        get {
            return UserDefaults.standard.string(forKey: "DeviceToken") ?? ""
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "DeviceToken")
        }
    }
    
    static var userImage: String {
        get {
            return UserDefaults.standard.string(forKey: "userImage") ?? ""
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "userImage")
        }
    }
    
    static var notificationConstant: String {
        get {
            return UserDefaults.standard.string(forKey: "notificationConstant") ?? ""
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "notificationConstant")
        }
    }
    
    static var badgeValue: Int{
        
        get {
            return UserDefaults.standard.integer(forKey: "badgeValue")
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "badgeValue")
        }
    }
}

