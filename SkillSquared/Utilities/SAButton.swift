//
//  SAButton.swift
//  SkillSquared
//
//  Created by Awais Aslam on 23/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import UIKit

class SAButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    
    private func setupButton() {
        backgroundColor = #colorLiteral(red: 0, green: 0.7127318382, blue: 0.3490835428, alpha: 1)
        layer.cornerRadius  = frame.size.height/5
        setTitleColor(.white, for: .normal)
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: 0, height: 3)
        layer.shadowColor = #colorLiteral(red: 0.7254417539, green: 0.721131146, blue: 0.7287563682, alpha: 0.8012895976)
        layer.shadowRadius = 10
        layer.shadowOpacity = 1
    }
}
