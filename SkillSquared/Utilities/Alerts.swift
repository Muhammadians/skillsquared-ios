//
//  Alerts.swift
//  SkillSquared
//
//  Created by Awais Aslam on 04/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import UIKit

struct Alert {


static func showAlert(on vc:UIViewController, title:String, message:  String){
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
    vc.present(alert, animated: true)
    }
}
