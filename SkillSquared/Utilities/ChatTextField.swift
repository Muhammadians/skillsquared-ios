//
//  ChatTextField.swift
//  SkillSquared
//
//  Created by Awais Aslam on 25/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import UIKit

class ChatTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpField()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init( coder: aDecoder )
        setUpField()
    }
    
    
    private func setUpField() {
        tintColor             = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        textColor             = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        backgroundColor       = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        autocorrectionType    = .no
        layer.cornerRadius  = frame.size.height/5
        clipsToBounds         = true
        
        
        
        
        let indentView        = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        leftView              = indentView
        leftViewMode          = .always
    }
}


