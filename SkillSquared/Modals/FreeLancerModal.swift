//
//  FreeLancerModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 21/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation

struct FreeLancerObject : Codable {
    let freelancer_info : Freelancer_info?

    enum CodingKeys: String, CodingKey {

        case freelancer_info = "freelancer_info"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        freelancer_info = try values.decodeIfPresent(Freelancer_info.self, forKey: .freelancer_info)
    }

}

struct Freelancer_info : Codable {
    let username : String?
    let freelancer_title : String?
    let description : String?
    let language : String?
    let avgRating : Int?
    let location : String?
    let userImage : String?

    enum CodingKeys: String, CodingKey {

        case username = "username"
        case freelancer_title = "freelancer_title"
        case description = "description"
        case language = "language"
        case avgRating = "avgRating"
        case location = "location"
        case userImage = "userImage"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        freelancer_title = try values.decodeIfPresent(String.self, forKey: .freelancer_title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        language = try values.decodeIfPresent(String.self, forKey: .language)
        avgRating = try values.decodeIfPresent(Int.self, forKey: .avgRating)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        userImage = try values.decodeIfPresent(String.self, forKey: .userImage)
    }

}
