//
//  StripeModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 27/03/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct StripeModal : Mappable {
    var id : String?
    var object : String?
    var card : Card?
    var client_ip : String?
    var created : Int?
    var livemode : Bool?
    var type : String?
    var used : Bool?
    var status : Int?
    var message: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        object <- map["object"]
        card <- map["card"]
        client_ip <- map["client_ip"]
        created <- map["created"]
        livemode <- map["livemode"]
        type <- map["type"]
        used <- map["used"]
        status <- map["status"]
        message <- map["message"]
    }

}

struct Card : Mappable {
    var id : String?
    var object : String?
    var address_city : String?
    var address_country : String?
    var address_line1 : String?
    var address_line1_check : String?
    var address_line2 : String?
    var address_state : String?
    var address_zip : String?
    var address_zip_check : String?
    var brand : String?
    var country : String?
    var cvc_check : String?
    var dynamic_last4 : String?
    var exp_month : Int?
    var exp_year : Int?
    var funding : String?
    var last4 : String?
    var metadata : [String]?
    var name : String?
    var tokenization_method : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        object <- map["object"]
        address_city <- map["address_city"]
        address_country <- map["address_country"]
        address_line1 <- map["address_line1"]
        address_line1_check <- map["address_line1_check"]
        address_line2 <- map["address_line2"]
        address_state <- map["address_state"]
        address_zip <- map["address_zip"]
        address_zip_check <- map["address_zip_check"]
        brand <- map["brand"]
        country <- map["country"]
        cvc_check <- map["cvc_check"]
        dynamic_last4 <- map["dynamic_last4"]
        exp_month <- map["exp_month"]
        exp_year <- map["exp_year"]
        funding <- map["funding"]
        last4 <- map["last4"]
        metadata <- map["metadata"]
        name <- map["name"]
        tokenization_method <- map["tokenization_method"]
    }

}
