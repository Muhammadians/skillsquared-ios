//
//  OrdersModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 23/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper


struct OrdersObject : Mappable {
    
    var sellerordersfrombuyers : Sellerordersfrombuyers?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        sellerordersfrombuyers <- map["orders"]
    }
}

struct Sellerordersfrombuyers : Mappable {
    
    var aactive : [Aactive]?
    var adelivered : [Adelivered]?
    var acompleted : [Acompleted]?
    var arevsion : [Arevsion]?
    var adisputed : [Adisputed]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        aactive <- map["aactive"]
        adelivered <- map["adelivered"]
        acompleted <- map["acompleted"]
        arevsion <- map["arevsion"]
        adisputed <- map["adisputed"]
    }
    
}

struct Sellerordersfrombuyers_custom : Mappable {
    
    var aactive : [Aactive]?
    var adelivered : [Adelivered]?
    var acompleted : [Acompleted]?
    var arevsion : [Arevsion]?
    var adisputed : [Adisputed]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        aactive <- map["aactive"]
        adelivered <- map["adelivered"]
        acompleted <- map["acompleted"]
        arevsion <- map["arevsion"]
        adisputed <- map["adisputed"]
    }

}

struct Aactive : Mappable {
    
    var id : String?
    var created_on : String?
    var seller_offer_request_id : String?
    var seller_id : String?
    var payment_order_id : String?
    var status : String?
    var price : String?
    var work_duration : String?
    var seller_service_info : Seller_service_info?
    var buyer_name : String?
    var buyer_image : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        created_on <- map["created_on"]
        seller_offer_request_id <- map["seller_offer_request_id"]
        seller_id <- map["seller_id"]
        payment_order_id <- map["payment_order_id"]
        status <- map["status"]
        price <- map["request_price"]
        work_duration <- map["work_duration"]
        seller_service_info <- map["seller_service_info"]
        buyer_name <- map["buyer_name"]
        buyer_image <- map["buyer_image"]
    }
    
}


struct Adelivered : Mappable {
    
    var id : String?
    var created_on : String?
    var seller_offer_request_id : String?
    var seller_id : String?
    var payment_order_id : String?
    var status : String?
    var price : String?
    var work_duration : String?
    var seller_service_info : Seller_service_info?
    var buyer_name : String?
    var buyer_image : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        created_on <- map["created_on"]
        seller_offer_request_id <- map["seller_offer_request_id"]
        seller_id <- map["seller_id"]
        payment_order_id <- map["payment_order_id"]
        status <- map["status"]
        price <- map["request_price"]
        work_duration <- map["work_duration"]
        seller_service_info <- map["seller_service_info"]
        buyer_name <- map["buyer_name"]
        buyer_image <- map["buyer_image"]
    }

}


struct Acompleted : Mappable {
    
    var id : String?
    var created_on : String?
    var seller_offer_request_id : String?
    var seller_id : String?
    var payment_order_id : String?
    var status : String?
    var price : String?
    var work_duration : String?
    var seller_service_info : Seller_service_info?
    var buyer_name : String?
    var buyer_image : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        created_on <- map["created_on"]
        seller_offer_request_id <- map["seller_offer_request_id"]
        seller_id <- map["seller_id"]
        payment_order_id <- map["payment_order_id"]
        status <- map["status"]
        price <- map["request_price"]
        work_duration <- map["work_duration"]
        seller_service_info <- map["seller_service_info"]
        buyer_name <- map["buyer_name"]
        buyer_image <- map["buyer_image"]
    }

}

struct Arevsion : Mappable {
    
    var id : String?
    var created_on : String?
    var seller_offer_request_id : String?
    var seller_id : String?
    var payment_order_id : String?
    var status : String?
    var price : String?
    var work_duration : String?
    var seller_service_info : Seller_service_info?
    var buyer_name : String?
    var buyer_image : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        created_on <- map["created_on"]
        seller_offer_request_id <- map["seller_offer_request_id"]
        seller_id <- map["seller_id"]
        payment_order_id <- map["payment_order_id"]
        status <- map["status"]
        price <- map["request_price"]
        work_duration <- map["work_duration"]
        seller_service_info <- map["seller_service_info"]
        buyer_name <- map["buyer_name"]
        buyer_image <- map["buyer_image"]
    }

}


struct Adisputed : Mappable {
    
    var id : String?
    var created_on : String?
    var seller_offer_request_id : String?
    var seller_id : String?
    var payment_order_id : String?
    var status : String?
    var price : String?
    var work_duration : String?
    var seller_service_info : Seller_service_info?
    var buyer_name : String?
    var buyer_image : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        created_on <- map["created_on"]
        seller_offer_request_id <- map["seller_offer_request_id"]
        seller_id <- map["seller_id"]
        payment_order_id <- map["payment_order_id"]
        status <- map["status"]
        price <- map["request_price"]
        work_duration <- map["work_duration"]
        seller_service_info <- map["seller_service_info"]
        buyer_name <- map["buyer_name"]
        buyer_image <- map["buyer_image"]
    }

}


struct Seller_service_info : Mappable {
    var title : String?
    var price : String?
    var delivery : String?
    var service_id : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        title <- map["title"]
        price <- map["price"]
        delivery <- map["delivery"]
        service_id <- map["service_id"]
    }

}




struct OrderDetailObj : Mappable {
    var sellerorderdetail : [Sellerorderdetail]?
    var disputedisexit : Int?
    var orderstatusbeforesellersubmisson : String?
    var buyerdisputedmessagesandresponse : [String]?
    var disputereason : [Disputereason]?
    var processing_fee_type : String?
    var orderStatus : String?
    var deliverNote : DeliverNote?
    var deliverdornot : Bool?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        sellerorderdetail <- map["sellerorderdetail"]
        disputedisexit <- map["disputedisexit"]
        orderstatusbeforesellersubmisson <- map["orderstatusbeforesellersubmisson"]
        buyerdisputedmessagesandresponse <- map["buyerdisputedmessagesandresponse"]
        disputereason <- map["disputereason"]
        processing_fee_type <- map["processing_fee_type"]
        orderStatus <- map["orderStatus"]
        deliverNote <- map["deliverNote"]
        deliverdornot <- map["deliverdornot"]
    }
    
}

struct Disputereason : Mappable {
    var id : String?
    var title : String?
    var type : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        title <- map["title"]
        type <- map["type"]
    }
}

struct Sellerorderdetail : Mappable {
    
    var order_id : String?
    var buyer_id : String?
    var service_id : String?
    var price : String?
    var duration : String?
    var orderNo : String?
    var amount : String?
    var ordertype : String?
    var created_on : String?
    var payment_id : String?
    var offer_description : String?
    var payer_name : String?
    var service_detail : String?
    
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        order_id <- map["orderderid"]
        buyer_id <- map["buyer_id"]
        service_id <- map["service_id"]
        price <- map["price"]
        duration <- map["work_duration"]
        orderNo <- map["orderNo"]
        amount <- map["amount"]
        ordertype <- map["ordertype"]
        created_on <- map["created_on"]
        payment_id <- map["payment_id"]
        offer_description <- map["offer_description"]
        payer_name <- map["payer_name"]
        service_detail <- map["service_detail"]
    }
}

struct DeliverNote : Mappable {
    var description : String?
    var delivered_time : String?
    var file : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        description <- map["description"]
        delivered_time <- map["delivered_time"]
        file <- map["file"]
    }
    
}
