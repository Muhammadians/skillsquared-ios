//
//  PostedRequestModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 19/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct PostedRequestObj : Mappable {
    var buyerrequests : Buyerrequests?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        buyerrequests <- map["buyerrequests"]
    }
}

struct Buyerrequests : Mappable {
    var approve : [Approve]?
    var unapproved : [Unapproved]?
    var pending : [Pending]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        approve <- map["approve"]
        unapproved <- map["unapproved"]
        pending <- map["pending"]
    }

}

struct Approve : Mappable {
    var id : String?
    var buyer_id : String?
    var category_id : String?
    var description : String?
    var budget : String?
    var delievry : String?
    var require_doc : Bool?
    var status : String?
    var created_date : String?
    var assign_to_seller_offer_request_id : String?
    var totaloffer : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        buyer_id <- map["buyer_id"]
        category_id <- map["category_id"]
        description <- map["description"]
        budget <- map["budget"]
        delievry <- map["delievry"]
        require_doc <- map["require_doc"]
        status <- map["status"]
        created_date <- map["created_date"]
        assign_to_seller_offer_request_id <- map["assign_to_seller_offer_request_id"]
        totaloffer <- map["totaloffer"]
    }

}

struct Pending : Mappable {
    var id : String?
    var buyer_id : String?
    var category_id : String?
    var description : String?
    var budget : String?
    var delievry : String?
    var require_doc : Bool?
    var status : String?
    var assigned_by_admin : String?
    var created_date : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        buyer_id <- map["buyer_id"]
        category_id <- map["category_id"]
        description <- map["description"]
        budget <- map["budget"]
        delievry <- map["delievry"]
        require_doc <- map["require_doc"]
        status <- map["status"]
        assigned_by_admin <- map["assigned_by_admin"]
        created_date <- map["created_date"]
    }

}

struct Unapproved : Mappable {
    var id : String?
    var buyer_id : String?
    var category_id : String?
    var description : String?
    var budget : String?
    var delievry : String?
    var require_doc : Bool?
    var status : String?
    var assigned_by_admin : String?
    var created_date : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        buyer_id <- map["buyer_id"]
        category_id <- map["category_id"]
        description <- map["description"]
        budget <- map["budget"]
        delievry <- map["delievry"]
        require_doc <- map["require_doc"]
        status <- map["status"]
        assigned_by_admin <- map["assigned_by_admin"]
        created_date <- map["created_date"]
    }
}




//struct PostedRequestObj : Codable {
//    let buyerrequests : Buyerrequests?
//
//    enum CodingKeys: String, CodingKey {
//        case buyerrequests = "buyerrequests"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        buyerrequests = try values.decodeIfPresent(Buyerrequests.self, forKey: .buyerrequests)
//    }
//}
//
//struct Buyerrequests : Codable {
//    let approve : [Approve]?
//    var unapproved : [Unapproved]?
//    let pending : [Pending]?
//
//    enum CodingKeys: String, CodingKey {
//
//        case approve = "approve"
//        case unapproved = "unapproved"
//        case pending = "pending"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        approve = try values.decodeIfPresent([Approve].self, forKey: .approve)
//        unapproved = try values.decodeIfPresent([Unapproved].self, forKey: .unapproved)
//        pending = try values.decodeIfPresent([Pending].self, forKey: .pending)
//
//        if let data = try? values.decodeIfPresent([Unapproved].self, forKey: .unapproved) {
//            unapproved = data
//        } else {
//            unapproved = []
//        }
//
//        if let pendingData = try? values.decodeIfPresent([Pending].self, forKey: .pending) {
//            pending = pendingData
//        } else {
//            pending = []
//        }
//
//
//    }
//}
//
//struct Approve : Codable {
//    let id : String?
//    let buyer_id : String?
//    let category_id : String?
//    let description : String?
//    let budget : String?
//    let delievry : String?
//    let require_doc : Bool?
//    let status : String?
//    let created_date : String?
//    let assign_to_seller_offer_request_id : String?
//    let totaloffer : Int?
//
//    enum CodingKeys: String, CodingKey {
//
//        case id = "id"
//        case buyer_id = "buyer_id"
//        case category_id = "category_id"
//        case description = "description"
//        case budget = "budget"
//        case delievry = "delievry"
//        case require_doc = "require_doc"
//        case status = "status"
//        case created_date = "created_date"
//        case assign_to_seller_offer_request_id = "assign_to_seller_offer_request_id"
//        case totaloffer = "totaloffer"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(String.self, forKey: .id)
//        buyer_id = try values.decodeIfPresent(String.self, forKey: .buyer_id)
//        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//        budget = try values.decodeIfPresent(String.self, forKey: .budget)
//        delievry = try values.decodeIfPresent(String.self, forKey: .delievry)
//        require_doc = try values.decodeIfPresent(Bool.self, forKey: .require_doc)
//        status = try values.decodeIfPresent(String.self, forKey: .status)
//        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
//        assign_to_seller_offer_request_id = try values.decodeIfPresent(String.self, forKey: .assign_to_seller_offer_request_id)
//        totaloffer = try values.decodeIfPresent(Int.self, forKey: .totaloffer)
//    }
//
//}
//
//
//
//struct Pending : Codable {
//    let id : String?
//    let buyer_id : String?
//    let category_id : String?
//    let description : String?
//    let budget : String?
//    let delievry : String?
//    let require_doc : Bool?
//    let status : String?
//    let assigned_by_admin : String?
//    let created_date : String?
//
//    enum CodingKeys: String, CodingKey {
//
//        case id = "id"
//        case buyer_id = "buyer_id"
//        case category_id = "category_id"
//        case description = "description"
//        case budget = "budget"
//        case delievry = "delievry"
//        case require_doc = "require_doc"
//        case status = "status"
//        case assigned_by_admin = "assigned_by_admin"
//        case created_date = "created_date"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(String.self, forKey: .id)
//        buyer_id = try values.decodeIfPresent(String.self, forKey: .buyer_id)
//        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//        budget = try values.decodeIfPresent(String.self, forKey: .budget)
//        delievry = try values.decodeIfPresent(String.self, forKey: .delievry)
//        require_doc = try values.decodeIfPresent(Bool.self, forKey: .require_doc)
//        status = try values.decodeIfPresent(String.self, forKey: .status)
//        assigned_by_admin = try values.decodeIfPresent(String.self, forKey: .assigned_by_admin)
//        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
//    }
//
//}
//
//struct Unapproved : Codable {
//    let id : String?
//    let buyer_id : String?
//    let category_id : String?
//    let description : String?
//    let budget : String?
//    let delievry : String?
//    let require_doc : Bool?
//    let status : String?
//    let assigned_by_admin : String?
//    let created_date : String?
//
//    enum CodingKeys: String, CodingKey {
//
//        case id = "id"
//        case buyer_id = "buyer_id"
//        case category_id = "category_id"
//        case description = "description"
//        case budget = "budget"
//        case delievry = "delievry"
//        case require_doc = "require_doc"
//        case status = "status"
//        case assigned_by_admin = "assigned_by_admin"
//        case created_date = "created_date"
//    }
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        id = try values.decodeIfPresent(String.self, forKey: .id)
//        buyer_id = try values.decodeIfPresent(String.self, forKey: .buyer_id)
//        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
//        description = try values.decodeIfPresent(String.self, forKey: .description)
//        budget = try values.decodeIfPresent(String.self, forKey: .budget)
//        delievry = try values.decodeIfPresent(String.self, forKey: .delievry)
//        require_doc = try values.decodeIfPresent(Bool.self, forKey: .require_doc)
//        status = try values.decodeIfPresent(String.self, forKey: .status)
//        assigned_by_admin = try values.decodeIfPresent(String.self, forKey: .assigned_by_admin)
//        created_date = try values.decodeIfPresent(String.self, forKey: .created_date)
//    }
//}
