//
//  subCategories.swift
//  SkillSquared
//
//  Created by Awais Aslam on 11/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation

struct Cat_Object : Codable {
    let object : [Object]?

    enum CodingKeys: String, CodingKey {

        case object = "object"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        object = try values.decodeIfPresent([Object].self, forKey: .object)
    }

}

struct Object : Codable {
    let cat_id : String?
    let title : String?
    let page_banner : String?

    enum CodingKeys: String, CodingKey {

        case cat_id = "cat_id"
        case title = "title"
        case page_banner = "page_banner"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cat_id = try values.decodeIfPresent(String.self, forKey: .cat_id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        page_banner = try values.decodeIfPresent(String.self, forKey: .page_banner)
    }

}
