//
//  UpdateProfileModel.swift
//  SkillSquared
//
//  Created by Awais Aslam on 09/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct updateProfileObject : Mappable {
    var status : Int?
    var uerprofile : [Uerprofile]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        uerprofile <- map["uerprofile"]
    }
}

struct Uerprofile : Mappable {
    var name : String?
    var email : String?
    var phone : String?
    var address : String?
    var cnic : String?
    var username : String?
    var profilePic : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
        address <- map["address"]
        cnic <- map["cnic"]
        username <- map["username"]
        profilePic <- map["profilePic"]
    }

}
