//
//  BecomeFreelancerModel.swift
//  SkillSquared
//
//  Created by Awais Aslam on 13/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper


struct MainObject : Mappable {
    var countries : [Countries]?
    var languages : [String]?
    var catoptions : [Catoptions]?
    var loc_data : Loc_data?
    var userMainInfo : UserMainInfo?
    var userFreelancerInfo : UserFreelancerInfo?
    var address : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        countries <- map["countries"]
        languages <- map["languages"]
        catoptions <- map["catoptions"]
        loc_data <- map["loc_data"]
        userMainInfo <- map["userMainInfo"]
        userFreelancerInfo <- map["userFreelancerInfo"]
        address <- map["address"]
    }

}

struct Countries : Mappable {
    var name : String?
    var id : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        name <- map["name"]
        id <- map["id"]
    }

}

struct Loc_data : Mappable {
    var id : String?
    var location_type : String?
    var country : String?
    var state : String?
    var city : String?
    var address : String?
    var street : String?
    var suite_apt : String?
    var zipcode : String?
    var location : String?
    var latitude : String?
    var longitude : String?
    var formatted_address : String?
    var zoom : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        location_type <- map["location_type"]
        country <- map["country"]
        state <- map["state"]
        city <- map["city"]
        address <- map["address"]
        street <- map["street"]
        suite_apt <- map["suite_apt"]
        zipcode <- map["zipcode"]
        location <- map["location"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        formatted_address <- map["formatted_address"]
        zoom <- map["zoom"]
    }

}

struct Catoptions : Mappable {
    var cat_id : String?
    var title : String?
    var subcat : [Subcat]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        cat_id <- map["cat_id"]
        title <- map["title"]
        subcat <- map["subcat"]
    }

}

struct Subcat_child : Mappable {
    var id : String?
    var title : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        title <- map["title"]
    }

}


struct Subcat : Mappable {
    var id : String?
    var title : String?
    var subcat_child : [Subcat_child]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        title <- map["title"]
        subcat_child <- map["subcat_child"]
    }

}

struct UserFreelancerInfo : Mappable {
    var id : String?
    var balance : String?
    var user_id : String?
    var category_id : String?
    var username : String?
    var freelancer_title : String?
    var description : String?
    var availablity : String?
    var lang : String?
    var cnic : String?
    var location_id : String?
    var facebbok : String?
    var linked_in : String?
    var portfolio : String?
    var google_plus : String?
    var twitter : String?
    var instagram : String?
    var type : String?
    var created_on : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        balance <- map["balance"]
        user_id <- map["user_id"]
        category_id <- map["category_id"]
        username <- map["username"]
        freelancer_title <- map["freelancer_title"]
        description <- map["description"]
        availablity <- map["availablity"]
        lang <- map["lang"]
        cnic <- map["cnic"]
        location_id <- map["location_id"]
        facebbok <- map["facebbok"]
        linked_in <- map["linked_in"]
        portfolio <- map["portfolio"]
        google_plus <- map["google_plus"]
        twitter <- map["twitter"]
        instagram <- map["instagram"]
        type <- map["type"]
        created_on <- map["created_on"]
    }

}

struct UserMainInfo : Mappable {
    var id : String?
    var name : String?
    var username : String?
    var email : String?
    var image : String?
    var address : String?
    var latitude : String?
    var longitude : String?
    var city : String?
    var postal_code : String?
    var user_type : String?
    var device_id : String?
    var devicetype : String?
    var social_id : String?
    var social_type : String?
    var added_by : String?
    var phone : String?
    var lang : String?
    var cnic : String?
    var mobile : String?
    var active : String?
    var ip_address : String?
    var password : String?
    var viewpw : String?
    var upw_hash : String?
    var currency_id : String?
    var salt : String?
    var activation_code : String?
    var forgotten_password_code : String?
    var forgotten_password_time : String?
    var remember_code : String?
    var created_on : String?
    var last_login : String?
    var online : String?
    var referal_code : String?
    var last_active_timestamp : String?
    var timezone : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        name <- map["name"]
        username <- map["username"]
        email <- map["email"]
        image <- map["image"]
        address <- map["address"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        city <- map["city"]
        postal_code <- map["postal_code"]
        user_type <- map["user_type"]
        device_id <- map["device_id"]
        devicetype <- map["devicetype"]
        social_id <- map["social_id"]
        social_type <- map["social_type"]
        added_by <- map["added_by"]
        phone <- map["phone"]
        lang <- map["lang"]
        cnic <- map["cnic"]
        mobile <- map["mobile"]
        active <- map["active"]
        ip_address <- map["ip_address"]
        password <- map["password"]
        viewpw <- map["viewpw"]
        upw_hash <- map["upw_hash"]
        currency_id <- map["currency_id"]
        salt <- map["salt"]
        activation_code <- map["activation_code"]
        forgotten_password_code <- map["forgotten_password_code"]
        forgotten_password_time <- map["forgotten_password_time"]
        remember_code <- map["remember_code"]
        created_on <- map["created_on"]
        last_login <- map["last_login"]
        online <- map["online"]
        referal_code <- map["referal_code"]
        last_active_timestamp <- map["last_active_timestamp"]
        timezone <- map["timezone"]
    }
}


struct stateArr : Codable {
    let name : String?
    let id : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        id = try values.decodeIfPresent(String.self, forKey: .id)
    }
}

struct CityArrObj : Codable {
    let name : String?
    let id : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case id = "id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        id = try values.decodeIfPresent(String.self, forKey: .id)
    }
}
