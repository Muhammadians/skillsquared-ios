//
//  ViewOffersModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 20/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation

struct ViewOffersObject : Codable {
    let abuyerrequest : Abuyerrequest?
    let asellerrequest : [Asellerrequest]?
    let ordercheck : Int?
    let job_awarded_seller_id : String?

    enum CodingKeys: String, CodingKey {

        case abuyerrequest = "abuyerrequest"
        case asellerrequest = "asellerrequest"
        case ordercheck = "ordercheck"
        case job_awarded_seller_id = "job_awarded_seller_id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        abuyerrequest = try values.decodeIfPresent(Abuyerrequest.self, forKey: .abuyerrequest)
        asellerrequest = try values.decodeIfPresent([Asellerrequest].self, forKey: .asellerrequest)
        ordercheck = try values.decodeIfPresent(Int.self, forKey: .ordercheck)
        job_awarded_seller_id = try values.decodeIfPresent(String.self, forKey: .job_awarded_seller_id)
    }

}

struct Abuyerrequest : Codable {
    let buyer_id : String?
    let id : String?
    let description : String?
    let budget : String?
    let delievry : String?
    let status : String?
    let buyerimage : String?
    let buyer_username : String?

    enum CodingKeys: String, CodingKey {

        case buyer_id = "buyer_id"
        case id = "id"
        case description = "description"
        case budget = "budget"
        case delievry = "delievry"
        case status = "status"
        case buyerimage = "buyerimage"
        case buyer_username = "buyer_username"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        buyer_id = try values.decodeIfPresent(String.self, forKey: .buyer_id)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        budget = try values.decodeIfPresent(String.self, forKey: .budget)
        delievry = try values.decodeIfPresent(String.self, forKey: .delievry)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        buyerimage = try values.decodeIfPresent(String.self, forKey: .buyerimage)
        buyer_username = try values.decodeIfPresent(String.self, forKey: .buyer_username)
    }

}

struct Asellerrequest : Codable {
    let freelancer_id : String?
    let username : String?
    let freelancer_name: String?
    let freelancer_title : String?
    let request_id : String?
    let description : String?
    let price : String?
    let service_id : String?
    let freelancerimage : String?
    let duration : String?
    let frellancer_location : String?
    let seller_id : String?
    let freelancer_skills : String?
    let avgRating : Int?
    let totalPay : Int?
    let seller_name: String?
    let seller_email : String?

    enum CodingKeys: String, CodingKey {

        case freelancer_id = "freelancer_id"
        case username = "username"
        case freelancer_name = "freelancer_username"
        case freelancer_title = "freelancer_title"
        case request_id = "request_id"
        case description = "description"
        case price = "price"
        case service_id = "service_id"
        case freelancerimage = "freelancerimage"
        case duration = "duration"
        case frellancer_location = "frellancer_location"
        case seller_id = "seller_id"
        case freelancer_skills = "freelancer_skills"
        case avgRating = "avgRating"
        case totalPay = "totalToPay"
        case seller_name = "name"
        case seller_email = "email"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        freelancer_id = try values.decodeIfPresent(String.self, forKey: .freelancer_id)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        freelancer_name = try values.decodeIfPresent(String.self, forKey: .freelancer_name)
        freelancer_title = try values.decodeIfPresent(String.self, forKey: .freelancer_title)
        request_id = try values.decodeIfPresent(String.self, forKey: .request_id)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        service_id = try values.decodeIfPresent(String.self, forKey: .service_id)
        freelancerimage = try values.decodeIfPresent(String.self, forKey: .freelancerimage)
        duration = try values.decodeIfPresent(String.self, forKey: .duration)
        frellancer_location = try values.decodeIfPresent(String.self, forKey: .frellancer_location)
        seller_id = try values.decodeIfPresent(String.self, forKey: .seller_id)
        freelancer_skills = try values.decodeIfPresent(String.self, forKey: .freelancer_skills)
        avgRating = try values.decodeIfPresent(Int.self, forKey: .avgRating)
        totalPay = try values.decodeIfPresent(Int.self, forKey: .totalPay)
        seller_name = try values.decodeIfPresent(String.self, forKey: .seller_name)
        seller_email = try values.decodeIfPresent(String.self, forKey: .seller_email)
        
    }
}
