//
//  EarningsModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 21/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct EarningsObject : Mappable {
    var seller_earnings : Int?
    var avgSellingPrice : Int?
    var seller_earnings_current_month : Int?
    var total_completed_orders : Int?
    var paystack: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        seller_earnings <- map["seller_earnings"]
        avgSellingPrice <- map["avgSellingPrice"]
        seller_earnings_current_month <- map["seller_earnings_current_month"]
        total_completed_orders <- map["total_completed_orders"]
        paystack <- map["paystack"]
    }
    
}
