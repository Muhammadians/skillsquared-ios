//
//  Login.swift
//  SkillSquared
//
//  Created by Awais Aslam on 04/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct Login : Mappable {
       var freelancer : Bool?
       var freelancerProfileSet : Bool?
       var freelancer_id : Int?
       var status : Int?
       var message : String?
       var skillSquaredUser : SkillSquaredUser?

       init?(map: Map) {

       }

       mutating func mapping(map: Map) {

           freelancer <- map["freelancer"]
           freelancerProfileSet <- map["freelancerProfileSet"]
           freelancer_id <- map["freelancer_id"]
           status <- map["status"]
           message <- map["message"]
           skillSquaredUser <- map["skillSquaredUser"]
       }
   }



// MARK: - User
struct SkillSquaredUser : Mappable {
    var user_id : String?
    var username : String?
    var name : String?
    var email : String?
    var phone : String?
    var address : String?
    var image : String?
    var timezone : String?
    var access_token : String?
    var referal_url : String?
    var freelancerProfileSet: Bool?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        user_id <- map["user_id"]
        username <- map["username"]
        name <- map["name"]
        email <- map["email"]
        phone <- map["h"]
        address <- map["address"]
        image <- map["image"]
        timezone <- map["timezone"]
        access_token <- map["access_token"]
        referal_url <- map["referal_url"]
        freelancerProfileSet <- map["freelancer"]
        
    }
}
