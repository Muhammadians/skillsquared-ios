//
//  ManageOrderModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 28/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct ManageOrderObject : Mappable {
    
    var buyerOrders : BuyerOrderManagment?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        buyerOrders <- map["orders"]
       
    }
}

struct BuyerOrderManagment : Mappable {
    
    var aactive : [BuyerAactive]?
    var amanage : [Amanage]?
    var acompleted : [BuyerAcompleted]?
    var arevsion : [BuyerArevsion]?
    var adisputed : [BuyerAdisputed]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        aactive <- map["aactive"]
        amanage <- map["amanage"]
        acompleted <- map["acompleted"]
        arevsion <- map["arevsion"]
        adisputed <- map["adisputed"]
    }
}

struct BuyerCustomOrders : Mappable {
    
    var aactive : [BuyerAactive]?
    var amanage : [Amanage]?
    var acompleted : [BuyerAcompleted]?
    var arevsion : [BuyerArevsion]?
    var adisputed : [BuyerAdisputed]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        aactive <- map["aactive"]
        amanage <- map["amanage"]
        acompleted <- map["acompleted"]
        arevsion <- map["arevsion"]
        adisputed <- map["adisputed"]
    }
}


struct Amanage : Mappable {
    
    var id : String?
    var created_on : String?
    var seller_offer_request_id : String?
    var seller_id : String?
    var payment_order_id : String?
    var status : String?
    var price : String?
    var work_duration : String?
    var seller_service_info : Seller_service_info?
    var sellerName : String?
    var sellerImage : String?
    var user_quote_duration: String?
    var offerPrice: String?
    var description: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        created_on <- map["created_on"]
        seller_offer_request_id <- map["seller_offer_request_id"]
        seller_id <- map["seller_id"]
        payment_order_id <- map["payment_order_id"]
        status <- map["status"]
        price <- map["request_price"]
        work_duration <- map["work_duration"]
        seller_service_info <- map["seller_service_info"]
        sellerName <- map["seller_name"]
        sellerImage <- map["seller_image"]
        user_quote_duration <- map["user_duartion_qoute"]
        offerPrice <- map["price"]
        description <- map["requirements"]
    }
}

struct BuyerAactive : Mappable {
    
    var id : String?
    var created_on : String?
    var seller_offer_request_id : String?
    var seller_id : String?
    var payment_order_id : String?
    var status : String?
    var price : String?
    var work_duration : String?
    var seller_service_info : Seller_service_info?
    var sellerName : String?
    var sellerImage : String?
    var user_quote_duration: String?
    var offerPrice: String?
    var description: String?


    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        created_on <- map["created_on"]
        seller_offer_request_id <- map["seller_offer_request_id"]
        seller_id <- map["seller_id"]
        payment_order_id <- map["payment_order_id"]
        status <- map["status"]
        price <- map["request_price"]
        work_duration <- map["work_duration"]
        seller_service_info <- map["seller_service_info"]
        sellerName <- map["seller_name"]
        sellerImage <- map["seller_image"]
        user_quote_duration <- map["user_duartion_qoute"]
        offerPrice <- map["price"]
        description <- map["requirements"]
    }
    
}

struct BuyerAcompleted : Mappable {
    
    var id : String?
    var created_on : String?
    var seller_offer_request_id : String?
    var seller_id : String?
    var payment_order_id : String?
    var status : String?
    var price : String?
    var work_duration : String?
    var seller_service_info : Seller_service_info?
    var sellerName : String?
    var sellerImage : String?
    var user_quote_duration: String?
    var offerPrice: String?
    var description: String?


    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        created_on <- map["created_on"]
        seller_offer_request_id <- map["seller_offer_request_id"]
        seller_id <- map["seller_id"]
        payment_order_id <- map["payment_order_id"]
        status <- map["status"]
        price <- map["request_price"]
        work_duration <- map["work_duration"]
        seller_service_info <- map["seller_service_info"]
        sellerName <- map["seller_name"]
        sellerImage <- map["seller_image"]
        user_quote_duration <- map["user_duartion_qoute"]
        offerPrice <- map["price"]
        description <- map["requirements"]
    }

}

struct BuyerArevsion : Mappable {
    
    var id : String?
    var created_on : String?
    var seller_offer_request_id : String?
    var seller_id : String?
    var payment_order_id : String?
    var status : String?
    var price : String?
    var work_duration : String?
    var seller_service_info : Seller_service_info?
    var sellerName : String?
    var sellerImage : String?
    var user_quote_duration: String?
    var offerPrice: String?
    var description: String?


    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        created_on <- map["created_on"]
        seller_offer_request_id <- map["seller_offer_request_id"]
        seller_id <- map["seller_id"]
        payment_order_id <- map["payment_order_id"]
        status <- map["status"]
        price <- map["request_price"]
        work_duration <- map["work_duration"]
        seller_service_info <- map["seller_service_info"]
        sellerName <- map["seller_name"]
        sellerImage <- map["seller_image"]
        user_quote_duration <- map["user_duartion_qoute"]
        offerPrice <- map["price"]
        description <- map["requirements"]
    }

}


struct BuyerAdisputed : Mappable {
    
    var id : String?
    var created_on : String?
    var seller_offer_request_id : String?
    var seller_id : String?
    var payment_order_id : String?
    var status : String?
    var price : String?
    var work_duration : String?
    var seller_service_info : Seller_service_info?
    var sellerName : String?
    var sellerImage : String?
    var user_quote_duration: String?
    var offerPrice: String?
    var description: String?


    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        created_on <- map["created_on"]
        seller_offer_request_id <- map["seller_offer_request_id"]
        seller_id <- map["seller_id"]
        payment_order_id <- map["payment_order_id"]
        status <- map["status"]
        price <- map["request_price"]
        work_duration <- map["work_duration"]
        seller_service_info <- map["seller_service_info"]
        sellerName <- map["seller_name"]
        sellerImage <- map["seller_image"]
        user_quote_duration <- map["user_duartion_qoute"]
        offerPrice <- map["price"]
        description <- map["requirements"]
    }

}



struct BuyerOrderDetailObj : Mappable {
    var sellerorderdetail : [Sellerorderdetail]?
    var orderstatusbeforesellersubmisson : String?
    var disputedisexit : Int?
    var processing_fee_type : String?
    var disputereason : [Disputereason]?
    var status : Status?
    var review : Bool?
    var deliveryNote: DeliverNote?
    var orderStatus: String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        sellerorderdetail <- map["sellerorderdetail"]
        orderstatusbeforesellersubmisson <- map["orderstatusbeforesellersubmisson"]
        disputedisexit <- map["disputedisexit"]
        processing_fee_type <- map["processing_fee_type"]
        disputereason <- map["disputereason"]
        status <- map["status"]
        review <- map["review"]
        deliveryNote <- map["deliverNote"]
        orderStatus <- map["orderStatus"]
        
    }

}


struct Status : Mappable {
    var amount : String?
    var payment_id : String?
    var service_id : String?
    var status_id : String?
    var statusTitle : String?
    var sellerName : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        amount <- map["amount"]
        payment_id <- map["payment_id"]
        service_id <- map["service_id"]
        status_id <- map["status_id"]
        statusTitle <- map["statusTitle"]
        sellerName <- map["sellerName"]
    }

}
