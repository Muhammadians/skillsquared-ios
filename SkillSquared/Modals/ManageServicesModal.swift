//
//  ManageServicesModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 23/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct ManageServicesObject : Mappable {
   var inactive_gigs_services : String?
        var active_gigs_services : [Active_gigs_services]?
        var pending_gigs_services : String?

        init?(map: Map) {

        }

        mutating func mapping(map: Map) {

            inactive_gigs_services <- map["inactive_gigs_services"]
            active_gigs_services <- map["active_gigs_services"]
            pending_gigs_services <- map["pending_gigs_services"]
    }
}
