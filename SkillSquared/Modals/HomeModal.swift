//
//  HomeModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 08/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation

struct HomeModalObject : Codable {
    
    let banners : [String]?
    let categories : [Categories]?
    let featured_services : [Featured_services]?
    

    enum CodingKeys: String, CodingKey {
        
        case banners = "banners"
        case categories = "categories"
        case featured_services = "featured_services"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        categories = try values.decodeIfPresent([Categories].self, forKey: .categories)
        featured_services = try values.decodeIfPresent([Featured_services].self, forKey: .featured_services)
        banners = try values.decodeIfPresent([String].self, forKey: .banners)
    }

}

struct Categories : Codable {
    let cat_id : String?
    let title : String?
    let page_banner : String?

    enum CodingKeys: String, CodingKey {

        case cat_id = "cat_id"
        case title = "title"
        case page_banner = "page_banner"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cat_id = try values.decodeIfPresent(String.self, forKey: .cat_id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        page_banner = try values.decodeIfPresent(String.self, forKey: .page_banner)
    }

}


struct Featured_services : Codable {
    let id : String?
    let category_id : String?
    let service_title : String?
    let description : String?
    let uniquekey : String?
    let servicekeyword : String?
    let price : String?
    let category_name : String?
    let current_status : String?
    let media : String?
    let service_username : String?
    let user_image : String?
    let service_rating : String?
    let name : String?
    let email : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case category_id = "category_id"
        case service_title = "service_title"
        case description = "description"
        case uniquekey = "uniquekey"
        case servicekeyword = "servicekeyword"
        case price = "price"
        case category_name = "category_name"
        case current_status = "current_status"
        case media = "media"
        case service_username = "service_username"
        case user_image = "user_image"
        case service_rating = "service_rating"
        case name = "name"
        case email = "email"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        category_id = try values.decodeIfPresent(String.self, forKey: .category_id)
        service_title = try values.decodeIfPresent(String.self, forKey: .service_title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        uniquekey = try values.decodeIfPresent(String.self, forKey: .uniquekey)
        servicekeyword = try values.decodeIfPresent(String.self, forKey: .servicekeyword)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
        current_status = try values.decodeIfPresent(String.self, forKey: .current_status)
        media = try values.decodeIfPresent(String.self, forKey: .media)
        service_username = try values.decodeIfPresent(String.self, forKey: .service_username)
        user_image = try values.decodeIfPresent(String.self, forKey: .user_image)
        service_rating = try values.decodeIfPresent(String.self, forKey: .service_rating)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
    }

}
