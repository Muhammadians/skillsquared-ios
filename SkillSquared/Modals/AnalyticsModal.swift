//
//  AnalyticsModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 21/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct AnalyticsObject : Mappable {
    var seller_complted_orders_count : Int?
    var seller_earnings_current_month : Int?
    var seller_earnings : Int?
    var seller_analytics : String?
    var avgSellingPrice : Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        seller_complted_orders_count <- map["seller_complted_orders_count"]
        seller_earnings_current_month <- map["seller_earnings_current_month"]
        seller_earnings <- map["seller_earnings"]
        seller_analytics <- map["seller_analytics"]
        avgSellingPrice <- map["avgSellingPrice"]
    }
    
}
