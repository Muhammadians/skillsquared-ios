//
//  GigsModal.swift
//  SkillSquared
//
//  Created by Awais Aslam on 11/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON

struct Gig_object : Mappable {
    var services : [Services]?
    var category_filters : [Category_filters]?
    var price_filters : [String]?
    var total_count : Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        services <- map["services"]
        category_filters <- map["category_filters"]
        price_filters <- map["price_filters"]
        total_count <- map["totalcount"]
    }
}

struct Services : Mappable {
    var id : String?
    var category_id : String?
    var service_title : String?
    var description : String?
    var uniquekey : String?
    var freelancer_image : String?
    var freelancer_username : String?
    var servicekeyword : String?
    var currency_unit : String?
    var price : String?
    var media : String?
    var service_rating : String?
    var liked : Bool?
    var username : String?
    var seller_name : String?
    var seller_email : String?
    var reviews : [Reviews]?
    var freelancer_user_id: Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        category_id <- map["category_id"]
        service_title <- map["service_title"]
        description <- map["description"]
        uniquekey <- map["uniquekey"]
        freelancer_image <- map["freelancer_image"]
        freelancer_username <- map["freelancer_username"]
        servicekeyword <- map["servicekeyword"]
        currency_unit <- map["currency_unit"]
        price <- map["price"]
        media <- map["media"]
        service_rating <- map["service_rating"]
        liked <- map["liked"]
        reviews <- map["reviews"]
        username <- map["username"]
        seller_name <- map["name"]
        seller_email <- map["email"]
        freelancer_user_id <- map["freelancer_user_id"]
    }
    
}


struct Reviews : Mappable {
    var review : String?
    var rating : String?
    var name : String?
    var image : String?
    var username : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        review <- map["review"]
        rating <- map["rating"]
        name <- map["name"]
        image <- map["image"]
        username <- map["username"]
    }
}


struct Category_filters : Mappable {
    var catgory_id : String?
    var category_title : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        catgory_id <- map["catgory_id"]
        category_title <- map["category_title"]
    }
}


struct customOrderObject : Mappable {
    var status : Int?
    var message : String?
    var orderObject : OrderObject?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        message <- map["message"]
        orderObject <- map["orderObject"]
    }
}

struct OrderObject : Mappable {
    var service_id : String?
    var requirements : String?
    var price : String?
    var work_duration : String?
    var buyer_id : Int?
    var seller_id : String?
    var created_on : String?
    var customorderid : Int?

    init?(map: Map) {

    }
    
    mutating func mapping(map: Map) {

        service_id <- map["service_id"]
        requirements <- map["requirements"]
        price <- map["price"]
        work_duration <- map["work_duration"]
        buyer_id <- map["buyer_id"]
        seller_id <- map["seller_id"]
        created_on <- map["created_on"]
        customorderid <- map["customorderid"]
    }
}



struct searchModel
{
    var title : String?

    init() {
        
    }
    
    init(json : JSON)
    {
        self.title = String(json["title"].stringValue)
    }
}
