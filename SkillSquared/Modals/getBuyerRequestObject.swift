//
//  getBuyerRequestObject.swift
//  SkillSquared
//
//  Created by Awais Aslam on 25/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct BuyerObject : Mappable {
    
    var buyer_requests : Buyer_requests?
    var sent_offers : Sent_offers?
    var sellerservices : [Sellerservices]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        buyer_requests <- map["buyer_requests"]
        sent_offers <- map["sent_offers"]
        sellerservices <- map["sellerservices"]
    }
}

struct Buyer_requests : Mappable {
    var buyerequest : [Buyerequest]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        buyerequest <- map["buyerequest"]
    }
}

struct Buyerequest : Mappable {
    var id : String?
    var offer_sent : String?
    var category : String?
    var description : String?
    var user_name : String?
    var budget : String?
    var delievry : String?
    var created_date : String?
    var require_doc : Bool?
    var user_image : String?
    var applide_or_not : String?
    var totaloffer : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        offer_sent <- map["offer_sent"]
        category <- map["category"]
        description <- map["description"]
        user_name <- map["user_name"]
        budget <- map["budget"]
        delievry <- map["delievry"]
        created_date <- map["created_date"]
        require_doc <- map["require_doc"]
        user_image <- map["user_image"]
        applide_or_not <- map["applide_or_not"]
        totaloffer <- map["totaloffer"]
    }
    
}


struct Sent_offers : Mappable {
    var sellersentoffers : [Sellersentoffers]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        sellersentoffers <- map["sellersentoffers"]
    }
    
}


struct Sellersentoffers : Mappable {
    var id : String?
    var offer_sent : String?
    var category : String?
    var description : String?
    var user_name : String?
    var budget : String?
    var delievry : String?
    var created_date : String?
    var require_doc : Bool?
    var user_image : String?
    var totaloffer : String?
    var applide_or_not : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        offer_sent <- map["offer_sent"]
        category <- map["category"]
        description <- map["description"]
        user_name <- map["user_name"]
        budget <- map["budget"]
        delievry <- map["delievry"]
        created_date <- map["created_date"]
        require_doc <- map["require_doc"]
        user_image <- map["user_image"]
        totaloffer <- map["totaloffer"]
        applide_or_not <- map["applide_or_not"]
    }
    
}

struct Sellerservices : Mappable {
    var id : String?
    var title : String?
    var service_image : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        title <- map["title"]
        service_image <- map["service_image"]
    }

}
