//
//  NotificationModel.swift
//  SkillSquared
//
//  Created by Awais Aslam on 29/04/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct NotificationObject : Mappable {
    var status : Int?
    var notifications : [Notifications]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        status <- map["status"]
        notifications <- map["notifications"]
    }
}

struct Notifications : Mappable {
    var id : String?
    var user_id : String?
    var body : String?
    var redirect_detail : String?
    var redirect_link : String?
    var created_on : String?
    var read : String?
    var type : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        user_id <- map["user_id"]
        body <- map["body"]
        redirect_detail <- map["redirect_detail"]
        redirect_link <- map["redirect_link"]
        created_on <- map["created_on"]
        read <- map["read"]
        type <- map["type"]
    }

}
