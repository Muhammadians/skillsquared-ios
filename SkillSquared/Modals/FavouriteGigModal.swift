//
//  FavouriteGigModal\.swift
//  SkillSquared
//
//  Created by Awais Aslam on 03/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct FavouriteGigObject : Mappable {
    var status : Int?
    var favouriteServices : [FavouriteServices]?

    init?(map: Map) {

    }
    mutating func mapping(map: Map) {

        status <- map["status"]
        favouriteServices <- map["favouriteServices"]
    }
}

struct FavouriteServices : Mappable {
    var id : String?
    var user_id : String?
    var category_id : String?
    var title : String?
    var service_keyword : String?
    var unique_id : String?
    var description : String?
    var price : String?
    var actualprice : String?
    var delivery_time : String?
    var additional_info : String?
    var status_by_admin : String?
    var status : String?
    var rating : String?
    var cat_level : String?
    var featured : String?
    var created_on : String?
    var updated_on : String?
    var service_image : String?
    var total_clicks : Int?
    var total_orders : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        user_id <- map["user_id"]
        category_id <- map["category_id"]
        title <- map["title"]
        service_keyword <- map["service_keyword"]
        unique_id <- map["unique_id"]
        description <- map["description"]
        price <- map["price"]
        actualprice <- map["actualprice"]
        delivery_time <- map["delivery_time"]
        additional_info <- map["additional_info"]
        status_by_admin <- map["status_by_admin"]
        status <- map["status"]
        rating <- map["rating"]
        cat_level <- map["cat_level"]
        featured <- map["featured"]
        created_on <- map["created_on"]
        updated_on <- map["updated_on"]
        service_image <- map["service_image"]
        total_clicks <- map["total_clicks"]
        total_orders <- map["total_orders"]
    }
}
