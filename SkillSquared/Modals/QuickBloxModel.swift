//
//  QuickBloxModel.swift
//  SkillSquared
//
//  Created by Awais Aslam on 21/07/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct QuickBloxModel : Mappable {
    var total_entries : Int?
    var skip : Int?
    var limit : Int?
    var items : [QBItems]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        total_entries <- map["total_entries"]
        skip <- map["skip"]
        limit <- map["limit"]
        items <- map["items"]
    }
}

struct QBItems : Mappable {
    var _id : String?
    var created_at : String?
    var last_message : String?
    var last_message_date_sent : Int?
    var last_message_id : String?
    var last_message_user_id : Int?
    var name : String?
    var occupants_ids : [Int]?
    var photo : String?
    var type : Int?
    var updated_at : String?
    var user_id : Int?
    var xmpp_room_jid : String?
    var unread_messages_count : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        _id <- map["_id"]
        created_at <- map["created_at"]
        last_message <- map["last_message"]
        last_message_date_sent <- map["last_message_date_sent"]
        last_message_id <- map["last_message_id"]
        last_message_user_id <- map["last_message_user_id"]
        name <- map["name"]
        occupants_ids <- map["occupants_ids"]
        photo <- map["photo"]
        type <- map["type"]
        updated_at <- map["updated_at"]
        user_id <- map["user_id"]
        xmpp_room_jid <- map["xmpp_room_jid"]
        unread_messages_count <- map["unread_messages_count"]
    }
}


