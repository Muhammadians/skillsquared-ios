//
//  GetSellerProfile.swift
//  SkillSquared
//
//  Created by Awais Aslam on 22/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct GetSellerObject : Mappable {
    var page_title : String?
    var freelancer_user_id : String?
    var avgRating : Int?
    var profileinfo : Profileinfo?
    var skills : [String]?
    var sellerReviews : [SellerReviews]?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        page_title <- map["page_title"]
        freelancer_user_id <- map["freelancer_user_id"]
        avgRating <- map["avgRating"]
        profileinfo <- map["profileinfo"]
        skills <- map["skills"]
        sellerReviews <- map["SellerReviews"]
    }
}

struct Gigs : Mappable {
    var inactive_gigs_services : String?
    var active_gigs_services : [Active_gigs_services]?
    var pending_gigs_services : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        inactive_gigs_services <- map["inactive_gigs_services"]
        active_gigs_services <- map["active_gigs_services"]
        pending_gigs_services <- map["pending_gigs_services"]
    }
}


struct Profileinfo : Mappable {
    var online : Int?
    var sellerProfileinfo : SellerProfileinfo?
    var gigs : Gigs?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        online <- map["online"]
        sellerProfileinfo <- map["SellerProfileinfo"]
        gigs <- map["gigs"]
    }
    
}

struct SellerProfileinfo : Mappable {
    var username : String?
    var freelancer_title : String?
    var description : String?
    var language : String?
    var memberSince : String?
    var location : String?
    var recent_delivery : String?
    var freelancer_image : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        username <- map["username"]
        freelancer_title <- map["freelancer_title"]
        description <- map["description"]
        language <- map["language"]
        memberSince <- map["memberSince"]
        location <- map["location"]
        recent_delivery <- map["recent_delivery"]
        freelancer_image <- map["freelancer_image"]
    }
}

struct Active_gigs_services : Mappable {
    var id : String?
    var user_id : String?
    var category_id : String?
    var title : String?
    var service_keyword : String?
    var unique_id : String?
    var description : String?
    var price : String?
    var actualprice : String?
    var delivery_time : String?
    var additional_info : String?
    var status_by_admin : String?
    var status : String?
    var rating : String?
    var cat_level : String?
    var featured : String?
    var created_on : String?
    var updated_on : String?
    var service_image : String?
    var total_clicks : Int?
    var total_orders : String?

    init?(map: Map) {

    }
    
    mutating func mapping(map: Map) {

        id <- map["id"]
        user_id <- map["user_id"]
        category_id <- map["category_id"]
        title <- map["title"]
        service_keyword <- map["service_keyword"]
        unique_id <- map["unique_id"]
        description <- map["description"]
        price <- map["price"]
        actualprice <- map["actualprice"]
        delivery_time <- map["delivery_time"]
        additional_info <- map["additional_info"]
        status_by_admin <- map["status_by_admin"]
        status <- map["status"]
        rating <- map["rating"]
        cat_level <- map["cat_level"]
        featured <- map["featured"]
        created_on <- map["created_on"]
        updated_on <- map["updated_on"]
        service_image <- map["service_image"]
        total_clicks <- map["total_clicks"]
        total_orders <- map["total_orders"]
    }

}


struct SellerReviews : Mappable{
    var review : String?
    var rating : String?
    var created_date : String?
    var buyerName : String?
    var buyer_image : String?
    
    init?(map: Map) {

       }
    
    mutating func mapping(map: Map) {

        review <- map["review"]
        rating <- map["rating"]
        created_date <- map["created_date"]
        buyerName <- map["buyerName"]
        buyer_image <- map["buyer_image"]
    }
}
