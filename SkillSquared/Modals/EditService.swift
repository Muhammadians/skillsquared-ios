//
//  EditService.swift
//  SkillSquared
//
//  Created by Awais Aslam on 28/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import Foundation
import ObjectMapper

struct EditServiceObj : Mappable {
  var category_id : String?
    var subcategory_id : String?
    var subcategorychild_id : String?
    var service_detail : Service_detail?
    var catoptions : [Catoptions]?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        category_id <- map["category_id"]
        subcategory_id <- map["subcategory_id"]
        subcategorychild_id <- map["subcategorychild_id"]
        service_detail <- map["service_detail"]
        catoptions <- map["catoptions"]
    }
}


struct Service_detail : Mappable {
    var id : String?
    var user_id : String?
    var category_id : String?
    var title : String?
    var service_keyword : String?
    var unique_id : String?
    var description : String?
    var price : String?
    var actualprice : String?
    var delivery_time : String?
    var additional_info : String?
    var status_by_admin : String?
    var status : String?
    var rating : String?
    var cat_level : String?
    var featured : String?
    var created_on : String?
    var updated_on : String?
    var service_id : String?
    var type : String?
    var media : String?
    var media_video : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        user_id <- map["user_id"]
        category_id <- map["category_id"]
        title <- map["title"]
        service_keyword <- map["service_keyword"]
        unique_id <- map["unique_id"]
        description <- map["description"]
        price <- map["price"]
        actualprice <- map["actualprice"]
        delivery_time <- map["delivery_time"]
        additional_info <- map["additional_info"]
        status_by_admin <- map["status_by_admin"]
        status <- map["status"]
        rating <- map["rating"]
        cat_level <- map["cat_level"]
        featured <- map["featured"]
        created_on <- map["created_on"]
        updated_on <- map["updated_on"]
        service_id <- map["service_id"]
        type <- map["type"]
        media <- map["media"]
        media_video <- map["media_video"]
    }
}


struct Subcategory : Mappable {
    var title : String?
    var id : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        title <- map["title"]
        id <- map["id"]
    }
}

struct Subcategorychild : Mappable {
    var id : String?
    var title : String?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        id <- map["id"]
        title <- map["title"]
    }
}
