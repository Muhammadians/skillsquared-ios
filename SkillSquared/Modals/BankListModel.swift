//
//  BankListModel.swift
//  SkillSquared
//
//  Created by Awais Aslam on 08/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import Foundation
import SwiftyJSON

struct BankDataModel
{
    var name : String?
    var slug : String?
    
    init() {
        
    }
    
    init(json : JSON)
    {
        self.name = String(json["name"].stringValue)
        self.slug = String(json["slug"].stringValue)
    }
}
