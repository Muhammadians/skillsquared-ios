//
//  CornerView.swift
//  sample-chat-swift
//
//  Created by Injoit on 1/28/19.
//  Copyright © 2019 Quickblox. All rights reserved.
//

import UIKit

class CornerView: UIView {
    
    //MARK: - Properties
    var bgColor = UIColor.clear {
        didSet {
            setNeedsDisplay()
        }
    }
    var title: String = "" {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var fontSize: CGFloat = 16.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    var touchesEndAction: (() -> Void)?
    
    //MARK: - Life Cycle
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//
//        defaultStyle()
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        defaultStyle()
    }
    
    //MARK: - Overrides
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
//    override func draw(_ rect: CGRect) {
//        draw(withBgColor: bgColor, cornerRadius: cornerRadius, rect: bounds, text: title, fontSize: fontSize)
//    }
    
    override func touchesEnded(_ touches: Set<UITouch>?, with event: UIEvent?) {
        if let touches = touches, let event = event {
            super.touchesEnded(touches, with: event)
        }
        
        UIView.animate(withDuration: 0.4,
                       delay: 0.0,
                       options: [.curveEaseIn, .allowUserInteraction],
                       animations: {
        }, completion: { finished in
            guard let touchesEndAction = self.touchesEndAction else {
                return
            }
            touchesEndAction()
        })
    }
    
    //MARK: - Internal Methods
    private func defaultStyle() {
        contentMode = .redraw
        backgroundColor = .clear
        isUserInteractionEnabled = false
    }
    
//    private func draw(withBgColor bgColor: UIColor, cornerRadius: CGFloat, rect: CGRect, text: String,
//                      fontSize: CGFloat) {
//        let rectanglePath = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius)
//        bgColor.setFill()
//        rectanglePath.fill()
//        let style = NSMutableParagraphStyle()
//        style.alignment = .center
//        let rectangleFontAttributes = [NSAttributedString.Key.font: UIFont(name: CornerViewConstant.fontName,
//                                                                           size: fontSize),
//                                       NSAttributedString.Key.foregroundColor: UIColor.white,
//                                       NSAttributedString.Key.paragraphStyle: style]
//        let attributes = rectangleFontAttributes as [NSAttributedString.Key : Any]
//        let offsetByY = (rect.height - (text.boundingRect(with: rect.size,
//                                                          options: .usesLineFragmentOrigin,
//                                                          attributes: attributes, context: nil).size.height))
//        let rectOffset: CGRect = rect.offsetBy(dx: 0.0, dy: offsetByY / 2)
//        text.draw(in: rectOffset, withAttributes: attributes)
//    }
//    var title: String = ""
//    private var fontSize: Float = 16
//    var cornerRadius:CGFloat = 6
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        contentMode = .redraw
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
    }
    
    func drawWithRect(rect: CGRect, text:String, fontSize:Float){

        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        
		guard let fontAttributeName = UIFont(name: "Helvetica", size: CGFloat(fontSize)) else {
			return
		}
			
        let rectangleFontAttributes: [NSAttributedString.Key: Any] = [.font: fontAttributeName,
                                                                      .foregroundColor: UIColor.white,
                                                                      .paragraphStyle: style]
		
		let rectOffset = rect.offsetBy(dx: 0, dy: ((rect.height - text.boundingRect(with: rect.size, options:.usesLineFragmentOrigin, attributes:rectangleFontAttributes, context: nil).size.height)/2))
		
		NSString(string: text).draw(in: rectOffset, withAttributes: rectangleFontAttributes)
    }
	
    override func draw(_ rect: CGRect) {
        drawWithRect(rect: bounds, text: title, fontSize: Float(fontSize))
    }
}
