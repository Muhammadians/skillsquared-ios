//
//  DarkenedHoleView.swift
//  sample-chat-swift
//
//  Created by Injoit on 18.01.2020.
//  Copyright © 2020 quickBlox. All rights reserved.
//

import UIKit

class DarkenedHoleView: UIView {
    var transparentHoleView: UIView?
    
    private lazy var roundingCorners: UIRectCorner = {
        let roundingCorners = UIRectCorner()
        return roundingCorners
    }()
    var isIncoming = false
   
    var isHasAttachmeht = false
    
    // MARK: - Initialization
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    // MARK: - Drawing
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        guard transparentHoleView != nil else {
            return
        }
       
        self.backgroundColor?.setFill()
        UIRectFill(rect)
        let layer = CAShapeLayer()
        let path = CGMutablePath()
        if isIncoming == false {
            roundingCorners = [.bottomLeft, .topLeft, .topRight]
        } else {
            roundingCorners = [.topLeft, .topRight, .bottomRight]
        }
        
        
        layer.path = path
        layer.fillRule = CAShapeLayerFillRule.evenOdd
        self.layer.mask = layer
    }
}
