//
//  NotificationCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 24/04/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var orderDESCRIPTION: UILabel!
    @IBOutlet weak var orderDate: UILabel!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
