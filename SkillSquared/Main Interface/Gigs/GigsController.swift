//
//  GigsController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 26/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import iOSDropDown
import FloatRatingView
import ObjectMapper
import CoreLocation
import Kingfisher
import Quickblox
import SVProgressHUD

class GigsController: UIViewController, CLLocationManagerDelegate, ModernSearchBarDelegate{
    
    
    @IBOutlet weak var price: DropDown!
    @IBOutlet weak var gigsCollectionView: UICollectionView!
    
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusSwitch: UISwitch!
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var sliderValue: UILabel!
    @IBOutlet weak var GigSearchBar: ModernSearchBar!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var postaRequestView: UIView!
    
    
    let locationManager = CLLocationManager()
    
    var selectedUsers = QBUUser()
    private let chatManager = ChatManager.instance
    
    var gigItems : Array<Services>?
    var searchTxt = ""
    var gigViewFlowLayout: UICollectionViewFlowLayout!
    let gigCollectionViewIdentifier = "GigsCell"
    
    var selectedIndex = Int()
    var selectedPrice = ""
    var categoriesArray: Array<Category_filters>?
    var count:Int!
    var gigsCount:Int!
    
    var cat_id = ""
    var ser_id = ""
    var status: Bool = false
    var isGroup:String!
    
    var searchString = ""
    
    var latitudeValue = ""
    var longitudeValue = ""
    var distanceValue: String = "5000"
    
    var titleArr = [searchModel]()
    
    var currentPage : Int = 1
    var isLoadingList : Bool = false
    var isPaging : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(searchTxt)
        getGigsData(pageNumber: currentPage)
        self.hideKeyboardWhenTappedAround()
        gigItems = []
        SetUpCollectionView()
        postaRequestView.isHidden = true
        GigSearchBar.delegateModernSearchBar = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        postaRequestView.isHidden = true
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {

        print(cat_id)
        getSearchBarTitles()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locationValue : CLLocationCoordinate2D = manager.location?.coordinate else {
            return
        }
        
        print("My location = \(locationValue.latitude)\("Or")\(locationValue.longitude)")
        latitudeValue = String(locationValue.latitude)
        longitudeValue = String(locationValue.longitude)
        
        locationManager.stopUpdatingLocation()
    }
    
    
    func onClickItemSuggestionsView(item: String) {
        
        print("User touched this item: "+item)
        self.searchString = item
        GigSearchBar.text = item
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Text did change, what i'm suppose to do ?")
        
        self.searchString = searchBar.text!
        getSearchBarTitles()
    }
    
    
    private func configureSearchBar(mainlist:[String]){
        
        print("Main list is \(mainlist)")
        
        self.GigSearchBar.setDatas(datas: mainlist)
    }
    
    
    func allPrice(mainlist:[String]){
        price.optionArray = mainlist
        price.didSelect{(selectedText , index ,id) in
            let val = selectedText
            let val2 = val.components(separatedBy: " ")
            let string = val2[0]
            
            self.selectedPrice = string
            self.isPaging = false
            self.gigsCount = 0
            self.count = 0
            self.currentPage = 1
            self.gigItems?.removeAll()
            self.gigsCollectionView.reloadData()
            self.getGigsData(pageNumber: self.currentPage)
        }
    }
    
    
    @IBAction func statusValueChanged(_ sender: Any) {
        
        if statusSwitch.isOn {
            statusLabel.text = "Online"
            status = true
            
            self.isPaging = false
            self.gigsCount = 0
            self.count = 0
            self.currentPage = 1
            self.gigItems?.removeAll()
            gigsCollectionView.reloadData()
            getGigsData(pageNumber: currentPage)
            
        }else{
            
            statusLabel.text = "Offline"
            status = false
            
            self.isPaging = false
            self.gigsCount = 0
            self.count = 0
            self.currentPage = 1
            self.gigItems?.removeAll()
            gigsCollectionView.reloadData()
            getGigsData(pageNumber: currentPage)
        }
    }
    
    @IBAction func sliderForDistance(_ sender: UISlider) {
        
        sliderValue.text = String(Int(sender.value))
        self.distanceValue = sliderValue.text!
        distanceSlider.isContinuous = false
        self.isPaging = false
        self.gigsCount = 0
        self.count = 0
        self.currentPage = 1
        self.gigItems?.removeAll()
        gigsCollectionView.reloadData()
        self.getGigsData(pageNumber: currentPage)
        
    }
    
    private func SetUpCollectionView(){
        
        gigsCollectionView.delegate = self
        gigsCollectionView.dataSource = self
        
        if let layout = gigsCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 15
            layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
            let width =  UIScreen.main.bounds.width
            let height = gigsCollectionView.frame.height / 4
            
            let size = CGSize(width:width - 10, height: height + 20)
            layout.itemSize = size
        }
        
        let Gignib = UINib(nibName: "GigCollectionCell", bundle: nil)
        gigsCollectionView.register(Gignib, forCellWithReuseIdentifier: gigCollectionViewIdentifier)
    }
    
    @IBAction func searchBtnTapped(_ sender: Any) {
        self.searchTxt = searchString
        self.isPaging = false
        self.gigsCount = 0
        self.count = 0
        self.currentPage = 1
        self.gigItems?.removeAll()
        gigsCollectionView.reloadData()
        getGigsData(pageNumber: currentPage)
    }
    
    @IBAction func dismissViewTapped(_ sender: Any) {
        
        postaRequestView.isHidden = true
        
    }
    
    @IBAction func postaQuickRequestTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let postVC = storyboard.instantiateViewController(withIdentifier: "QuickPostVC") as! QuickPostViewController
        postVC.value = "Gigs"
        self.navigationController?.pushViewController(postVC, animated: true)
        
    }
}

extension GigsController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if gigItems?.count ?? 0 > 0 {
            self.gigsCollectionView.restore()
            return gigItems?.count ?? 0
        } else {
            self.gigsCollectionView.setEmptyMessage("No data found.")
            return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: gigCollectionViewIdentifier, for: indexPath) as! GigCollectionCell
        
        
        if let gig = gigItems{
            
            cell.GigImage.kf.setImage(with: URL(string: gig[indexPath.item].media ?? ""), placeholder: UIImage(named: "Placeholder"))
            cell.userImage.kf.setImage(with: URL(string: gig[indexPath.item].freelancer_image ?? ""), placeholder: UIImage(named: "Placeholder"))
            cell.GigLabel.text = gig[indexPath.row].service_title
            cell.gigPrice.text = gig[indexPath.row].price
            cell.gigDescription.text = gig[indexPath.row].description
            
            cell.likeButton.isSelected = gig[indexPath.row].liked!
            
            cell.likeButton.addTarget(self, action: #selector(likedButtonTapped(_ :)), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            
            let str = gig[indexPath.item].service_rating
            let star = str?.components(separatedBy: "=>")
            let val = star?[1]
            let rating = val?.toDouble()
            cell.reviewRating.rating = rating ?? 0
            
            cell.messageBtn.addTarget(self, action: #selector(mesageButtonTapped(_ :)), for: .touchUpInside)
            cell.messageBtn.tag = indexPath.row
            
            
        }
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) &&
            !isLoadingList){
            
            self.isLoadingList = true
            isPaging = true
            self.loadMoreItemsForList()
        }
    }
    
    func loadMoreItemsForList(){
        
        print(gigsCount ?? 0)
        if gigsCount == count {
            
            print("Do Nothing")
            self.isLoadingList = false
            self.isPaging = false
        }
            
        else {
            
            currentPage += 1
            print(currentPage)
            getListFromServer(currentPage)
        }
    }
    
    func getListFromServer(_ pageNumber: Int){
        
        if gigsCount == count {
            
            print("Do Nothing")
            self.isLoadingList = false
            self.isPaging = false
        }
        else {
            getGigsData(pageNumber: pageNumber)
        }
    }
    
    @objc func likedButtonTapped(_ sender: UIButton){
        
        self.ser_id = gigItems?[sender.tag].id ?? ""
        makeFavouriteService(index: sender.tag)
    }
    
    @objc func mesageButtonTapped(_ sender: UIButton){
        
        let fullName = gigItems?[sender.tag].seller_name ?? ""
        let login = gigItems?[sender.tag].seller_email ?? ""
        
        SVProgressHUD.show()
        
        signUp(fullName: fullName, login: login)
        
    }
    
    private func signUp(fullName: String, login: String) {
        
        let user = QBUUser()
        user.login = login
        user.fullName = fullName
        user.password = LoginConstant.defaultPassword
        
        QBRequest.signUp(user, successBlock: { response, user in
            
            self.getUser(withLogin: login)
            
        }, errorBlock: { (response) in
            
            print("User Already Created")
            self.getUser(withLogin: login)
            
        })
    }
    
    private func getUser(withLogin: String){
        
        QBRequest.user(withLogin: withLogin, successBlock: { (response, user) in
            
            print("Get user is : \(user)")
            self.selectedUsers = user
            self.CreateDialogue()
            
        }, errorBlock: { (response) in
        })
    }
    
    func CreateDialogue(){
        
        if Reachability.instance.networkConnectionStatus() == .notConnection {
            showAlertView(LoginConstant.checkInternet, message: LoginConstant.checkInternetMessage)
            SVProgressHUD.dismiss()
            return
        }
        
        SVProgressHUD.show()
        
        chatManager.createPrivateDialog(withOpponent: selectedUsers, completion: { (response, dialog) in
            guard let dialog = dialog else {
                if let error = response?.error {
                    SVProgressHUD.showError(withStatus: error.error?.localizedDescription)
                }
                return
            }
            SVProgressHUD.showSuccess(withStatus: "Success".localized)
            self.openNewDialog(dialog)
        })
    }
    
    private func openNewDialog(_ newDialog: QBChatDialog) {
        
        let storyboard = UIStoryboard(name: "Chat", bundle: nil)
        let dialogsVC = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        dialogsVC.dialogID = newDialog.id
        dialogsVC.id = newDialog.userID
        dialogsVC.newDialogue = selectedUsers
        dialogsVC.selectedValue = "Gigs"
        self.navigationController?.pushViewController(dialogsVC, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        selectedIndex = indexPath.row
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let detailGigVC = storyboard.instantiateViewController(withIdentifier: "DetailGigs") as! DetailGigsViewController
        detailGigVC.selectedIndex = self.selectedIndex
        detailGigVC.gigDetails = self.gigItems
        self.navigationController?.pushViewController(detailGigVC, animated: true)
        
        
    }
}

extension GigsController{
    
    func getGigsData(pageNumber: Int) {
        
        SVProgressHUD.show()
        
        let url = "getServices"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let params = [
            "cat_id" : cat_id,
            "search": searchTxt,
            "price": selectedPrice,
            "statusfilter": status,
            "radiusrange": distanceValue,
            "latitude": latitudeValue,
            "longitude" : longitudeValue,
            "page" : pageNumber
            ] as [String : Any]
        
        print(params)
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .get, parameters: params, headers: headers).responseJSON { (response) in
            print(response.request as Any)  // original url request
            
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                
                
                self.isLoadingList = false
                
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<Gig_object>().map(JSONString: jsonStr!)
                
                self.categoriesArray = responseModel?.category_filters
                self.count = responseModel?.total_count
                
                if self.isPaging == true {

                    self.gigItems?.append(contentsOf: (responseModel?.services) ?? [])
                    self.gigsCount = self.gigItems?.count
                    print("Api gig count \(self.gigsCount ?? 0)")
                    self.isPaging = false
                }

                else {
                    
                    self.gigItems = responseModel?.services
                    self.gigsCount = self.gigItems?.count
                    print("Api gig count \(self.gigsCount ?? 0)")
                }
                
                print("Category array is \(self.categoriesArray ?? [])")
                
                var priceList = [String]()
                
                for i in 0..<(responseModel?.price_filters?.count)! where i != 0{
                    priceList.append("\(responseModel?.price_filters![i] ?? "")")
                }
                
                self.allPrice(mainlist: priceList)
                self.gigsCollectionView.reloadData()
                
                if self.gigItems == nil{
                    
                    self.postaRequestView.isHidden = false
                }
                else {
                    
                    self.postaRequestView.isHidden = true
                }
                
            }
            else if response.result.isFailure{
                print(response)
                self.isLoadingList = false
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}

extension GigsController{
    
    func makeFavouriteService(index:Int){
        
        SVProgressHUD.show()
        
        let url = "makeFavouriteService"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let params = [
            "service_id": ser_id
            ] as [String : Any]
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            var err:Error?
            
            switch response.result {
                
            case .success(let json):
                print(json)
                
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let message = dictObject["message"] as! String
                    let status = dictObject["status"] as! Int
                    
                    if message == "Success: Unliked!"{
                        self.gigItems?[index].liked = false
                    }
                    else if message == "Success: Liked!"{
                        self.gigItems?[index].liked = true
                    }
                    self.gigsCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                    
                    if status == 200 {
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                        
                    else{
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
            case .failure(let error):
                err = error
                print(err!)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
            }
        }
    }
}

extension GigsController{
    
    func getSearchBarTitles(){
        
        let url = "suggestion"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let params = [
            "keyword": searchString
            ] as [String : Any]
        
        Alamofire.request(baseUrl, method: .post,parameters: params, headers: headers).responseJSON { (response) in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
                print(json)
                
                self.titleArr.removeAll()
                
                let searchList = json["list"]
                for titles in searchList.arrayValue{
                    self.titleArr.append(searchModel(json: titles))
                }
                
                var arrayList = [String]()
                arrayList.removeAll()
                
                for i in 0..<self.titleArr.count{
                    arrayList.append(self.titleArr[i].title ?? "")
                }
                print(arrayList)
                self.configureSearchBar(mainlist: arrayList)
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
