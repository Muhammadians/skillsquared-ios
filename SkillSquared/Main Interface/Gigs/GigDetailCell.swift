//
//  GigDetailCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 12/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import FloatRatingView

class GigDetailCell: UITableViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDis: UILabel!
    @IBOutlet weak var reviewTime: UILabel!
    @IBOutlet weak var userStarReview: FloatRatingView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
