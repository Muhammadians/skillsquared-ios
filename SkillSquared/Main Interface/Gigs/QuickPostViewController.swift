//
//  QuickPostViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 07/04/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import iOSDropDown
import SVProgressHUD
import Alamofire
import SwiftyJSON
import ObjectMapper

class QuickPostViewController: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var submitRequest: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var categoryDropDown: DropDown!
    @IBOutlet weak var subCategoryDropDown: DropDown!
    @IBOutlet weak var deliveryDropDown: DropDown!
    @IBOutlet weak var subCatChildDropDown: DropDown!
    @IBOutlet weak var subCatView: UIView!
    @IBOutlet weak var subChildView: UIView!
    @IBOutlet weak var budget: UITextField!
    
    
    var value:String!
    
    var selectMainCat = ""
    var selectSubCat = ""
    var selectChildCat = ""
    var selectDeliveryTime = ""
    var catId = ""
    var postObjectArray: Array<Catoptions>?
    var subCatArray : Array<Subcat>?
    var subCatChild : Array<Subcat_child>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        subCatView.visibility = .invisible
        subCatView.visibility = .gone
        
        subChildView.visibility = .invisible
        subChildView.visibility = .gone
        
        deliverydDown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCatDetails()
    }
    
    
    @IBAction func SubmitRequestBtn(_ sender: Any) {
        
        postRequest()
    }
    
    func dropDownCat(mainlist:[String])
    {
        categoryDropDown.optionArray = mainlist
        categoryDropDown.didSelect{(selectedText , index ,id) in
            
            self.selectMainCat = selectedText
            print("\(self.selectMainCat)")
            let checkVal2 = selectedText
            
            if let mainArray = self.postObjectArray{
                for i in 0..<mainArray.count{
                    if (mainArray[i].title == checkVal2){
                        if (mainArray[i].subcat != nil) {
                            self.subCatArray = mainArray[i].subcat
                            self.subCatView.visibility = .visible
                        } else {
                            self.catId = mainArray[i].cat_id ?? ""
                            print(self.catId)
                        }
                    }
                }
            }
            var subCategory = [String]()
            if let subCategoryArray = self.subCatArray{
                for i in 0..<subCategoryArray.count{
                    subCategory.append(subCategoryArray[i].title ?? "")
                    self.catId = subCategoryArray[i].id ?? ""
                    //                    print("\(self.catId)")
                }
                self.subCatDropDown(subList: subCategory)
            }
        }
    }
    
    func subCatDropDown(subList:[String])
    {
        subCategoryDropDown.optionArray = subList
        subCategoryDropDown.didSelect{(selectedText , index ,id) in
            
            self.selectSubCat = selectedText
            print("\(self.selectSubCat)")
            let checkVal2 = selectedText
            
            if let subCategoryArray = self.subCatArray{
                for i in 0..<subCategoryArray.count{
                    if (subCategoryArray[i].title == checkVal2){
                        if (subCategoryArray[i].subcat_child != nil) {
                            self.subCatChild = subCategoryArray[i].subcat_child
                            self.subChildView.visibility = .visible
                        } else {
                            self.catId = subCategoryArray[i].id ?? ""
                            print(self.catId)
                        }
                    }
                }
            }
            var subCategoryChild = [String]()
            if let subChildArray = self.subCatChild{
                for i in 0..<subChildArray.count{
                    subCategoryChild.append(subChildArray[i].title ?? "")
                    self.catId = subChildArray[i].id ?? ""
                    //                    print("\(self.catId)")
                }
                self.subCatChildDropDown(subChildList: subCategoryChild)
            }
        }
    }
    
    func subCatChildDropDown(subChildList:[String])
    {
        subCatChildDropDown.optionArray = subChildList
        subCatChildDropDown.didSelect{(selectedText , index ,id) in
            
            //
            let checkVal2 = selectedText
            
            for i in 0..<self.subCatChild!.count{
                if (self.subCatChild![i].title == checkVal2){
                    self.catId = self.subCatChild![i].id ?? ""
                    print(self.catId)
                }
            }
            //
            
            print("\(self.selectChildCat)")
        }
    }
    
    func deliverydDown()
    {
        deliveryDropDown.optionArray = ["1 day", "2 days", "3 days", "4 days", "5 days", "6 days", "7 days", "8 days", "9 days", "10 days", "11 days", "12 days", "13 days", "14 days", "15 days", "16 days", "17 days", "18 days", "19 days", "20 days", "21 days", "22 days", "23 days", "24 days", "25 days", "26 days", "27 days", "28 days", "29 days", "30 days"]
        
        deliveryDropDown.didSelect{(selectedText , index ,id) in
            
            let str = selectedText
            let data = str.components(separatedBy: " ")
            let value = data[0]
            self.selectDeliveryTime = value
            
            print("\(self.selectDeliveryTime)")
            
            
        }
    }
}

extension QuickPostViewController {
    
    func getCatDetails() {
        
        SVProgressHUD.show()
        
        let url = "becomeafreelancer"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            print(response) // http url reponse
            if response.result.isSuccess {
                
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<MainObject>().map(JSONString: jsonStr!)
                print("\(String(describing: responseModel))")
                
                self.postObjectArray = responseModel?.catoptions
                
                var arrayList = [String]()
                
                let data = responseModel?.catoptions
                for i in 0..<data!.count{
                    arrayList.append(data?[i].title ?? "")
                }
                self.dropDownCat(mainlist: arrayList)
                
                
                
            }else if response.result.isFailure{
                print(response)
                
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}


extension QuickPostViewController: postrequest{
    
    func postRequest(){
        
        
        if descriptionTextView.text.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter description")
        }
        else if catId.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter category id")
        }
        else if deliveryDropDown.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter delivery time")
        }
        else if budget.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter budget")
        }
            
            
        else {
            
            SVProgressHUD.show()
            
            let id = catId
            let des = descriptionTextView.text
            let bud = budget.text
            let del_time = selectDeliveryTime
            
            print("\(id)\(String(describing: bud))\(del_time)")
            
            
            let headers: HTTPHeaders = [
                "accesstoken": Constants.accessToken
            ]
            
            let params = [
                "category_id": id,
                "description": des!,
                "budget": bud!,
                "delievry": del_time
                ] as [String : Any]
            
            let url = "postquickrequest"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
                var err:Error?
                
                switch response.result {
                    
                case .success(let json):
                    print(json)
                    
                    SVProgressHUD.dismiss()
                    
                    let jsonData = JSON(response.result.value!)
                    print(jsonData)
                    
                    
                    if let dictObject = jsonData.dictionaryObject {
                        print(dictObject)
                        
                        let message = dictObject["message"] as! String
                        let status = dictObject["status"] as! Int
                        
                        if status == 200 {
                            
                            let PopupController = self.storyboard?.instantiateViewController(withIdentifier: "PostRequestPopup") as! PostRequestPopup
                            PopupController.navigation = self.navigationController
                            PopupController.providesPresentationContextTransitionStyle = true
                            PopupController.definesPresentationContext = true
                            PopupController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            self.navigationController?.present(PopupController, animated: true, completion: nil)
                            PopupController.postdismiss = self
                            
                        }
                            
                        else{
                            
                            let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                            alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                    
                case .failure(let error):
                    SVProgressHUD.dismiss()
                    err = error.localizedDescription as? Error
                    print(err!)
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
                }
            }
        }
    }
    
    func didJobPosted() {
        
        if value == "Gigs" {
            
            self.navigationController?.popToRootViewController(animated: true)
        }
            
        else {
            
            self.navigationController!.popViewController(animated: true)
            
        }
    }
}
