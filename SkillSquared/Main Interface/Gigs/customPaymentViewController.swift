//
//  customPaymentViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 04/03/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import Braintree
import Alamofire
import SwiftyJSON
import SVProgressHUD

class customPaymentViewController: UIViewController, postrequest {
    
    @IBOutlet weak var paypalBtn: UIButton!
    var braintreeClient: BTAPIClient?
    
    
    var navigation : UINavigationController!
    var postdismiss : postrequest?
    var orderId:String!
    var serviceDescription:String!
    var totalamount:String!
    var deliveryTime:String!
    var Paypalnonce:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paypalBtn.visibility = .invisible
        paypalBtn.visibility = .gone
        braintreeClient = BTAPIClient(authorization: "sandbox_kttn24dq_63zmsxbxhgp8p9g9")!
        print(orderId!)
        print(totalamount!)
    }
    
    
    @IBAction func payWithPaypalTap(_ sender: Any) {
        
         SVProgressHUD.show(withStatus: "Loading...")
        
        let payPalDriver = BTPayPalDriver(apiClient: braintreeClient!)
        payPalDriver.viewControllerPresentingDelegate = self
        payPalDriver.appSwitchDelegate = self
        
        // Specify the transaction amount here. "2.32" is used in this example.
        let request = BTPayPalRequest(amount: "2.32")
        request.currencyCode = "USD" // Optional; see BTPayPalRequest.h for more options
        
        payPalDriver.requestOneTimePayment(request) { (tokenizedPayPalAccount, error) in
            if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                print("Got a nonce: \(tokenizedPayPalAccount.nonce)")
                self.Paypalnonce = tokenizedPayPalAccount.nonce
                SVProgressHUD.dismiss()
                
                self.ProceedPayment()
                //
                // Access additional information
                //                let email = tokenizedPayPalAccount.email
                //                let firstName = tokenizedPayPalAccount.firstName
                //                let lastName = tokenizedPayPalAccount.lastName
                //                let phone = tokenizedPayPalAccount.phone
                //
                //                // See BTPostalAddress.h for details
                //                let billingAddress = tokenizedPayPalAccount.billingAddress
                //                let shippingAddress = tokenizedPayPalAccount.shippingAddress
                
                
            } else if error != nil {
                print(error ?? "")
            }
                
            else {
                SVProgressHUD.dismiss()
                let message = "Payment Not Successful"
                
                let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel,handler: nil));
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
    }
    
    @IBAction func payWithPaystackTap(_ sender: Any) {
        
        
        
    }
    
    @IBAction func paywithStripe(_ sender: Any) {
        
        let stripeController = self.storyboard?.instantiateViewController(withIdentifier: "StripeController") as! StripeViewController
        stripeController.navigation = self.navigationController
        stripeController.request_id = orderId
        stripeController.price = totalamount
        stripeController.OrderType = "customOrder"
        stripeController.providesPresentationContextTransitionStyle = true
        stripeController.definesPresentationContext = true
        self.navigationController?.present(stripeController, animated: true, completion: nil)
        stripeController.dismissController = self
        
    }
    
    func didJobPosted() {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DetailGigsViewController.self) {
                _ =  self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}

extension customPaymentViewController{
    
    func ProceedPayment()  {
        
        SVProgressHUD.show()
        
        let val = self.totalamount.toDouble()! + 3.00
        
        let price = val
        let payNonce = Paypalnonce
        
        let params = ["nonce":payNonce!,
                      "amount":price,
                      "payment_method": "Paypal",
                      "customorderid":orderId!]
            
            as [String : AnyObject]
        
        print(params)
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let url = "placeOrderCustom"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON {response in
            var err:Error?
            
            switch response.result {
                
            case .success(let json):
                print(json)
                
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let status = dictObject["status"] as! Int
                    let message = dictObject["message"] as! String
                    
                    if (status == 200){
                        
                        SVProgressHUD.dismiss()
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
                            switch action.style{
                            case .default:
                                
                                for controller in self.navigationController!.viewControllers as Array {
                                    if controller.isKind(of: GigsController.self) {
                                        _ =  self.navigationController!.popToViewController(controller, animated: true)
                                        break
                                    }
                                }
                                
                            case .cancel: break
                                
                            case .destructive: break
                            @unknown default:
                                fatalError()
                            }}))
                        self.present(alertController, animated: true, completion: nil)
                        
                    } else {
                        
                        SVProgressHUD.dismiss()
                        
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
            case .failure(let error):
                err = error
                print(err!)
                SVProgressHUD.dismiss()
            }
        }
    }
}

extension customPaymentViewController: BTViewControllerPresentingDelegate {
    
    
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
    }
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
    }
}

extension customPaymentViewController: BTAppSwitchDelegate{
    
    
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
    }
}
