//
//  GigCollectionCellsCollectionViewCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 26/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import  FloatRatingView

class GigCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var GigImage: UIImageView!
    @IBOutlet weak var GigLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var gigDescription: UILabel!
    @IBOutlet weak var reviewRating: FloatRatingView!
    @IBOutlet weak var gigPrice: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var messageBtn: SAButton!
    @IBOutlet weak var callBtn: SAButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
