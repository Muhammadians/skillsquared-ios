//
//  DetailGigsViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 12/11/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Kingfisher
import ExpandableLabel
import FloatRatingView
import SVProgressHUD

class DetailGigsViewController: UIViewController {
    
    
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var reviewTableView: UITableView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descriptionHeading: UILabel!
    @IBOutlet weak var userDescription: UILabel!
    @IBOutlet weak var totalReviews: UILabel!
    @IBOutlet weak var allUserStarReviews: FloatRatingView!
    @IBOutlet weak var totalServiceRating: UILabel!
    @IBOutlet weak var freeLancerPrice: UILabel!
    
    var gigDetails : Array<Services>?
    var selectedIndex: Int = 0
    var id:String!
    var profile_user_id:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userImage.layer.masksToBounds = false
        userImage.layer.borderColor = UIColor.black.cgColor
        userImage.layer.cornerRadius = userImage.frame.height/2
        userImage.clipsToBounds = true
        
        reviewTableView.delegate = self
        reviewTableView.dataSource = self
        
        print(gigDetails ?? [])
        fillUserDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.reviewTableView.reloadData()
        print(selectedIndex)
    }
    
    func fillUserDetail()  {
        
        if let gig = gigDetails?[selectedIndex]{
            self.id = gig.id
            self.name.text = gig.freelancer_username
            
            self.headerImage.kf.setImage(with: URL(string: (gig.media)!), placeholder: UIImage(named: "Placeholder"))
            
            self.userImage.kf.setImage(with: URL(string: (gig.freelancer_image)!), placeholder: UIImage(named: "Placeholder"))
            
            self.descriptionHeading.text = gig.service_title
            self.userDescription.text = gig.description?.html2String
            self.freeLancerPrice.text = gig.price
            
            let str = gig.service_rating
            let star = str?.components(separatedBy: "=>")
            let val_1 = star?[1]
            let val_2 = star?[0]
            let rating = val_1?.toDouble()
            let avg_rating = val_2?.toDouble()
            self.allUserStarReviews.rating = rating ?? 0
            self.totalServiceRating.text = ("(\(avg_rating ?? 0))")
            
            self.profile_user_id = gig.freelancer_user_id
        }
        
        
        if let totalrev = gigDetails?[selectedIndex].reviews?.count {
            let value = "\(totalrev)" as String
            self.totalReviews.text = value
        }
    }
    
    @IBAction func customeOrderTapped(_ sender: Any) {
        
        let orderController = self.storyboard?.instantiateViewController(withIdentifier: "customOrder") as! customOrderViewController
        orderController.ser_id = id
        self.navigationController?.pushViewController(orderController, animated: true)
    }
    
    
    @IBAction func moreBtnPressed(_ sender: Any) {
        
        
        let alert = UIAlertController(title: NSLocalizedString("Report", comment: ""), message: NSLocalizedString("Are you sure you want to report this user?", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
            switch action.style{
            case .default:
                
                SVProgressHUD.show()
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    
                    SVProgressHUD.showSuccess(withStatus: "Report Successfully")
                    
                }
                
                
            case .cancel: break
                
            case .destructive: break
            @unknown default:
                fatalError()
            }}))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel",comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func profileBtnTapped(_ sender: Any) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let profileController = storyboard.instantiateViewController(withIdentifier: "SellerProfileViewController") as! SellerProfileViewController
        profileController.checkValue = "Gigs"
        profileController.userID = profile_user_id
        self.navigationController?.pushViewController(profileController, animated: true)
        
    }
}

extension DetailGigsViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gigDetails?[selectedIndex].reviews?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = reviewTableView.dequeueReusableCell(withIdentifier: "DetailGig", for: indexPath) as! GigDetailCell
        
        if let gig = gigDetails?[selectedIndex] {
            
            let name = gig.reviews?[indexPath.row].name
            let details = gig.reviews?[indexPath.row].review
            let starRating = gig.reviews?[indexPath.row].rating
            let val = starRating?.toDouble()
            
            cell.userName.text = name
            cell.userDis.text = details
            if let val = val {
                cell.userStarReview.rating = val
            }else{
                cell.userStarReview.rating = 0
            }
        }
        return cell
    }
    
}


public extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
