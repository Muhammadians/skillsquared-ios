//
//  FavouritGigController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 03/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import ObjectMapper
import Kingfisher

class FavouriteGigController: UIViewController {
    
    @IBOutlet weak var favouritGigCollectionView: UICollectionView!
    
    var favouriteItems: Array<FavouriteServices>?
    let favouriteGigIdentifier = "GigsCell"
    
     var ser_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SetUpCollectionView()
        favouriteItems = []
        getFavouritServices()
    }
    
    private func SetUpCollectionView(){
        favouritGigCollectionView.delegate = self
        favouritGigCollectionView.dataSource = self
        
        if let layout = favouritGigCollectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.minimumLineSpacing = 10
            layout.minimumInteritemSpacing = 15
            layout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
            let width =  UIScreen.main.bounds.width / 1
            let height = favouritGigCollectionView.frame.height / 4
            
            let size = CGSize(width:width - 10, height: height)
            layout.itemSize = size
        }
        
        let Gignib = UINib(nibName: "GigCollectionCell", bundle: nil)
        favouritGigCollectionView.register(Gignib, forCellWithReuseIdentifier: favouriteGigIdentifier)
    }
    
}


extension FavouriteGigController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if favouriteItems?.count ?? 0 > 0 {
            self.favouritGigCollectionView.restore()
            return favouriteItems?.count ?? 0
        } else {
            self.favouritGigCollectionView.setEmptyMessage("No data found.")
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: favouriteGigIdentifier, for: indexPath) as! GigCollectionCell
        
        if let gig = favouriteItems{
            
            cell.GigImage.kf.setImage(with: URL(string: gig[indexPath.item].service_image ?? ""), placeholder: UIImage(named: "Placeholder"))
//          cell.userImage.kf.setImage(with: URL(string: gig[indexPath.item].freelancer_image ?? ""))
            cell.GigLabel.text = gig[indexPath.row].title
            cell.gigPrice.text = gig[indexPath.row].price
            
            
            cell.likeButton.addTarget(self, action: #selector(likedButtonTapped(_ :)), for: .touchUpInside)
            cell.likeButton.tag = indexPath.row
            
            
            if gig[indexPath.row].status == "1"{
                cell.likeButton.setImage(UIImage(named: "filled-heart"), for: .normal)
            }
                
            else if gig[indexPath.row].status == "0"{
                cell.likeButton.setImage(UIImage(named: "empty-heart"), for: .normal)
            }
            
            let rating = gig[indexPath.row].rating
            let val = rating?.toDouble()
            cell.reviewRating.rating = val ?? 0
        }
        return cell
    }
    
    @objc func likedButtonTapped(_ sender: UIButton){
           
           self.ser_id = favouriteItems?[sender.tag].id ?? ""
           makeFavouriteService(index: sender.tag)
       }
    
}

extension FavouriteGigController{
    
    func getFavouritServices(){
        
        SVProgressHUD.show()
        
        let url = "getFavouriteServices"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .get, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                
                SVProgressHUD.dismiss()
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<FavouriteGigObject>().map(JSONString: jsonStr!)
                self.favouriteItems = responseModel?.favouriteServices
                
                self.favouritGigCollectionView.reloadData()
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}


extension FavouriteGigController{
    
    func makeFavouriteService(index:Int){
        
        SVProgressHUD.show()
        
        let url = "makeFavouriteService"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let params = [
            "service_id": ser_id
            ] as [String : Any]
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
            
            var err:Error?
            
            switch response.result {
                
            case .success(let json):
                print(json)
                
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let message = dictObject["message"] as! String
                    let status = dictObject["status"] as! Int
                    
                    if message == "Success: Unliked!"{
                        self.favouriteItems?[index].status = "0"
                    }
                    else if message == "Success: Liked!"{
                        self.favouriteItems?[index].status = "1"
                    }
                    self.favouritGigCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                    self.favouritGigCollectionView.reloadData()
                    
                    if status == 200 {
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                        
                    else{
                        let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
                
            case .failure(let error):
                err = error
                print(err!)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
            }
        }
    }
}
