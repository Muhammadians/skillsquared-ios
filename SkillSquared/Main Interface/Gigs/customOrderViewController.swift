//
//  customOrderViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 03/03/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import iOSDropDown
import SVProgressHUD
import Alamofire
import SwiftyJSON
import ObjectMapper

class customOrderViewController: UIViewController, postrequest {
    
    @IBOutlet weak var offerDescription: UITextView!
    @IBOutlet weak var totalAmmountOffer: UITextField!
    @IBOutlet weak var deliveryTimeDropDown: DropDown!
    
    var ser_id:String!
    var days:String!
    var orderId:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        deliveryDropDown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(ser_id!)
    }
    
    func deliveryDropDown()
    {
        deliveryTimeDropDown.optionArray = ["1 days", "2 days", "3 days", "4 days", "5 days", "6 days", "7 days", "8 days", "9 days", "10 days", "11 days", "12 days", "13 days", "14 days", "15 days", "16 days", "17 days", "18 days", "19 days", "20 days", "21 days", "22 days", "23 days", "24 days", "25 days", "26 days", "27 days", "28 days", "29 days", "30 days"]
        deliveryTimeDropDown.didSelect{(selectedText , index ,id) in
            
            let str = selectedText
            let data = str.components(separatedBy: " ")
            let value = data[0]
            self.days = value
            
            print("\(String(describing: self.days))")
            
        }
    }
    
    @IBAction func sendOrderTapped(_ sender: Any) {
        
        if offerDescription.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter description")
        }
        if totalAmmountOffer.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter ammount")
        }
        if  deliveryTimeDropDown.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter days")
        }
            
        else{
            
            SVProgressHUD.show()
            
//            let val = self.totalAmmountOffer.text!.toDouble()! + 3.00
            
            let price = self.totalAmmountOffer.text
            let id = ser_id
            let des = offerDescription.text
            let time = deliveryTimeDropDown.text
            
            
            let params = ["service_id":id!,
                          "requirements":des!,
                          "price":price!,
                          "work_duration":time!]
                
                as [String : AnyObject]
            
            let headers: HTTPHeaders = [
                "accesstoken": Constants.accessToken
            ]
            
            let url = "saveCustomOrder"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            print(params)
            
            Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
                
                print(response.request as Any)  // original url request
                print(response)  // http url reponse
                
                if response.result.isSuccess {
                    
                    SVProgressHUD.dismiss()
                    guard let jsonData = response.data else{return}
                    let jsonStr = String(data: jsonData, encoding: .utf8)
                    
                    let responseModel = Mapper<customOrderObject>().map(JSONString: jsonStr!)
                    let status = responseModel?.status
                    let message = responseModel?.message
                    
                    let id = responseModel?.orderObject?.customorderid
                    self.orderId = String(id ?? 0)
                    
                    
                    
                    if status == 200{
                        
                        let alertController = UIAlertController(title: "Title", message: (message!), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                            UIAlertAction in
                            
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let customePayment = storyboard.instantiateViewController(withIdentifier: "CustomePaymentVc") as! customPaymentViewController
                            
                            customePayment.orderId = self.orderId
                            customePayment.totalamount = self.totalAmmountOffer.text
                            self.navigationController?.pushViewController(customePayment, animated: true)
                            
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else{
                        
                        let alertController = UIAlertController(title: "Alert", message: (message!), preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                }else if response.result.isFailure{
                    print(response)
                    SVProgressHUD.dismiss()
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
                }
            }
        }
    }
    
    func didJobPosted() {
        self.navigationController!.popViewController(animated: true)
    }
    
}

