//
//  SupportController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 03/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class SupportController: UIViewController {
    
    @IBOutlet weak var subject: UITextField!
    @IBOutlet weak var supportDescription: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func submitButton(_ sender: Any) {
        submitPost()
    }
}

extension SupportController{
    
    func submitPost(){
        
        if subject.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter subject")
        }
        else if supportDescription.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter description")
        }
            
        else{
            
            SVProgressHUD.show()
            
            let url = "contactSupport"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            let headers: HTTPHeaders = [
                "accesstoken": Constants.accessToken
            ]
            
            let params = [
                "name": Constants.userName,
                "email": Constants.userEmail,
                "subject": subject.text!,
                "message": supportDescription.text!
                
            ] as [String : Any]
            
            Alamofire.request(baseUrl, method: .post, parameters: params, headers: headers).responseJSON { (response) in
                var err:Error?
                
                switch response.result {
                    
                case .success(let json):
                    print(json)
                    
                    SVProgressHUD.dismiss()

                    
                    let jsonData = JSON(response.result.value!)
                    print(jsonData)
                    
                    
                    if let dictObject = jsonData.dictionaryObject {
                        print(dictObject)
                        
                        let message = dictObject["message"] as! String
                        let status = dictObject["status"] as! Int
                        
                        if status == 200 {
                            let alertController = UIAlertController(title: "Title", message: (String(describing: message)), preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                
                                self.dismiss(animated: true) {
                                    self.navigationController?.popToRootViewController(animated: true)
                                }
                                
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                            
                        else{
                            let alertController = UIAlertController(title: "Alert", message:(String(describing: message)), preferredStyle: .alert);
                            alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                    
                case .failure(let error):
                    SVProgressHUD.dismiss()
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
                    err = error.localizedDescription as? Error
                    print(err!)
                    
                }
            }
        }
    }
}
