//
//  FeaturedGigDetailController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 31/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import ExpandableLabel
import Kingfisher
import Quickblox
import SVProgressHUD

class FeaturedGigDetailController: UIViewController {
    
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var category_Title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var serviceDescription: ExpandableLabel!
    @IBOutlet weak var contactSellerBtn: SAButton!
    
    var featuredArray : Array<Featured_services>?
    var selectedIndex: Int = 0
    var name: String!
    var email: String!
    
    var selectedUsers = QBUUser()
    private let chatManager = ChatManager.instance
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fillUserDetail()
        print(selectedIndex)
    }
    
    func fillUserDetail()  {
        
        if let data = featuredArray?[selectedIndex]{
            self.headerImage.kf.setImage(with: URL(string: (data.media)!), placeholder: UIImage(named: "Placeholder"))
            self.userImage.kf.setImage(with: URL(string: (data.user_image)!), placeholder: UIImage(named: "Placeholder"))
            self.userName.text = data.service_username
            self.serviceTitle.text = data.service_title
            self.category_Title.text = data.category_name
            self.serviceDescription.text = data.description?.html2String
            self.price.text = data.price
            self.name = data.name
            self.email = data.email
        }
    }
    
    
    func signUp(fullName: String, login: String) {
               
               let user = QBUUser()
               user.login = login
               user.fullName = fullName
               user.password = LoginConstant.defaultPassword
               
               QBRequest.signUp(user, successBlock: { response, user in
                   
                self.getUser(withLogin: login)
                   
               }, errorBlock: { (response) in
                   
                   print("User Already Created")
                   self.getUser(withLogin: login)
                   
               })
           }
           
           func getUser(withLogin: String){
               
               QBRequest.user(withLogin: withLogin, successBlock: { (response, user) in
                   
                   print("Get user is : \(user)")
                   self.selectedUsers = user
                   self.CreateDialogue()
                   
               }, errorBlock: { (response) in
               })
           }
           
           func CreateDialogue(){
               
               if Reachability.instance.networkConnectionStatus() == .notConnection {
                   showAlertView(LoginConstant.checkInternet, message: LoginConstant.checkInternetMessage)
                   SVProgressHUD.dismiss()
                   return
               }
               
               SVProgressHUD.show()
               
               chatManager.createPrivateDialog(withOpponent: selectedUsers, completion: { (response, dialog) in
                   guard let dialog = dialog else {
                       if let error = response?.error {
                           SVProgressHUD.showError(withStatus: error.error?.localizedDescription)
                       }
                       return
                   }
                   SVProgressHUD.showSuccess(withStatus: "Success".localized)
                   self.openNewDialog(dialog)
               })
           }
           
           func openNewDialog(_ newDialog: QBChatDialog) {
               
               let storyboard = UIStoryboard(name: "Chat", bundle: nil)
               let dialogsVC = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
               dialogsVC.dialogID = newDialog.id
               dialogsVC.id = newDialog.userID
               dialogsVC.newDialogue = selectedUsers
               dialogsVC.selectedValue = "Gigs"
               self.navigationController?.pushViewController(dialogsVC, animated: true)
               
           }
    
    
    @IBAction func contactBtn(_ sender: Any) {
        
            self.signUp(fullName: name , login: email)
        
    }
}
