//
//  Home.swift
//  SkillSquared
//
//  Created by Awais Aslam on 23/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import SVProgressHUD
import SideMenu
import Firebase
import Quickblox
import UserNotifications
import SVProgressHUD
import QuickbloxWebRTC
import ObjectMapper


class HomeViewController: UIViewController, ModernSearchBarDelegate {
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var featuredCollectionView: UICollectionView!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var bannerPageControl: UIPageControl!
    @IBOutlet weak var homeSearchBar: ModernSearchBar!
    @IBOutlet weak var searchBtn: UIButton!
    
    
    lazy private var dataSourceCall: UsersDataSource = {
        let dataSource = UsersDataSource()
        return dataSource
    }()
    
    
    lazy private var navViewController: UINavigationController = {
        let navViewController = UINavigationController()
        return navViewController
        
    }()
    
    private func hasConnectivity() -> Bool {
        let status = Reachability.instance.networkConnectionStatus()
        guard status != NetworkConnectionStatus.notConnection else {
            showAlertView("Alert", message: UsersAlertConstant.checkInternet)
            if CallKitManager.instance.isCallStarted() == false {
                CallKitManager.instance.endCall(with: callUUID) {
                    debugPrint("[UsersViewController] endCall func hasConnectivity")
                }
            }
            return false
        }
        return true
    }
    
    private var answerTimer: Timer?
    private var sessionID: String?
    private var isUpdatedPayload = true
    private weak var session: QBRTCSession?
    private var callUUID: UUID?
    
    
    var categoriesItems: Array<Categories>?
    var featuredItems: Array<Featured_services>?
    var filterFeaturedItems: Array<Featured_services>?
    var titleArr = [searchModel]()
    var bannerImages: [String]? = []
    var qbBadgeCount : [Int] = []
    
    var searching = false
    
    var API_KEY:String!
    let APP_ID = "15914374135d964"
    
    var categoryFlowLayout: UICollectionViewFlowLayout!
    var featuredFlowLayout: UICollectionViewFlowLayout!
    var bannerFlowLayout: UICollectionViewFlowLayout!
    
    let categoryIdentifier = "PopularService"
    let featuredIdentifier = "FeaturedGig"
    
    var catId = ""
    var selectedIndex = Int()
    var selectedIndexService = Int()
    var searchString = ""
    
    var bannerSlider = 0
    var timer : Timer?
    
    var value: String!
    var dialogId: String!
    var dialogName: String!
    var qbMessageCounter : Int = 0
    
    var selectedText: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        QBRTCClient.instance().add(self)
        self.hideKeyboardWhenTappedAround()
        homeSearchBar.delegateModernSearchBar = self
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "notification"), object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        print(" Data is \(Constants.notificationConstant)")
        
        perform(#selector(showNotificationScreen), with: nil, afterDelay: 0.4)
        
        if AppConstants.sharedInstance.userDefaults.bool(forKey: AppConstants.sharedInstance.notificationClick) == true
        {
            //Land on SecondViewController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OrdersViewController") as! OrdersViewController
            self.navigationController?.pushViewController(vc, animated: true)
            AppConstants.sharedInstance.userDefaults.set(false, forKey: AppConstants.sharedInstance.notificationClick)
        }
        
        categoriesItems = []
        featuredItems = []
        bannerImages = []
        
        setupHomeView1()
        setupHomeView2()
        
        getHomeData()
        //startTimer()
        
    }
    
    @objc func showNotificationScreen(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("notification"), object: nil)
        
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        guard let text = notification.userInfo?["text"] as? String else { return }
        print ("text is : \(text)")
        Constants.notificationConstant = text
        
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let chatController = storyboard.instantiateViewController(withIdentifier: "OrdersViewController") as! OrdersViewController
            self.navigationController?.pushViewController(chatController, animated: true)
            
        }
    }
    
    func onClickItemSuggestionsView(item: String) {
        
        print("User touched this item: "+item)
        self.selectedText = item
        homeSearchBar.text = item
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Text did change, what i'm suppose to do ?")
        
        self.searchString = searchBar.text!
        getSearchBarTitles()
    }
    
    
    private func configureSearchBar(mainlist:[String]){
        
        print("Main list is \(mainlist)")
        
        self.homeSearchBar.setDatas(datas: mainlist)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupHomeView1Size()
        setupHomeView2Size()
        bannerCollectionViewSize()
    }
    
    
    private func setupHomeView1(){
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        let nib = UINib(nibName: "PopularServices", bundle: nil)
        categoryCollectionView.register(nib, forCellWithReuseIdentifier: categoryIdentifier)
        featuredCollectionView.register(nib, forCellWithReuseIdentifier: featuredIdentifier)
    }
    
    private func setupHomeView2(){
        featuredCollectionView.delegate = self
        featuredCollectionView.dataSource = self
        let nib2 = UINib(nibName: "FeaturedGigs", bundle: nil)
        featuredCollectionView.register(nib2, forCellWithReuseIdentifier: featuredIdentifier)
    }
    
    private func setupHomeView1Size() {
        
        if categoryFlowLayout == nil {
            
            let lineSpacing: CGFloat = 5
            let interItemSpacing: CGFloat = 5
            let width = UIScreen.main.bounds.width / 2.2
            let height = categoryCollectionView.frame.height
            
            categoryFlowLayout = UICollectionViewFlowLayout()
            categoryFlowLayout.itemSize = CGSize(width: width - 10, height: height)
            categoryFlowLayout.sectionInset = UIEdgeInsets.zero
            categoryFlowLayout.scrollDirection = .horizontal
            categoryFlowLayout.minimumLineSpacing = lineSpacing
            categoryFlowLayout.minimumInteritemSpacing = interItemSpacing
            categoryFlowLayout.invalidateLayout()
            categoryCollectionView.setCollectionViewLayout(categoryFlowLayout, animated: true)
        }
    }
    
    private func setupHomeView2Size() {
        
        if featuredFlowLayout == nil {
            
            
            let lineSpacing: CGFloat = 5
            let interItemSpacing: CGFloat = 5
            let width = UIScreen.main.bounds.width / 2.5
            let height = featuredCollectionView.frame.height
            
            
            featuredFlowLayout = UICollectionViewFlowLayout()
            featuredFlowLayout.itemSize = CGSize(width: width, height: height)
            featuredFlowLayout.sectionInset = UIEdgeInsets.zero
            featuredFlowLayout.scrollDirection = .horizontal
            featuredFlowLayout.minimumLineSpacing = lineSpacing
            featuredFlowLayout.minimumInteritemSpacing = interItemSpacing
            featuredFlowLayout.invalidateLayout()
            featuredCollectionView.setCollectionViewLayout(featuredFlowLayout, animated: true)
        }
    }
    
    private func bannerCollectionViewSize() {
        
        if bannerFlowLayout == nil {
            
            
            let lineSpacing: CGFloat = 0
            let interItemSpacing: CGFloat = 0
            let width = UIScreen.main.bounds.width
            let height = UIScreen.main.bounds.height
            
            
            bannerFlowLayout = UICollectionViewFlowLayout()
            bannerFlowLayout.itemSize = CGSize(width: width, height: height)
            bannerFlowLayout.sectionInset = UIEdgeInsets.zero
            bannerFlowLayout.scrollDirection = .horizontal
            bannerFlowLayout.minimumLineSpacing = lineSpacing
            bannerFlowLayout.minimumInteritemSpacing = interItemSpacing
            bannerCollectionView.setCollectionViewLayout(bannerFlowLayout, animated: true)
        }
    }
    
    func startTimer(){
        
        bannerCollectionView.reloadData()
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction(){
        
        let desiredScrollPosition = (bannerSlider < bannerImages!.count - 1) ? bannerSlider + 1 : 0
        bannerCollectionView.scrollToItem(at: IndexPath(item: desiredScrollPosition, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //
    //        bannerSlider = Int(scrollView.contentOffset.x / bannerCollectionView.frame.size.width)
    //        bannerPageControl.currentPage = bannerSlider
    //    }
    
    @IBAction func goToGigController(_ sender: Any) {
        
        let txt = selectedText
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let gigVC = storyboard.instantiateViewController(withIdentifier: "GigVC") as! GigsController
        gigVC.searchTxt = txt ?? ""
        self.navigationController?.pushViewController(gigVC, animated: true)
    }
}

//extension HomeViewController: UISearchBarDelegate{
//
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//
//        self.searchString = searchBar.text!
//    }
//}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == categoryCollectionView{
            if categoriesItems?.count ?? 0 > 0{
                self.categoryCollectionView.restore()
                return categoriesItems!.count
            }
            else {
                self.categoryCollectionView.setEmptyMessage("No Data Found.")
                return 0
            }
        }
            
        else if collectionView == featuredCollectionView{
            if featuredItems?.count ?? 0 > 0{
                self.featuredCollectionView.restore()
                return featuredItems!.count
                
            }else {
                self.featuredCollectionView.setEmptyMessage("No Data Found.")
                return 0
            }
            
        }else{
            if bannerImages!.count > 0 {
                self.bannerCollectionView.restore()
                //              bannerPageControl.numberOfPages = bannerImages!.count
                return bannerImages!.count
            }else {
                self.bannerCollectionView.setEmptyMessage("No Data Found.")
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == categoryCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: categoryIdentifier, for: indexPath) as! PopularServices
            
            if let details = categoriesItems{
                cell.ServicesHeading.text = details[indexPath.item].title
                cell.ServicesImage.kf.setImage(with: URL(string: details[indexPath.item].page_banner!))
            }
            return cell
        }
            
        else if collectionView == featuredCollectionView{
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: featuredIdentifier, for: indexPath) as! FeaturedGigs
            
            if let details = featuredItems{
                cellA.FeaturedLabel.text = details[indexPath.item].category_name ?? ""
                cellA.FeaturedImage.kf.setImage(with: URL(string: details[indexPath.item].media!))
            }
            
            return cellA
        }
            
        else{
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "bannerSliderCell", for: indexPath) as! BannerCell
            cellB.bannerImage.kf.setImage(with: URL(string: bannerImages![indexPath.item]))
            return cellB
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == categoryCollectionView{
            
            self.catId = categoriesItems![selectedIndex].cat_id!
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil);
            let categoryVC = storyboard.instantiateViewController(withIdentifier: "categoriesController") as! CategoriesController
            categoryVC.catId = self.catId
            self.navigationController?.pushViewController(categoryVC, animated: true)
        }
            
        else if collectionView == featuredCollectionView{
            
            self.selectedIndexService = indexPath.row
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil);
            let featuredVC = storyboard.instantiateViewController(withIdentifier: "FeaturedController") as! FeaturedGigDetailController
            featuredVC.selectedIndex = self.selectedIndexService
            featuredVC.featuredArray = self.featuredItems
            self.navigationController?.pushViewController(featuredVC, animated: true)
        }
    }
}

extension HomeViewController{
    
    func getHomeData() {
        
        SVProgressHUD.show()
        
        
        let url = "homeData"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .get, headers: headers).responseJSON { (response) in
            
            print(response)  // http url reponse
            if response.result.isSuccess {
                
                SVProgressHUD.dismiss()
                
                
                guard let jsonData = response.data else{return}
                
                do{
                    
                    let responseObject = try JSONDecoder().decode(HomeModalObject.self, from: jsonData)
                    
                    let catData = responseObject.categories
                    let featuredData = responseObject.featured_services
                    //                    let data = responseObject.banners
                    
                    var imageList = [String]()
                    
                    let data = responseObject.banners ?? []
                    for i in 0..<data.count{
                        imageList.append(data[i])
                    }
                    
                    self.bannerImages = imageList
                    print(self.bannerImages!)
                    
                    self.categoriesItems = catData
                    self.featuredItems = featuredData
                    
                    self.featuredCollectionView.reloadData()
                    self.categoryCollectionView.reloadData()
                    self.bannerCollectionView.reloadData()
                    
                    self.getQBDialogs()
                }
                    
                catch{
                    print(error.localizedDescription)
                }
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}

extension HomeViewController{
    
    
    func getSearchBarTitles(){
        
        let url = "suggestion"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let params = [
            "keyword": searchString
            ] as [String : Any]
        
        
        
        Alamofire.request(baseUrl, method: .post,parameters: params, headers: headers).responseJSON { (response) in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
                print(json)
                
                self.titleArr.removeAll()
                
                let searchList = json["list"]
                for titles in searchList.arrayValue{
                    self.titleArr.append(searchModel(json: titles))
                }
                
                var arrayList = [String]()
                arrayList.removeAll()
                
                for i in 0..<self.titleArr.count{
                    arrayList.append(self.titleArr[i].title ?? "")
                }
                print(arrayList)
                self.configureSearchBar(mainlist: arrayList)
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension HomeViewController {
    
    func getQBDialogs(){
        
        QBRequest.dialogs(for: QBResponsePage(limit: 100), extendedRequest: nil, successBlock: { (response, dialogs, dialogsUsersIDs, page) in
            print("Response is " , response)
            print("dialogs is " , dialogs)
            print("dialogsUsersIDs is " , dialogsUsersIDs)
            
            quickBloxUnReadCount = 0
            self.qbMessageCounter = 0
            
            for i in 0..<dialogs.count{
                let counter = dialogs[i].unreadMessagesCount
                print(counter)
                self.qbMessageCounter = self.qbMessageCounter + Int(counter)
            }
            quickBloxUnReadCount = self.qbMessageCounter
            print(quickBloxUnReadCount)
        }, errorBlock: { (response) in
            print(response)
        })
    }
}


extension HomeViewController: QBRTCClientDelegate {
    
    func session(_ session: QBRTCSession, hungUpByUser userID: NSNumber, userInfo: [String : String]? = nil) {
        if CallKitManager.instance.isCallStarted() == false,
            let sessionID = self.sessionID,
            sessionID == session.id,
            session.initiatorID == userID || isUpdatedPayload == false {
            CallKitManager.instance.endCall(with: callUUID)
            prepareCloseCall()
        }
    }
    
    func didReceiveNewSession(_ session: QBRTCSession, userInfo: [String : String]? = nil) {
        if self.session != nil {
            session.rejectCall(["reject": "busy"])
            return
        }
        invalidateAnswerTimer()
        
        self.session = session
        
        if let currentCall = CallKitManager.instance.currentCall() {
            //open by VOIP Push
            
            CallKitManager.instance.setupSession(session)
            if currentCall.status == .ended {
                CallKitManager.instance.setupSession(session)
                CallKitManager.instance.endCall(with: currentCall.uuid)
                session.rejectCall(["reject": "busy"])
                prepareCloseCall()
            } else {
                var opponentIDs = [session.initiatorID]
                let profile = Profile()
                guard profile.isFull == true else {
                    return
                }
                for userID in session.opponentsIDs {
                    if userID.uintValue != profile.ID {
                        opponentIDs.append(userID)
                    }
                }
                
                prepareCallerNameForOpponentIDs(opponentIDs) { (callerName) in
                    CallKitManager.instance.updateIncomingCall(withUserIDs: session.opponentsIDs,
                                                               outCallerName: callerName,
                                                               session: session,
                                                               uuid: currentCall.uuid)
                }
            }
        } else {
            //open by call
            
            if let uuid = UUID(uuidString: session.id) {
                callUUID = uuid
                sessionID = session.id
                
                var opponentIDs = [session.initiatorID]
                let profile = Profile()
                guard profile.isFull == true else {
                    return
                }
                for userID in session.opponentsIDs {
                    if userID.uintValue != profile.ID {
                        opponentIDs.append(userID)
                    }
                }
                
                prepareCallerNameForOpponentIDs(opponentIDs) { [weak self] (callerName) in
                    self?.reportIncomingCall(withUserIDs: opponentIDs,
                                             outCallerName: callerName,
                                             session: session,
                                             uuid: uuid)
                }
            }
        }
    }
    
    private func prepareCallerNameForOpponentIDs(_ opponentIDs: [NSNumber], completion: @escaping (String) -> Void)  {
        var callerName = ""
        var opponentNames = [String]()
        var newUsers = [String]()
        for userID in opponentIDs {
            
            // Getting recipient from users.
            if let user = dataSourceCall.user(withID: userID.uintValue),
                let fullName = user.fullName {
                opponentNames.append(fullName)
            } else {
                newUsers.append(userID.stringValue)
            }
        }
        
        if newUsers.isEmpty == false {
            
            QBRequest.users(withIDs: newUsers, page: nil, successBlock: { [weak self] (respose, page, users) in
                if users.isEmpty == false {
                    self?.dataSourceCall.update(users: users)
                    for user in users {
                        opponentNames.append(user.fullName ?? user.login ?? "")
                    }
                    callerName = opponentNames.joined(separator: ", ")
                    completion(callerName)
                }
            }) { (respose) in
                for userID in newUsers {
                    opponentNames.append(userID)
                }
                callerName = opponentNames.joined(separator: ", ")
                completion(callerName)
            }
        } else {
            callerName = opponentNames.joined(separator: ", ")
            completion(callerName)
        }
    }
    
    private func reportIncomingCall(withUserIDs userIDs: [NSNumber], outCallerName: String, session: QBRTCSession, uuid: UUID) {
        if hasConnectivity() {
            CallKitManager.instance.reportIncomingCall(withUserIDs: userIDs,
                                                       outCallerName: outCallerName,
                                                       session: session,
                                                       sessionID: session.id,
                                                       sessionConferenceType: session.conferenceType,
                                                       uuid: uuid,
                                                       onAcceptAction: { [weak self] (isAccept) in
                                                        guard let self = self else {
                                                            return
                                                        }
                                                        if isAccept == true {
                                                            self.openCall(withSession: session, uuid: uuid, sessionConferenceType: session.conferenceType)
                                                        } else {
                                                            debugPrint("[UsersViewController] endCall reportIncomingCall")
                                                        }
                                                        
                }, completion: { (isOpen) in
                    debugPrint("[UsersViewController] callKit did presented")
            })
        } else {
            
        }
    }
    
    private func openCall(withSession session: QBRTCSession?, uuid: UUID, sessionConferenceType: QBRTCConferenceType) {
        if hasConnectivity() {
            
            let storyboard = UIStoryboard(name: "Call", bundle: nil)
            if let callViewController = storyboard.instantiateViewController(withIdentifier: UsersSegueConstant.call) as? CallViewController {
                if let qbSession = session {
                    callViewController.session = qbSession
                }
                callViewController.usersDataSource = self.dataSourceCall
                callViewController.callUUID = uuid
                callViewController.sessionConferenceType = sessionConferenceType
                self.navViewController = UINavigationController(rootViewController: callViewController)
                self.navViewController.modalTransitionStyle = .crossDissolve
                self.present(self.navViewController , animated: false)
            } else {
                return
            }
        } else {
            return
        }
    }
    
    func sessionDidClose(_ session: QBRTCSession) {
        if let sessionID = self.session?.id,
            sessionID == session.id {
            if let endedCall = CallKitManager.instance.currentCall() {
                CallKitManager.instance.endCall(with: endedCall.uuid) {
                    debugPrint("[UsersViewController] endCall sessionDidClose")
                }
            }
            prepareCloseCall()
        }
    }
    
    private func prepareCloseCall() {
        if self.navViewController.presentingViewController?.presentedViewController == self.navViewController {
            self.navViewController.view.isUserInteractionEnabled = false
            self.navViewController.dismiss(animated: false)
        }
        self.callUUID = nil
        self.session = nil
        self.sessionID = nil
        if QBChat.instance.isConnected == false {
            self.connectToChat()
        }
    }
    
    private func connectToChat(success:QBChatCompletionBlock? = nil) {
        let profile = Profile()
        guard profile.isFull == true else {
            return
        }
        
        QBChat.instance.connect(withUserID: profile.ID,
                                password: LoginConstant.defaultPassword,
                                completion: { [weak self] error in
                                    guard let self = self else { return }
                                    if let error = error {
                                        if error._code == QBResponseStatusCode.unAuthorized.rawValue {
                                            
                                        } else {
                                            debugPrint("[UsersViewController] login error response:\n \(error.localizedDescription)")
                                        }
                                        success?(error)
                                    } else {
                                        success?(nil)
                                        //did Login action
                                        SVProgressHUD.dismiss()
                                    }
        })
    }
    
    
    private func invalidateAnswerTimer() {
        if self.answerTimer != nil {
            self.answerTimer?.invalidate()
            self.answerTimer = nil
        }
    }
}
