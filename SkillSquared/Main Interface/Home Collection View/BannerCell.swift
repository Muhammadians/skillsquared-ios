//
//  BannerCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 09/12/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class BannerCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImage: UIImageView!
    
}


