//
//  FeaturedGigs.swift
//  SkillSquared
//
//  Created by Awais Aslam on 24/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class FeaturedGigs: UICollectionViewCell {
    
    @IBOutlet weak var FeaturedImage: UIImageView!
    @IBOutlet weak var FeaturedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
