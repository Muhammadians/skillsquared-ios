//
//  PopularServices.swift
//  SkillSquared
//
//  Created by Awais Aslam on 23/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class PopularServices: UICollectionViewCell {

    @IBOutlet var ServicesImage: UIImageView!
    @IBOutlet var ServicesHeading: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
