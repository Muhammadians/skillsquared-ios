//
//  MenuController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 30/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Kingfisher
import Firebase
import Quickblox
import SVProgressHUD
import Alamofire
import SwiftyJSON

class MenuController: UITableViewController{
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var buyerSellerCell: UITableViewCell!
    @IBOutlet weak var buyerCell: UITableViewCell!
    @IBOutlet weak var notificationCounter: UILabel!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var messageCounterView: UIView!
    @IBOutlet weak var messageCounter: UILabel!
    
    
    var index: IndexPath?
    private let chatManager = ChatManager.instance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print(Constants.badgeValue)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        setMenu()
        self.setUpLabelTapped()
        
        notificationView.isHidden = true
        messageCounterView.isHidden = true
       
        if quickBloxUnReadCount > 0 {
            messageCounterView.isHidden = false
            let num = "\(quickBloxUnReadCount)"
            messageCounter.text = num
        }else if quickBloxUnReadCount == 0{
            messageCounterView.isHidden = true
        }
        
        if Constants.badgeValue != 0 {
            notificationView.isHidden = false
            let val = String(Constants.badgeValue)
            notificationCounter.text = val
        }
        else {
            notificationView.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func setMenu() {
        
        self.userImage.kf.setImage(with: URL(string: Constants.userImage), placeholder: UIImage(named: "User"))
        self.userName.text = Constants.userName
        
    }
    
    func setUpLabelTapped(){
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.userNameTapped(_ :)))
        self.userName.isUserInteractionEnabled = true
        self.userName.addGestureRecognizer(labelTap)
    }
    
    @objc func userNameTapped(_ sender: UITapGestureRecognizer) {
        
        if Constants.freeLancer == true {
            
            performSegue(withIdentifier: "GoToProfile", sender: self)
        }
        
        else{
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil);
            let HomeController = storyboard.instantiateViewController(withIdentifier: "profileController") as! EditProfileViewController
            self.navigationController?.pushViewController(HomeController, animated: true)
            
        }
    }
    
    @IBAction func buyerFreelancerTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let HomeController = storyboard.instantiateViewController(withIdentifier: "BackToWhatYouDo") as! WhatYouDo
        self.navigationController?.pushViewController(HomeController, animated: true)
        
    }
    
    @IBAction func buyerTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let HomeController = storyboard.instantiateViewController(withIdentifier: "BackToWhatYouDo") as! WhatYouDo
        self.navigationController?.pushViewController(HomeController, animated: true)
        
    }
    
    @IBAction func sellerTapped(_ sender: Any) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let HomeController = storyboard.instantiateViewController(withIdentifier: "sellerScreens") as! AllSallerScreensViewController
        self.navigationController?.pushViewController(HomeController, animated: true)
        
    }
    
    
    @IBAction func becomeAFreelancerTapped(_ sender: Any) {
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let HomeController = storyboard.instantiateViewController(withIdentifier: "FreelancerForm") as! FreeLancerForm1VC
        self.navigationController?.pushViewController(HomeController, animated: true)
        
    }
    
    @IBAction func goToNotifications(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let HomeController = storyboard.instantiateViewController(withIdentifier: "NotificationController") as! NotificationViewController
        self.navigationController?.pushViewController(HomeController, animated: true)
        
    }
    
    
    
    @IBAction func logOutBtn(_ sender: Any) {
        
        let alert = UIAlertController(title: NSLocalizedString("Logout", comment: ""), message: NSLocalizedString("Are you sure you want to Logout?", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
            switch action.style{
            case .default:
                
                
                self.didTapLogout()
                
            case .cancel: break
                
            case .destructive: break
            @unknown default:
                fatalError()
            }}))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel",comment: ""), style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func didTapLogout() {
        
        if QBChat.instance.isConnected == false {
            SVProgressHUD.showError(withStatus: "Error please check your connection")
            return
        }
        
        SVProgressHUD.show(withStatus: "Logging out".localized)
        SVProgressHUD.setDefaultMaskType(.clear)
        
        guard let identifierForVendor = UIDevice.current.identifierForVendor else {
            return
        }
        let uuidString = identifierForVendor.uuidString
        #if targetEnvironment(simulator)
        disconnectUser()
        #else
        QBRequest.subscriptions(successBlock: { (response, subscriptions) in
            if let subscriptions = subscriptions {
                for subscription in subscriptions {
                    if let subscriptionsUIUD = subscriptions.first?.deviceUDID,
                        subscriptionsUIUD == uuidString,
                        subscription.notificationChannel == .APNSVOIP {
                        self.unregisterSubscription(forUniqueDeviceIdentifier: uuidString)
                        return
                    }
                }
            }
            self.disconnectUser()
            
        }) { response in
            if response.status.rawValue == 404 {
                self.disconnectUser()
            }
        }
        #endif
    }
    
    private func unregisterSubscription(forUniqueDeviceIdentifier uuidString: String) {
        QBRequest.unregisterSubscription(forUniqueDeviceIdentifier: uuidString, successBlock: { response in
            self.disconnectUser()
            print("unsubscribe")
        }, errorBlock: { error in
            if let error = error.error {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                return
            }
            SVProgressHUD.dismiss()
        })
    }
    
    //MARK: - Internal Methods
    func hasConnectivity() -> Bool {
        
        let status = Reachability.instance.networkConnectionStatus()
        guard status != NetworkConnectionStatus.notConnection else {
            showAlertView(LoginConstant.checkInternet, message: LoginConstant.checkInternetMessage)
            return false
        }
        return true
    }
    
    private func disconnectUser() {
        QBChat.instance.disconnect(completionBlock: { error in
            if let error = error {
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                return
            }
            self.logOut()
        })
    }
    
    private func logOut() {
        QBRequest.logOut(successBlock: { [weak self] response in
            //ClearProfile
            Profile.clearProfile()
            self?.chatManager.storage.clear()
            CacheManager.shared.clearCache()
        
            DispatchQueue.main.async {
                
                self?.LogoutTapped()
            }
            
            SVProgressHUD.showSuccess(withStatus: "SA_STR_COMPLETED".localized)
            app.loginWindow()
            
            
        }) { response in
            debugPrint("[DialogsViewController] logOut error: \(response)")
        }
    }
    
    
    @IBAction func homeTapped(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        //        performSegue(withIdentifier: "GoToHome", sender: self)
    }
    
    @IBAction func inboxTap(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Dialogs", bundle: nil)
        let dialogsVC = storyboard.instantiateViewController(withIdentifier: "DialogsViewController") as! DialogsViewController
        self.navigationController?.pushViewController(dialogsVC, animated: true)
        
    }
    
    @IBAction func savedTap(_ sender: Any) {
        
        performSegue(withIdentifier: "GoToSaved", sender: self)
    }
    
    @IBAction func settingsTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "GoToSettings", sender: self)
        
    }
    
    @IBAction func supportTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "GoToSupport", sender: self)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let user: Bool = false
        
        if user == Constants.freeLancer {
            
            if indexPath.row == 5 {
                return 0
            }
            
        }else{
            if indexPath.row == 6 {
                return 0
            }
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
}

extension MenuController {
    
    func LogoutTapped(){
        
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let url = "logout"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            var err:Error?
            
            switch response.result {
                
            case .success(let json):
                print(json)
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                UserDefaults.standard.removeObject(forKey: "userEmail")
                UserDefaults.standard.removeObject(forKey: "accessToken")
                UserDefaults.standard.removeObject(forKey: "userName")
                UserDefaults.standard.removeObject(forKey: "address")
                UserDefaults.standard.removeObject(forKey: "phoneNumber")
                UserDefaults.standard.removeObject(forKey: "freeLancer")
                UserDefaults.standard.removeObject(forKey: "userImage")
                UserDefaults.standard.removeObject(forKey: "userID")
                
            case .failure(let error):
                err = error.localizedDescription as? Error
                print(err!)
                
            }
        }
    }
}

