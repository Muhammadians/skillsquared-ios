//
//  AllUserChatCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 25/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class AllUserChatCell: UITableViewCell {
    
    @IBOutlet weak var UserProfile: UIImageView!
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var MessageTime: UILabel!
    @IBOutlet weak var Message: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
