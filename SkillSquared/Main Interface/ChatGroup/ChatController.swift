//
//  ChatController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 24/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import WMSegmentControl

struct UserChatItems {
    var UserImage: String
    var UserName:  String
    var Message:   String
    var MessageTime: String
}



class ChatController: UIViewController {
    @IBOutlet weak var ChatSegment: WMSegment!
    @IBOutlet weak var ChatView: UIView!
    @IBOutlet weak var CallView: UIView!
    
    
    
    var ChatItems: [UserChatItems] = [UserChatItems(UserImage: "User", UserName: "Awais", Message: "Hey Your New Slides is Here", MessageTime: "1 min ago"),UserChatItems(UserImage: "User", UserName: "Talha", Message: "Hey Your New Slides is Here", MessageTime: "2 min ago"),UserChatItems(UserImage: "User", UserName: "Aslam", Message: "Hey Your New Slides is Here", MessageTime: "3 min ago"),UserChatItems(UserImage: "User", UserName: "Awais", Message: "Hey Your New Slides is Here", MessageTime: "4 min ago"),UserChatItems(UserImage: "User", UserName: "Talha", Message: "Hey Your New Slides is Here", MessageTime: "5 min ago"),UserChatItems(UserImage: "User", UserName: "Aslam", Message: "Hey Your New Slides is Here", MessageTime: "6 min ago"),UserChatItems(UserImage: "User", UserName: "Awais", Message: "Hey Your New Slides is Here", MessageTime: "7 min ago")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ChatSegment.selectorType = .bottomBar
        CallView.isHidden = true
    }
    
    
    @IBAction func SegmentViewChange(_ sender: WMSegment) {
        switch sender.selectedSegmentIndex {
                case 0:
                print("first item")
                view.addSubview(ChatView)
                CallView.isHidden = true
                ChatView.isHidden = false
        
                case 1:
                print("second item")
                view.addSubview(CallView)
                CallView.isHidden = false
                ChatView.isHidden = true
            
                default:
                break
        }
    }
}

extension ChatController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ChatItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllUserChats", for: indexPath) as! AllUserChatCell
        cell.UserProfile.image = UIImage(named: ChatItems[indexPath.item].UserImage)
        cell.UserName.text = ChatItems[indexPath.item].UserName
        cell.Message.text = ChatItems[indexPath.item].Message
        cell.MessageTime.text = ChatItems[indexPath.item].MessageTime
        return cell
    }
}
