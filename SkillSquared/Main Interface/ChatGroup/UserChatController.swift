//
//  UserChatController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 25/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class UserChatController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
}
