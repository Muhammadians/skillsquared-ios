//
//  TermOfServicesVC.swift
//  SkillSquared
//
//  Created by Awais Aslam on 09/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class TermOfServicesVC: UIViewController {
    
    @IBOutlet weak var termOfServicesTxt: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GetText()
    }
    
    func GetText(){
        
        SVProgressHUD.show()
        
        let url = "terms"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        Alamofire.request(baseUrl, method: .get,encoding: URLEncoding.default).responseJSON {response in
            
            var err:Error?
            
            switch response.result {
                
            case .success(let json):
                print(json)
                SVProgressHUD.dismiss()
                
                let jsonData = JSON(response.result.value!)
                print(jsonData)
                
                
                if let dictObject = jsonData.dictionaryObject {
                    print(dictObject)
                    
                    let txt = dictObject["data"] as! String
                    self.termOfServicesTxt.text = txt.html2String
                }
                
            case .failure(let error):
                err = error
                print(err!)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
            }
        }
    }
}
