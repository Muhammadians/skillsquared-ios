//
//  EditProfileViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 09/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import ObjectMapper
import Kingfisher
import Photos
import MobileCoreServices

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var phnNumber: UITextField!
    @IBOutlet weak var address: UITextView!
    @IBOutlet weak var userImage: UIImageView!
    
    var pickedImageName:String!
    var imagePicker = UIImagePickerController()
    var selectedPhoto:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserProfile()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpImageTapped()
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        updateProfile()
    }
    
    func setUpImageTapped(){
        
        let imageTap = UITapGestureRecognizer(target: self, action: #selector(self.userImageTapped(_ :)))
        self.userImage.isUserInteractionEnabled = true
        self.userImage.addGestureRecognizer(imageTap)
    }
    
    @objc func userImageTapped(_ sender: UITapGestureRecognizer) {
         imageActionSheet()
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? NSURL
        self.pickedImageName = (imageURL?.lastPathComponent) ?? ""
       
        var selectImage : UIImage!
        if let img = info[.editedImage] as? UIImage
        {
            selectImage = img
        }
        else if let img = info[.originalImage] as? UIImage
        {
            selectImage = img
        }
        
        self.selectedPhoto = selectImage
        self.userImage.image = selectImage
        dismiss(animated: true, completion: nil)
        
    }
    
    func imageActionSheet() {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let pickFromCamera = UIAlertAction(title: "Take From Camera", style: .default)
        { _ in
            
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .camera;
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheetController.addAction(pickFromCamera)
        
        let pickFromGallery = UIAlertAction(title: "Pick From Gallery", style: .default)
        { _ in
            
            self.imagePicker.mediaTypes = [kUTTypeImage as String]
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .photoLibrary;
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }
        
        actionSheetController.addAction(pickFromGallery)
        self.present(actionSheetController, animated: true, completion: nil)
    }
}


extension EditProfileViewController{
    
    func getUserProfile(){
        
        SVProgressHUD.show()
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        let url = "getProfile"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        Alamofire.request(baseUrl, method: .get, headers: headers).responseJSON { (response) in
            print(response) // http url reponse
            if response.result.isSuccess {
                
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<updateProfileObject>().map(JSONString: jsonStr!)
                
                self.name.text = responseModel?.uerprofile?.first?.name
                self.phnNumber.text = responseModel?.uerprofile?.first?.phone
                self.address.text = responseModel?.uerprofile?.first?.address
                
                if let image = responseModel?.uerprofile?.first?.profilePic{
                self.userImage.kf.setImage(with: URL(string: image), placeholder: UIImage(named: "Placeholder"))
                }
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
            }
        }
    }
    
    func updateProfile() {
        
        if name.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter user name")
        }
        else if phnNumber.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter phone number")
        }
        else if address.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter address")
        }
        else {
            
            SVProgressHUD.show()
            
            let uName =  name.text
            let phNo = phnNumber.text
            let userAddress = address.text
            
            let headers: HTTPHeaders = [
                "accesstoken": Constants.accessToken
            ]
            
            let params = ["name":uName!,"phone":phNo!,"address":userAddress!] as [String : AnyObject]
            
            let url = "updateProfile"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            let imageData = selectedPhoto?.jpegData(compressionQuality: 0.7)
            
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in params {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                if let data = imageData{
                    multipartFormData.append(data, withName: "image", fileName: self.pickedImageName, mimeType: "image/jpeg")
                }
                
            },  to: baseUrl, method: .post, headers: headers) { (result) in
                switch result{
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        print("Succesfully uploaded")
                        print(response.result.isSuccess)
                        
                        SVProgressHUD.dismiss()
                        
                        let jsonData = JSON(response.result.value!)
                        print(jsonData)
                        
                        if let dictObject = jsonData.dictionaryObject {
                            print(dictObject)
                            
                            let message = dictObject["message"] as! String
                            let status = dictObject["status"] as! Int
                            
                            if status == 200 {
                                let alertController = UIAlertController(title: "Title", message: (String(describing: message)), preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                    UIAlertAction in
                                    
                                    self.dismiss(animated: true) {
                                        self.navigationController?.popToRootViewController(animated: true)
                                    }
                                    
                                }
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                                
                            else{
                                let alertController = UIAlertController(title: "Alert", message: "\(String(describing: message))", preferredStyle: .alert);
                                alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                                self.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
                    
                case .failure(let error):
                    SVProgressHUD.dismiss()
                    print("Error in upload: \(error.localizedDescription)")
                    print(error)
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
                }
            }
        }
    }
}
