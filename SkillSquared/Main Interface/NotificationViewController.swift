//
//  NotificationViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 24/04/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SwiftyJSON
import SVProgressHUD

class NotificationViewController: UIViewController {
    
    
    @IBOutlet weak var notificationTableView: UITableView!

    
    var notificationList : Array<Notifications>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.clearBadgeCounter()
        
    }

}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationCell
      
        if let data = notificationList {
            cell.orderDESCRIPTION.text = data[indexPath.item].body
            cell.orderDate.text = data[indexPath.item].created_on
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detail = notificationList?[indexPath.row].redirect_detail
        
        if detail != "0" {
            let link = detail?.components(separatedBy: "_")

            guard let name = link?[0] else {return}
            guard let id = link?[1] else {return}
            
            
            if name == "orderDetailBuyer"{
                
                let con = storyboard?.instantiateViewController(withIdentifier: "BuyerNormalOrder") as! ManageOrdersViewController
                self.navigationController?.pushViewController(con, animated: true)
                
            }
            else if name == "offerReceived"{
                
                let con = storyboard?.instantiateViewController(withIdentifier: "ViewOffersVC") as! ViewOffersVC
                con.catId = id
                self.navigationController?.pushViewController(con, animated: true)
            }
                
            else if name == "orderDetail"{
                
                let con = storyboard?.instantiateViewController(withIdentifier: "OrdersViewController") as! OrdersViewController
                self.navigationController?.pushViewController(con, animated: true)
            }
        }
    }
}

extension NotificationViewController{
    
    func clearBadgeCounter() {
        
        SVProgressHUD.show()
        
        let url = "notificationReadDone"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)
            print(response)
            
            if response.result.isSuccess {
                
                UserDefaults.standard.removeObject(forKey: "badgeValue")
                self.getAllNotifications()
            
                
            }else if response.result.isFailure{
                
                UserDefaults.standard.removeObject(forKey: "badgeValue")
                self.getAllNotifications()
                
            }
        }
    }
    
    func getAllNotifications() {
        
        SVProgressHUD.show()
        
        let url = "getNotifications"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let headers: HTTPHeaders = [
            "accesstoken": Constants.accessToken
        ]
        
        Alamofire.request(baseUrl, method: .post, headers: headers).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            print(response)  // http url reponse
            
            if response.result.isSuccess {
                SVProgressHUD.dismiss()
                
                guard let jsonData = response.data else{return}
                let jsonStr = String(data: jsonData, encoding: .utf8)
                
                let responseModel = Mapper<NotificationObject>().map(JSONString: jsonStr!)
                self.notificationList = responseModel?.notifications
                
                self.notificationTableView.reloadData()
                
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please check your internet connection or try again later")
            }
        }
    }
}
