//
//  CategoriesDataController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 28/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

struct AllData {
    var Label: String
}

class CategoriesDataController: UIViewController {
    
     var DataItems: [AllData] = [AllData(Label: "Dummy 1"),AllData(Label: "Dummy 2"),AllData(Label: "Dummy 3"),AllData(Label: "Dummy 4"),AllData(Label: "Dummy 5"),AllData(Label: "Dummy 6"),AllData(Label: "Dummy 7"),AllData(Label: "Dummy 8"),AllData(Label: "Dummy 9"),AllData(Label: "Dummy 10"),AllData(Label: "Dummy 11"),AllData(Label: "Dummy 12"),AllData(Label: "Dummy 13"),AllData(Label: "Dummy 14"),AllData(Label: "Dummy 15"),AllData(Label: "Dummy 16"),]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.02479270101, green: 0.7008461952, blue: 0.4103333652, alpha: 1)
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.isTranslucent = false
        
    }
}

extension CategoriesDataController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as! DataCell
        cell.LableData.text = DataItems[indexPath.item].Label
        return cell
    }
}
