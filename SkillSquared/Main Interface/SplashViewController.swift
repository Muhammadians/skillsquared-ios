//
//  SplashViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 07/04/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import Quickblox
import SVProgressHUD
import RappleProgressHUD

class SplashViewController: UIViewController {
    
    @IBOutlet weak var loadingApp: UILabel!
    @IBOutlet weak var value: UILabel!
    
    private var infoText = "" {
        didSet {
            loadingApp.text = infoText
        }
    }
    
    var val: String!
    var dialogId: String!
    var dialogName: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        perform(#selector(authenticate), with: nil, afterDelay: 0.4)
//         if (UserDefaults.standard.bool(forKey: "openedByShortcutAction")) {
//                  NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("notification"), object: nil)
//               }
        
        let profile = Profile()
        
                if profile.isFull == false || Constants.userEmail == "" {
                    print("email is \(Constants.userEmail)")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                        app.loginWindow()
                    })
                }
        
//        if Constants.userEmail == "" {
//            print("email is \(Constants.userEmail)")
//            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
//                app.loginWindow()
//            })
//        }
    }
    
//    @objc func authenticate(){
//
//        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("notification"), object: nil)
//
//    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "notification"), object: nil)
//    }
    
    
     @objc func methodOfReceivedNotification(notification: Notification) {
           
           guard let text = notification.userInfo?["text"] as? String else { return }
           print ("text is : \(text)")
           Constants.notificationConstant = text
           
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                      let chatController = storyboard.instantiateViewController(withIdentifier: "OrdersViewController") as! OrdersViewController
                  self.navigationController?.pushViewController(chatController, animated: true)
           
       }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.quicBloxLogin()
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "notification"), object: nil)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func connect(completion: QBChatCompletionBlock? = nil) {
        let currentUser = Profile()
        
        guard currentUser.isFull == true else {
            completion?(NSError(domain: LoginConstant.chatServiceDomain,
                                code: LoginConstant.errorDomaimCode,
                                userInfo: [
                                    NSLocalizedDescriptionKey: "Please enter your login and username."
            ]))
            return
        }
        if QBChat.instance.isConnected == true {
            completion?(nil)
        } else {
            QBSettings.autoReconnectEnabled = true
            QBChat.instance.connect(withUserID: currentUser.ID, password: currentUser.password, completion: completion)
        }
    }
    
    
    func quicBloxLogin() {
        
        print("email is \(Constants.userEmail)")
        let profile = Profile()
        if profile.isFull == false || Constants.userEmail == "" {
            print("email is \(Constants.userEmail)")
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                app.loginWindow()
            })
        }
            
        else {
            self.updateLoginInfo()
        }
    }
}


extension SplashViewController{
    
    func updateLoginInfo() {
        
        let updateLoginInfo: ((_ status: NetworkConnectionStatus) -> Void)? = { [weak self] status in
            let notConnection = status == .notConnection
            let loginInfo = notConnection ? LoginConstant.checkInternet : LoginStatusConstant.intoChat
            let profile = Profile()
            if profile.isFull == true, notConnection == false {
                self?.login(fullName: profile.fullName, login: profile.login)
            }
            self?.infoText = loginInfo
        }
        
        Reachability.instance.networkStatusBlock = { status in
            updateLoginInfo?(status)
        }
        updateLoginInfo?(Reachability.instance.networkConnectionStatus())
    }
    
    
    private func login(fullName: String, login: String, password: String = LoginConstant.defaultPassword) {
        
        QBRequest.logIn(withUserLogin: login,
                        password: password,
                        successBlock: { [weak self] response, user in
                            guard let self = self else {
                                return
                            }
                            
                            user.password = password
                            Profile.synchronize(user)
                            self.connectToChat(user: user)
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(3)) {
                                
                                app.dashboardWindow()
                                
                            }
                            
            }, errorBlock: { [weak self] response in
                self?.handleError(response.error?.error, domain: ErrorDomain.logIn)
                if response.status == QBResponseStatusCode.unAuthorized {
                    // Clean profile
                    
                    Profile.clearProfile()
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                        app.loginWindow()
                    }
                }
        })
    }
    
    /**
     *  connectToChat
     */
    private func connectToChat(user: QBUUser) {
        
        infoText = LoginStatusConstant.intoChat
        QBChat.instance.connect(withUserID: user.id,
                                password: LoginConstant.defaultPassword,
                                completion: { [weak self] error in
                                    guard let self = self else { return }
                                    if let error = error {
                                        if error._code == QBResponseStatusCode.unAuthorized.rawValue {
                                            // Clean profile
                                            Profile.clearProfile()
                                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                                                app.loginWindow()
                                            }
                                        }
                                        
                                    } else {
                                        self.registerForRemoteNotifications()
                                        //did Login action
                                        //                                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                                        //                                            app.dashboardWindow()
                                        //                                        }
                                    }
        })
        
    }
    
    private func registerForRemoteNotifications() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.sound, .alert, .badge], completionHandler: { granted, error in
            if let error = error {
                debugPrint("[AuthorizationViewController] registerForRemoteNotifications error: \(error.localizedDescription)")
                return
            }
            center.getNotificationSettings(completionHandler: { settings in
                if settings.authorizationStatus != .authorized {
                    return
                }
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            })
        })
    }
    
    private func handleError(_ error: Error?, domain: ErrorDomain) {
        guard let error = error else {
            return
        }
        var infoText = error.localizedDescription
        if error._code == NSURLErrorNotConnectedToInternet {
            infoText = LoginConstant.checkInternet
        }
        
        self.infoText = infoText
    }
}

