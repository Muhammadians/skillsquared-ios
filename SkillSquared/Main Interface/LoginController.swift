//
//  Sign-in.swift
//  SkillSquared
//
//  Created by Awais Aslam on 23/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import ObjectMapper
import Quickblox
import UserNotifications



class LoginController: UIViewController{
    
    @IBOutlet weak var mailField: SATextField!
    @IBOutlet weak var passwordField: SATextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    @IBAction func signInTapped(_ sender: Any) {
        
        getloginData()
        
    }
    
    @IBAction func forgetPasswordTapped(_ sender: Any)
        
    {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let HomeController = storyboard.instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordViewController
        self.navigationController?.pushViewController(HomeController, animated: true)
        
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let HomeController = storyboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpViewController
        self.navigationController?.pushViewController(HomeController, animated: true)
        
    }
    
    
}

extension LoginController{
    
    
    func getloginData() {
        
        
        
        let Email = mailField.text
        let password = passwordField.text
        
        
        if ( Email == "" || password == "" ){
            
            Alert.showAlert(on: self, title: "Alert", message: "Please enter email and password")
        }
        else{
            
            SVProgressHUD.show()
            
            let params = [
                "devicetype": "ios",
                "email": Email!,
                "password": password!,
                "device_id": deviceTokenGet
                
                ] as [String : Any]
            
            let url = "login"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            print(params)
            
            Alamofire.request(baseUrl, method: .post, parameters: params).responseJSON { (response) in
                print(response) // http url reponse
                if response.result.isSuccess {
                    
                    guard let jsonData = response.data else{return}
                    let jsonStr = String(data: jsonData, encoding: .utf8)
                    
                    let responseModel = Mapper<Login>().map(JSONString: jsonStr!)
                    let message = responseModel?.message
                    
                    if responseModel?.status == 200 {
                        
                        Constants.userEmail = responseModel?.skillSquaredUser?.email ?? ""
                        Constants.phoneNumber = responseModel?.skillSquaredUser?.phone ?? ""
                        Constants.address = responseModel?.skillSquaredUser?.address ?? ""
                        Constants.accessToken = responseModel?.skillSquaredUser?.access_token ?? ""
                        Constants.userName = responseModel?.skillSquaredUser?.name ?? ""
                        Constants.userID = responseModel?.skillSquaredUser?.username ?? ""
                        Constants.userImage = responseModel?.skillSquaredUser?.image ?? ""
                        Constants.freeLancer = (responseModel?.freelancerProfileSet!)!
                        
                        
//                      self.signUp(fullName: Constants.userEmail, login: Constants.userName)
                        self.signUp(fullName: Constants.userName, login: Constants.userEmail)
                         
                        
                    }
                        
                    else if responseModel?.status == 204 {
                        
                        SVProgressHUD.dismiss()
                        
                        let alertController = UIAlertController(title: "Alert", message: message!, preferredStyle: .alert);
                        alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                        self.present(alertController, animated: true, completion: nil)
                        
                    }
                    
                }else if response.result.isFailure{
                    print(response)
                    SVProgressHUD.dismiss()
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
                }
            }
        }
    }
}


extension LoginController {
    
    private func signUp(fullName: String, login: String) {
        
        let user = QBUUser()
        user.login = login
        user.fullName = fullName
        user.password = LoginConstant.defaultPassword
        
        QBRequest.signUp(user, successBlock: { response, user in
            
            self.login(fullName: fullName, login: login)
            
        }, errorBlock: { (response) in
            
            print("User Already Created")
            self.login(fullName: fullName, login: login)
            
        })
    }
    
    private func login(fullName: String, login: String, password: String = LoginConstant.defaultPassword) {
        print(fullName)
        QBRequest.logIn(withUserLogin: login, password: password, successBlock: { (response, user) in
            
            
            user.password = password
            Profile.synchronize(user)
            
            
            if user.fullName != fullName {
                self.updateFullName(fullName: fullName, login: login)
            } else {
                self.connectToChat(user: user)
            }
            
            print("Login Success")
            
            
        }, errorBlock: { (response) in
            
            SVProgressHUD.dismiss()
            print("Login Failed")
            
        })
    }
    
    private func updateFullName(fullName: String, login: String) {
        let updateUserParameter = QBUpdateUserParameters()
        updateUserParameter.fullName = fullName
        QBRequest.updateCurrentUser(updateUserParameter, successBlock: { [weak self] response, user in
            guard let self = self else {
                return
            }
            Profile.update(user)
            self.connectToChat(user: user)
            
            }, errorBlock: { [weak self] response in
                print("Error in signup")
        })
    }
    
    
    private func connectToChat(user: QBUUser) {
        QBChat.instance.connect(withUserID: user.id,
                                password: LoginConstant.defaultPassword,
                                completion: { [weak self] error in
                                    guard let self = self else { return }
                                    
                                    SVProgressHUD.dismiss()
                                    self.registerForRemoteNotifications()
                                    app.dashboardWindow()
                                    //did Login action
                                    
        })
    }
    
    private func registerForRemoteNotifications() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.sound, .alert, .badge], completionHandler: { granted, error in
            if let error = error {
                debugPrint("[AuthorizationViewController] registerForRemoteNotifications error: \(error.localizedDescription)")
                return
            }
            center.getNotificationSettings(completionHandler: { settings in
                if settings.authorizationStatus != .authorized {
                    return
                }
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            })
        })
    }
    
}
