//
//  ForgetPasswordViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 29/01/2020.
//  Copyright © 2020 Awais Aslam. All rights reserved.
//

import UIKit
import SwiftyJSON
import SVProgressHUD
import Alamofire

class ForgetPasswordViewController: UIViewController {
    
    @IBOutlet weak var enterEmail: SATextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func resetPasswordBtn(_ sender: Any) {
        
        if enterEmail.text!.isEmpty{
            
            Alert.showAlert(on: self, title: "Alert", message: "Please enter email")
        }
            
        else{
            
            SVProgressHUD.show()
            
            let emailTxt =  enterEmail.text
            
            
            let params = ["email":emailTxt!] as [String : AnyObject]
            
            let url = "forgotPassword"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default).responseJSON {response in
                
                var err:Error?
                
                switch response.result {
                    
                case .success(let json):
                    print(json)
                    SVProgressHUD.dismiss()
                    
                    let jsonData = JSON(response.result.value!)
                    print(jsonData)
                    
                    
                    if let dictObject = jsonData.dictionaryObject {
                        print(dictObject)
                        
                        let message = dictObject["message"] as! String
                        let status = dictObject["status"] as! Int
                        
                        if status == 200 {
                            
                            let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                self.navigationController?.popToRootViewController(animated: false)
                            }
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                            
                        else{
                            let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                            alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                    
                case .failure(let error):
                    err = error
                    print(err!)
                    SVProgressHUD.dismiss()
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
                }
            }
        }
    }
}

