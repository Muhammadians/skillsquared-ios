//
//  Sign-Up.swift
//  SkillSquared
//
//  Created by Awais Aslam on 23/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import SVProgressHUD
import CountryPickerView
import GooglePlaces

class SignUpViewController: UIViewController, CLLocationManagerDelegate{
    
    @IBOutlet weak var fullName: SATextField!
    @IBOutlet weak var emailId: SATextField!
    @IBOutlet weak var PhoneNumber: SATextField!
    @IBOutlet weak var password: SATextField!
    @IBOutlet weak var confirmPass: SATextField!
    @IBOutlet weak var termAndServicesBox: UIButton!
    @IBOutlet weak var conditionsLabel: UILabel!
    @IBOutlet weak var addressTxt: SATextField!
    
    var zoneTime = ""
    var deviceToken = ""
    var countryCode:String!
    var countryNumber:String!
    
    var g_lat1:Double!
    var g_long1:Double!
    
    private let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        let countryView = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        PhoneNumber.leftView = countryView
        PhoneNumber.leftViewMode = .always
        
        countryView.delegate = self
        countryView.dataSource = self
        let country = countryView.selectedCountry
        
        self.countryNumber = countryView.selectedCountry.phoneCode
        self.countryCode = countryView.selectedCountry.code
        
        print("Country code is : ", country)
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setUpLabelTapped()
    }
    
    func getLocal()  -> String {
        let countryCode = Locale.current.regionCode!
        zoneTime = countryCode
        print("\(countryCode)")
        return countryCode
    }
    
    @IBAction func checkBoxTapped(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            sender.isSelected = !sender.isSelected
            
        }) { (success) in
            
            UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveLinear, animations: {
                sender.transform = .identity
            }, completion: nil)
        }
        
    }
    
    @IBAction func textFieldTapped(_ sender: Any) {
        
        addressTxt.resignFirstResponder()
        let acController = GMSAutocompleteViewController()
        acController.delegate = self
        present(acController, animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func signInTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func setUpLabelTapped(){
        
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.termAndConditionsTapped(_ :)))
        self.conditionsLabel.isUserInteractionEnabled = true
        self.conditionsLabel.addGestureRecognizer(labelTap)
    }
    
    @objc func termAndConditionsTapped(_ sender: UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let conditionsController = storyboard.instantiateViewController(withIdentifier: "TermAndConditions") as! TermOfServicesVC
        self.navigationController?.pushViewController(conditionsController, animated: true)
        
    }
    
    
    
    @IBAction func signUpPressed(_ sender: Any) {
        
        if fullName.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter user name")
        }
        else if emailId.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter email address")
        }
        else if PhoneNumber.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter Phone number")
        }
        else if addressTxt.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter address")
        }
        else if password.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter password")
        }
        else if confirmPass.text!.isEmpty{
            Alert.showAlert(on: self, title: "Alert", message: "Please enter confirm password")
        }
        else if confirmPass.text != password.text {
            Alert.showAlert(on: self, title: "Alert", message: "Password not matched" )
        }
        else if !termAndServicesBox.isSelected{
            Alert.showAlert(on: self, title: "Alert", message: "Please agree to our Term and service" )
            
        }
            
        else {
            
            SVProgressHUD.show()
            
            let uName =  fullName.text
            let mailId = emailId.text
            let phnNumber = PhoneNumber.text
            let userPass = password.text
            let device = "IOS"
            let zoneTime = self.getLocal()
            let deviceId = deviceTokenGet
            let address = addressTxt.text
            
            
            let params = ["name":uName!,"email":mailId!,"password":userPass!,"phone":"\(self.countryCode ?? "")\(" ")\(self.countryNumber ?? "")\(" ")\(phnNumber!)","devicetype":device,"device_id":deviceId,"timezone":zoneTime, "address":address!, "latitude":g_lat1!, "longitude":g_long1!] as [String : AnyObject]
            
            print(params)
            
            print(g_long1!)
            print(g_lat1!)
            
            let url = "signup"
            let baseUrl = "\(K_BaseUrl)\(url)"
            
            Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default).responseJSON {response in
                
                var err:Error?
                
                switch response.result {
                    
                case .success(let json):
                    print(json)
                    SVProgressHUD.dismiss()
                    
                    let jsonData = JSON(response.result.value!)
                    print(jsonData)
                    
                    
                    if let dictObject = jsonData.dictionaryObject {
                        print(dictObject)
                        
                        let message = dictObject["message"] as! String
                        let status = dictObject["status"] as! Int
                        
                        if status == 200 {
                            let alertController = UIAlertController(title: "Title", message: (String(describing: message)), preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                                UIAlertAction in
                                self.navigationController?.popViewController(animated: true)
                            }
                            
                            alertController.addAction(okAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                            
                        else {
                            
                            let alertController = UIAlertController(title: "Alert", message: (String(describing: message)), preferredStyle: .alert);
                            alertController.addAction(UIAlertAction(title: "OK", style: .default,handler: nil));
                            self.present(alertController, animated: true, completion: nil)
                            
                        }
                    }
                    
                case .failure(let error):
                    err = error
                    print(err!)
                    SVProgressHUD.dismiss()
                    Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, Check your connection or try again later")
                }
            }
        }
    }
}


extension SignUpViewController: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        let lang = place.coordinate.longitude
        let lat = place.coordinate.latitude
        g_lat1 = lat
        g_long1 = lang
        
        addressTxt.text = place.name
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // Handle the error
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        // Dismiss when the user canceled the action
        dismiss(animated: true, completion: nil)
    }
}

extension SignUpViewController : CountryPickerViewDelegate, CountryPickerViewDataSource{
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country){
        
        self.countryCode = country.code
        self.countryNumber = country.phoneCode
        
        print(self.countryNumber)
        print(self.countryCode)
    }
}
