//
//  CategoriesFilterViewController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 26/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import iOSDropDown

class CategoriesFilterViewController: UIViewController {
    @IBOutlet weak var Categories: DropDown!
    @IBOutlet weak var Location: DropDown!
    @IBOutlet weak var Price: DropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DropDownCat()
        DropDownLoc()
        DropDownPrize()
        
    }
    
    func DropDownCat()
    {
        Categories.optionArray = ["Dummy 1", "Dummy 2", "Dummy 3"]
        Categories.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //Its Id Values and its optional
        Categories.optionIds = [31,31,33]
    }
    
    func DropDownLoc(){
        Location.optionArray = ["Dummy 1", "Dummy 2", "Dummy 3", "Dummy 4"]
        Location.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //Its Id Values and its optional
        Location.optionIds = [31,31,33,34]
    }
    
    func DropDownPrize() {
        
        Price.optionArray = ["Dummy 1", "Dummy 2", "Dummy 3"]
        Price.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //Its Id Values and its optional
        Price.optionIds = [35,36,37]
    }

   
}
