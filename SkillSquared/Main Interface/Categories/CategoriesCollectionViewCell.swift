//
//  CategoriesCollectionViewCell.swift
//  SkillSquared
//
//  Created by Awais Aslam on 25/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var CategoriesImage: UIImageView!
    @IBOutlet weak var CategoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
