//
//  CategoriesController.swift
//  SkillSquared
//
//  Created by Awais Aslam on 25/10/2019.
//  Copyright © 2019 Awais Aslam. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

//struct Categoryitems {
//    var CatImage: String
//    var Catlable: String
//}

class CategoriesController: UIViewController{
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var categorySearch: UISearchBar!
    
    var catItems : Array<Object>?
    var forSearchCatItems : Array<Object>?
    var catId = ""
    var gigCat_Id = ""
    var CollectionViewFlowLayout: UICollectionViewFlowLayout!
    let CollectionViewIdentifier = "CategoriesCell"
    var selectedIndex:Int = 0
    
    var searching = false
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        categorySearch.delegate = self
        SetUpCollectionView()
        catItems = []
        getSubCategoriesData()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        CollectionViewSize()
    }
    
    
    private func CollectionViewSize() {
        
        if CollectionViewFlowLayout == nil {
            
            // let numberOfItemPerRow: CGFloat = 2
            
            let lineSpacing: CGFloat = 10
            let interItemSpacing: CGFloat = 10
            
            // let width = UIScreen.main.bounds.width / 2
            // let height = UIScreen.main.bounds.height / 4
            
            let width = UIScreen.main.bounds.width / 2
            //                print(width)
            let height = width
            
            CollectionViewFlowLayout = UICollectionViewFlowLayout()
            
            CollectionViewFlowLayout.itemSize = CGSize(width: width - 15 , height: height)
            CollectionViewFlowLayout.sectionInset = UIEdgeInsets.zero
            CollectionViewFlowLayout.scrollDirection = .vertical
            CollectionViewFlowLayout.minimumLineSpacing = lineSpacing
            CollectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
            
            categoryCollectionView.setCollectionViewLayout(CollectionViewFlowLayout, animated: true)
        }
    }
    
    private func SetUpCollectionView(){
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        let Categorynib = UINib(nibName: "CategoriesCollectionViewCell", bundle: nil)
        categoryCollectionView.register(Categorynib, forCellWithReuseIdentifier: CollectionViewIdentifier)
    }
}

extension CategoriesController: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        forSearchCatItems = catItems?.filter{($0.title?.lowercased().contains(searchText.lowercased()))!}
        searching = true
        categoryCollectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        categoryCollectionView.reloadData()
    }
    
}

extension CategoriesController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if searching{
            return forSearchCatItems?.count ?? 0
        }else{
            return catItems?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewIdentifier, for: indexPath) as! CategoriesCollectionViewCell
        
        if searching{
            
            if let details = forSearchCatItems{
                cell.CategoryLabel.text = details[indexPath.row].title ?? ""
            }
        }else{
            if let details = catItems{
                
                cell.CategoriesImage.kf.setImage(with: URL(string: details[indexPath.item].page_banner!))
                cell.CategoryLabel.text = details[indexPath.row].title ?? ""
            }
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let gigsVC = storyboard.instantiateViewController(withIdentifier: "GigVC") as! GigsController
        
        if searching == true {
            gigsVC.cat_id = forSearchCatItems![selectedIndex].cat_id!
        }
        else{
            gigsVC.cat_id = catItems![selectedIndex].cat_id!
        }
        self.navigationController?.pushViewController(gigsVC, animated: true)
        
    }
}

extension CategoriesController{
    
    func getSubCategoriesData() {
        
        SVProgressHUD.show()
        
        let url = "getCategories"
        let baseUrl = "\(K_BaseUrl)\(url)"
        
        let params = [
            "cat_id": catId
            ] as [String : Any]
        
        Alamofire.request(baseUrl, method: .post, parameters: params).responseJSON { (response) in
            
            print(response.request as Any)  // original url request
            
            print(response)  // http url reponse
            if response.result.isSuccess {
                
                SVProgressHUD.dismiss()
                
                
                guard let jsonData = response.data else{return}
                do{
                    
                    let responseObject = try JSONDecoder().decode(Cat_Object.self, from: jsonData)
                    
                    let catData = responseObject.object
                    
                    self.catItems = catData
                    self.forSearchCatItems = catData
                    self.categoryCollectionView.reloadData()
                }
                catch{
                    print(error.localizedDescription)
                }
                
            }else if response.result.isFailure{
                print(response)
                SVProgressHUD.dismiss()
                Alert.showAlert(on: self, title: "Alert", message: "Somthing wrong, please try again later")
            }
        }
    }
}
