//
//  RootParentVC.swift
//  sample-chat-swift
//
//  Created by Injoit on 1/27/20.
//  Copyright © 2020 quickBlox. All rights reserved.
//

import UIKit

class RootParentVC: UIViewController {
    
    private var current: UIViewController
    
    init() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        current = storyboard.instantiateViewController(withIdentifier: "splachController") as! SplashViewController
        super.init(nibName:  nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addChild(current)
        current.view.frame = view.bounds
        view.addSubview(current.view)
        current.didMove(toParent: self)
    }
    
    func showLoginScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let authNavVC = storyboard.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
        
        addChild(authNavVC)
        authNavVC.view.frame = view.bounds
        view.addSubview(authNavVC.view)
        authNavVC.didMove(toParent: self)
        
        current.willMove(toParent: nil)
        current.view.removeFromSuperview()
        current.removeFromParent()
        current = authNavVC
    }
    
    func goToDialogsScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let dialogsVC = storyboard.instantiateViewController(withIdentifier: "Dashboard") as! HomeViewController
        let dialogsScreen = UINavigationController(rootViewController: dialogsVC)
        
        addChild(dialogsScreen)
        dialogsScreen.view.frame = view.bounds
        view.addSubview(dialogsScreen.view)
        dialogsScreen.didMove(toParent: self)
        
        current.willMove(toParent: nil)
        current.view.removeFromSuperview()
        current.removeFromParent()
        current = dialogsScreen
    }
    
}
